# Læringsmål
## Målsetting
* Å oppnå en grunnleggende forståelse for 
kommunikasjonsnett og nettbaserte tjenester
* Å lære prinsipper som gjør det mulig å kunne resonere, drøfte og 
være kreativ med hensyn til kommunikasjonsnett og nettbaserte tjenester.
## Kunnkap
* Gi kunnskap om og grunnleggende forståelse av de mest fundamentale 
konsepter, prinsipper og teknologier som er grunnlaget for 
kommunikasjonsnett og tjenester. Dette inkluderer arkitektur, 
prinsipper som omhandler nettbaserte tjenester, svitsjeteknikker, 
funksjonalitet og protokoller i nettets fem ulike kommunikasjonslag, 
og bruken av Transportlag-protokoller i design av nett-applikasjoner.
* Grunnleggende forståelse av Trådløse og mobile nett.
* Grunnleggende forståelse av Multimedia over nett.
* Grunnleggende forståelse av Kommunikasjonssikkerhet.
## Ferdigheter
* Å kunne resonere og drøfte arkitektur, prinsipper, og teknologier ved 
design og implementering av kommunikasjonsnett og tjenester.
*    Å kunne forklare hvordan og hvorfor nettets fem lag fungerer.
*    Å kunne kommunisere, resonere og dr
øfte trådløse og mobile nett, og 
bruk av multimedia over nettet.
*    Å kunne designe og implementere elementære nettapplikasjoner som 
utnytter transportlagsprotokollen.
*    Å kunne kommunisere, resonere og drøfte grunnleggende 
kommunikasjonssikkerhet.
# Chapter 1
## Misc poo
* Protocol = format and order of msgs between entities, also actions + receipts.
* IP: specifies format of packets
* Host = end system = client/server
* Access network: Connects end systemts to first router.
* DSL does cool things; frequency-division multiplexing for up/down/phone simult.
* Or cable = coax + fiber.
* Many ways to get internet, physically. Twisted copper, coax, fiber, radio, microwaves to satellites. Satellites in geostationary (high up) or low earth orbit.
* Packet switches = routers and linklayer switches (mainly).
* Store and forward: A router needs to receive whole packet before starting transmission to end destination (or next dest).
* Packet loss: When output buffer is full at the packet switch, but it still receives shit to forward (more quickly than it can forward things already queued)
* Forwarding table: Map (part of) destination addr in packet header to a link (which may be another router)
* Routing protocol: Automagic set forward table. Based on metrics such as shortest path.
* Circuit switching: Reserving a portion of the transsmission rate of the link instead of being a spammy asshole. Guarantees rate. Can be freq. divided or time divided. Wasteful when a reserved link portion is inactive (but still in use).
* Time division: Time is divied into frames (e.g. 1 sec) and frames have # of slots. Say 5. Then each connection can get a slot per frame, and then gets to send shit 200ms per frame.
* ISPs come in layers. Some kinda global, many regional, plenty of access ISPs.
* Delays: processing, queuing, transmission, propagation
## Layers
* Application: HTTP, SMTP, FTP, DNS. Packet here = message.
* Transport: TCP (conn), UDP (connless). Packet here = segment.
* Network: Datagram. Delivery of TCP/UDP's segments. IP.
* Link: Frames. Move from one node to another (the network layer is responsible for the WHOLE trip). Ethernet, WiFi.
* Physical: Moving actual bits.
* OSI: Has extra layers; presentation (encryption, compression) and session layer (syncrhonization ++).
Take note: A link-layer switch does not know the network layer, hence it does not understand an IP address.
Notice we get encapsulation by prepending headers as we traverse down layers.
## Attack!
* We got viruses, worms (no user interaction), botnets, DoS attacks, packet sniffers, IP spoofing
# Chapter 2 - Application Layer
* Applications client/server or P2P.
* P2P challenges: ISPs now have higher upload loads, P2P is open and hard to secure, how to incentivize (?) users into giving bandwidth
* Processes live in end systems. Client process contacts a server process.
* Processes communicate via sockets; sockets are at their "edges". Processes send msgs through sockets and listen to sockets. What goes on after giving a msg to a socket? That depends on a deeper layer; transport.
* A packet given to a socket must have an IP address and also specify which process should receive the msg. How to do this? Use a port number.
* Transport services: Reliable, throughput, timing, security
## TCP
* Connection-oriented (handshaking etc).
* Full duplex
* Reliable
* SSL = TCP++; secure socket layer. It's like using TCP but getting encryption +++. Realized in the app layer.
* Can be non-persistent or persistent.
* Data streams
## UDP
* Spam
* Used when you should tolerate losses; Skype. Many firewalls block UDP, so TCP as fallback is common.
* Sends whole packets
## HTTP
* Stateless
* Pull protocol
* Used by TCP client <--> server connections. Default persistent
* Typical: Three way handshake, last part is an ACK comboed with a file request (to server).
* Request msg:
method  url         version
header  field name  value
header  field name  value
header  field name  value
header  field name  value

entity body
* Methods: GET PUT HEAD PUT DELETE
* Response msg:
version status code phrase
header  field name  value
header  field name  value
header  field name  value
header  field name  value

entity body
* Status codes are numbered; 200, 404...
* Phrases can be OK, Not Found, Bad Request...
* Cookies: You might have a cookie file which has several IDs that you have gotten from various sites. These are added to the header lines of request msgs.
## Web cache == proxy server
Sits between client and server and gives clients files. If it does not have the file it gets it from server.
Can improve response times by lots. If cache on campus has hit rate from 0.2-0.7, then in those cases the response times are juts millisecs since internet need not be concerned.

Another thing is that objects can go stale; cached version != newest version. So client uses GET and cache uses If-modified-since GET to server.

## Mail
Mail servers do dirty work. Stores mailbox. Retries if failing, many times ongoing for days.
The servers also act as SMTP clients, and SMTP servers.

The client uses an "user agent" == mail program to perform actions. The user agent then again contacts mail server. The mail server then SMTP clients itself with the recipient's mail server acting as SMTP server.
### SMTP
* Persistent
* 7-bit ASCII
* Client can write stuff like HELO, MAIL FROM, RCTP TO, DATA, and QUIT.
* Push protocol
This means you need something else than SMTP to pull from a mail server (to receive). This can be POP3, IMAP, or HTTP.

### POP3
Client --> TCP --> mail server (port 110).

Auth --> transaction --> update

Does not support things like remote folders

### IMAP
Now you can have remote folders. Also carries state across sessions.

### Web based
Now HTTP is used to and from users. STMP is used internally though.

## DNS
* hostname --> IP
* Is a distributed database; hosts can query
* UDP port 53
* Allows many servers to server same website. Then got many IPs. Each DNS query makes the DNS server rotate which IP is served.
* IP read from left to right, with increasinly revealing info
* Applications (mail, web, ...) use client DNS to translate hostnames.
* Root servers (13 worldwide), Top level domain = TLD DNS servers (.org, .com ...) and many under (ebay, amazon, ...). Lastly local DNS server each ISP has.
* Requesting host to local DNS is typically recursive, rest iterative (local DNS talks to root, TLD, ...)
* Local DNS server caches a lot. TLD servers, also hostname--> translations directly.
### Resource records
* Four-tuple: (Name, Value, Type, TTL = Time to Live = when to kill in cache).
Type A then hostname=name, value=IP addr. Type NS then name = domain, value = hostname of authorative DNS. Type = CNAME then value is canonical hostname for the alias hostname.
Type = MX then value canonical name of mail server that has an alias hostname Name.
### Messages
Are the same for replies and queries, with lotsa flags and shit.
### Entering into database
Need a registrar to have a hostname. Then a DNS server needs to have an NS-record which tells who has the A record.
## P2P
Each peer can redist any portion to any other peer.

While client-server distribution of files (time taken) scales linearly with time, P2P looks more like a e^{-N...} kinda function. Scales well. Time to distr is determined by: Server upload being shit, a peer's download speed being shit, or the collectively super-upload-coop being shit.
## Socket programming
### Client
* Make socket, specify IP type and UDP/TCP.
The OS will make a port nr for this client socket
* Send a msg using the socket, specify host and port.
The OS will attach source addr to the datagram
* Now you can make two vars; msg, serverAddr = cliSock.recvfrom(2048)
This way you know who replied. You specify size
### Server
* Make socket same way
* Bind socket to the port you want
* Listen forever (a listen instance gives an addr)
* Reply forever (using given addr)
### TCP
The TCP server has a special socket used as initial point of contact to TCP clients. TCP clients access this socket, three way handshakes, and are then given a new connection socket by the server.

The new socket is a pipe between the two, which you can just dump bytes into.

So you first have to sock.Connect() as client, and as server you must first .Listen(), then .Accept().
# Chapter 3: Transport Layer
Point of this layer? Instead of having the network layer delivering shit between two end systems, we want to be able to deliver shit between two application-layer processes (all the way baby!).
So we get logical (not physical) comm between processes on different hosts.

This layer makes segments by possibly breaking up app msgs into smaller ones and adding headers.
Then net layer will make datagrams with additional headers, which are the headers the routers and packet switches look at.

So: Transport layer gives comms between processes on hosts, net layer gives comms between hosts.

Problems: How to do this in an unreliable world, how to avoid congestion so that shit isn't bogged down.

So this layer offers UDP and TCP, which sends segments. TCP is reliable. The underlying thing, network layer IP (which sends datagrams), is an unreliable protocol. Mindblowing stuff we got here.

What does UDP offer? Process-to-process data sending, and error checking. That is it. TCP? Flow control, error check, reliable, acks, timers, sequence numbers...
## Muxing and demuxing
Demultiplexing: delivering to correct socket by inspection

Multiplexing: gather data at source host from sockets, encapsulate with header, make segments etc. 

A segment has a source port and a destination port.

A UDP socket is simply identified by two-tuple IP addr and dest port #. Thus different segments (different source addrs and/or source port #s) can be "equally" sent to the same UDP socket.

Why include source port #? Welllllllllllllll to reply.

TCP tho is four-tuple, source ip + port, dest ip + port. Will be routed uniquely even if dest ip + port same.
## UPD in-depth
So simple. Adds source and dest ports for demux- and muxing.

Why use UDP? Sometimes don't want to be throttled by TCP, and can tolerate loss. No connection is fine, so speedy! No state, easier to serve many! Small packet overhead!

UDP has two other fields: Lenght (headr + data) and checksum (1s complement of all 16bit words in segment)
The 16-bit words are summed, and overflow (at MSB) is wrapped around to LSB.
## Principles of reliable data transfer
### Bit errors, packet loss, duplicates
Positive ack: "OK" ACK

Negative ack: "Pls repeat" NAK

Using these two known as ARQ = Automatic repeat request protocls.

So we need error detection, receiver feedback (ACK/NAK), and retransmission.

Use sequence numbers to handle cases where you don't know if the ACK was corrupted.
Seq nrs also help with duplicate packets (just keep track of what you already got).

How about packet loss? Just use timer.
### Pipelining
Sad! Only send 1 packet at a time and wait for ack. Sad!
Need to allow sending loads of packets (thus many seq nrs) and not have them ack'd until a while.
Increases throughput by loadz. Need buffers.
### Go-Back-N
You now have a window size N. Starts at base (index is named base), the index at "nextseqnum" is what the num of the next packet will have as seq. You fill the window with "sent but not ack'd" and "next to be sent".

Also called sliding-window protocol since the base slides forwards as soon as the oldest ACK is recieved.

The name? If we get an error or timeout, we send out ALL packets not ack'd.
### Selective repeat
A problem is when GBN retransmit everything often and we have lots of packets all the time, then we do a lot of unwanted work.

Now we ACK out of order packets. We buffer them since they are out of order, until the correct ordering is gotten.

*Sender*: Get data, if place in window, transmit. Each packet has timeout. Mark as received when an ACK corresponding to packet arrives. If that is the *send_base* packet then move window.

*Receiver*: Has its own window, which moves as it sends packets to upper layers. Send ACKs as a packet is received. The window will be held up as an old packet is not received, but that packet will be retransmitted as the sender times out.

Problem: Receiver gets packets # 0 1 2, and send ACKs that get lost. Then it expect packets 3 0 1 (wraps around after 3). Since the sender did not get ACKs, it sends the OLD 0 and not the new!

Same problem for receiver but not for sender: Things work out. How does the receiver know that it's a new packet and not an old one?

Problem is too large window. It can be shown it should be < 1/2 of range of seq nums.

Summary:
* Timer: Lost packets. Can cause duplicates.
* Seq nr: Allows receiver to detect lost packets. Can also detect duplicates.
* ACK: To tell ok receive packet
* Neg Ack: To tell not ok receive packet
* Window: To increase utilization
## Connection-oriented transport: TCP
### The TCP connection
Conn oriented, both end systems have state vars, handshake.
Notice only end systems know about TCP, routers only see datagrams.

Full duplex. Point to point (single sender single receiver). Send buffer. Maximum segment size.
### TCP segment structure
Header: Source and dest port #. Checksum. Seq #: Says which byte in the stream is the first one in this payload. ACK #: The seq number the sender expects from the receiver next, this is called cumulative ack. Receive window. Header length field.
### Round-trip time estimation and timeout
TCP uses timeout+retransmit. But how long should the timeout be?

The RTT is estimated all the time during TCP. It is averaged over past samples and the new, with a weight typically:

EstRTT = 0.875 * EstRTT + 0.125 * NewSampleRTT

A std deviation is also kept track of. Then the timeout is based on the estimate + 4 * DevRTT.
### Reliable data transfer
Timer is set for oldest unack'd seq nr. So when window base updated (because ACK received for > oldest seq nr) restart timer. If timeout retransmit. If timeout double the timeout interval to be safe. Double each time, until things work, then go back to using est and samples.

You can detect packet loss before timeout by noting duplicate ACK. This happens if receiver gets out-of-order seq nr, it will resend ACK for how much it knows so far.
### Flow control
Receiver have receive buffers that might get filled (if in golang you don't <- from the chan).
In order to prevent this TCP uses speed control.

How to know when this should be done? The sender has a receive window (full duplex --> both have this). Underway they tell each other how much spare space is in the window.
### TCP connection management
First client send SYN with a random starting seq nr.

Server makes buffers and variables, and sends OK connection. Sets SYN = 1, ACK = client seq nr + 1, and a random starting seq nr for itself. This was a SYNACK.

Getting SYNACK the client also buffers and variables for conn. Sends ACK reply and now SYN = 0 and shit can start as normal.

Protect againts SYNFLOOD by not allocating shiz before the last handshake part (then need cookies).

What if host gets TCP SYN on a socket it does not accept conns on? It sends a RST flag set response. "PLS DO NOT RESEND".

So what can happen to a port scanner for example?
* Get a SYNACK "oh hey"
* Get a RST "pls no"
* Get nothing "my firewall pwnd u"

## Principles of congestion control
### Approaches to congestion control
* End to end congestion control:
No network layer help is given, and this is the case for TCP. Must be inferred from behaviour. Triple ACKs and packet loss etc. makes TCP shrink window size.
* Network assisted congestion control: Network layer (routers!) give feedback.
Now routers can directly send choke packets (HALP). Also the receiver sends feedback.
## TCP congestion control
Want to
* Limit rate sending
* Notice congestion
* Have an algo for changing send rate when congestion occurs
How to limit send rate? Just have a congestion window which limits max unack'd packets.
Then we get ~ (CongestionWindow / RTT) bytes/sec.

TCP is self-clocking: It increases the congestion window quickly when ACKs come in quickly, and slowly when they come in slow.

The algorithm three parts:
* Slow start: Start of with tiny congestion window, grow exponentially with ACKs. Each time a loss event happens, we start over, and set a threshold...
* Congestion avoidance: When we get here we have about cwnd/2 size of when we started losing packets. Now we are more conservative. A small linear increase. When lost event, go into next mode.
* Fast recovery: cwnd increased for each duplicate ACK...

Zooming out, this works generally in an additive-increase, multiplicative-decrease fashion.
### Fairness
If avg transmission rate for each conn is R/K, where R is bps and K is # conns.

If two connections and one has higher throughput, when loss occurs both ends will decrease throughput by a factor of two. This means it will eventually even out.
# Chapter 4: The Network Layer
Now for host-to-host instead of end-to-end.
* Forwarding: Transfer packet from inc link to out link within single router
* Routing: All of network's router involved
## Introduction
### Forwarding and routing
Each router has a forwarding table. This maps which outlink a packet should go. The table gets info from the packet header to determine this.

We also need connection setup, handshaking between routers, for some functionality.
### Network service models
Which services could be offered?
Guaranteed deliver, guaranteed deliver with time bounded above, in order delivey, guaranteed bandwidth, guaranteed max jitter, security.

The internet offers about none of these.
## What's inside a router?
Input ports. Switching fabric, connects in to out ports. Output ports, puts shit on out links.
Routing processor which does routing protocols.

It's like a roundabout where input ports put cars into switching fabric which puts em on outport.
### Input processing
Each input port has a shadow copy of forwarding table. Each port does forwarding. Then now centralized bottleneck.
Forward table uses longest prefix match.

Also need to queue if switching fabric is busy. Also we do phyiscal termination w link layer.
Need to update some relevant fields in packet too.
### Switching
Done via mememory (and other techs). Then the packet is copied into memory, analyzed.
Also single bus, crossbar bus.
### Output processing
Queuing, encapsulation, line termination.
### Where does queueing occur?
Can happen when many input ports want to forward to same output.

Can happen when the switching fabric can not accomodate all input ports being active.

How much buffer needed? Rule of thumb is RTT times link capacity.

HOL = head of line blocking. This occurs when a packet on input is being blocked because output is not available, but the packet behind the head of line would have been let through.

This can fuck up things.
## The internet protocol (IP): Forwarding and addressing in the internet
### Datagram format
IPv4 datagram: IP version, header length, TOS, datagram length, IP fragmentation flags (deprecated), TTL, protocol (indicates UDP/TCP etc), header checksum, source+dest IP, options (extended header), payload.

So 20 bytes of header without options. In addition to TCPs 20 bytes, this gives 40 bytes total overhead per packet.

What if the link-layer on your output port does not support the size of the datagram you got? Split into fragments. These must be reassembled, and flags indicate how to do this (by saying if more is coming and offset and ID).

IPv6 deprecates fragments!
### IPv4 addressing
About 4 billion total unique addr.

223.1.1.0/24 means that the leftmost 24 bits out of 32 decide the subnet addr (this is a subnet mask).
A network can have several subnets. The one above could also have 223.1.2.0/24 etc etc and each subnet can have many clients (and routers...).

This format a.b.c.d/x tells that x bits are the prefix of the addr.
These x bits are considered by outsiders, only insiders need to check the total-x bits.

255.255.255.255 means for a router to broadcast to all hosts on same subnet.

How to obtain a block of addrs? Say an isp has got a.b.c.d/20. It can then use three bits: a.b.c.d/23 for a total of 000->111 = 8 different blocks to give to organizations.

### DHCP
Auto-allocation of IP addr.
Say a DHCP server takes charge of a.b.c.d/23 --> 32 - 23 = 9, 2^9 = 512 addrs. Each time a student connects lappytoppy then give random IP addr from pool and update.

What happens when a client connects to a network?
Four steps.
* DHCP server discovery. Send UDP port 67 ask for DHCP server. Send to 255.255.255.255.
* DHCP responds also to 255.255.255.255. Sends a IP addr, lease time.
* Client chooses from maybe several of these offers. Then sends a DHCP request msg.
* Server responds with DHCP ACK.
### NAT
Now we can have 10.0.0.0/24 in a private realm == only makes sense in that network. Not visible to outside.
NAT comes in when needing to get/receive to outside.
To the outside world NAT router looks like a single IP addr.

Then we need a NAT translation table of course. How else to know where to deliver shit?
A host on 10.0.0.0/24 sends datagram to some outside world IP with some source port. The NAT takes this and adds another available port # from its table.

This is kinda fucky though. Ports should be assigned to processes not hosts. Also this breaks other encapsulation things.
Use IPv6 instead!

P2P also gets a bit fucky.
### UPnP
Universal plug and play. Makes a NAT "hole". Allows external hosts to init comms to NATed hosts, using TCP or UDP. Tries to remedy P2P fuckery.

### Internet control message protocol (ICMP)
Reports errors between hosts and routers.
Is sent as datagrams. Has type and code. Can be stuff like TTL expired, echo request, destination port unreachable etc.
### IPv6
Now we have 128 bits addr instead of 32. Also anycast: A datagram can be delivered to a group of hosts.
Fixed 40-byte header.

Flow label?

No longer fragmenation. No longer checksums.

Tunneling: Putting the whole IPv6 datagram into payload of IPv4 (compatibility shit).
### A brief foray into IP security
IPsec can offer integrity of data, origin auth, and cryptographic agreement ...
## Routing algorithms
Default router = the first a host sends to. Now called source router for host, dest router for default destination router.

Now we do graph theory. We can do global routing if we know all info, or iterative if decentralized.

Can also look at static vs dynamic = slowly or often changing.

# Chapter 5: The link layer: Links, access, networks, and LANs
Two types of link-layer channels:
* Broadcast channels (connects multiple hosts in WLANs, sat-nets etc.)
* Point-to-point comm link, e.g. between two routers long dist or between office computer and nearby Eth switch
## Introduction to the link layer
* Node (layer2): Hosts, routers, switches, wifi APs
* Links: Connect adjacent nodes
* Link-layer frame: Transmitting nodes put datagrams into these and transmits on link
### The services provided by the link layer
* Framing: Datagram put into data field, additional headers. Structure decided by link layer protocol.
* Link access: MAC protocol = medium access control p.) specifies the rules by which a frame is transmitted
* Reliable deliver: Used in WiFi often, cuz error prone. Uneccessary overhead for fiber, coax etc.
* Error detection and correction: Sophisticated! Can not only detect but correct.
### Where is the link layer implemented?
In a network adapter, NIC = network interface card.
Physcial transmission--> controller --> PCI bus to CPU, memory etc. Here transm and contrl is part of the network adapter.
## Error-detection and -correction techniques
When encapsulating add EDC = error detection + correction bits. Check these at receiver side. Then you MAY detect bit errors.
### Parity checks
Use a single parity bit, then: If using even parity make sure all bits (data + parity) # of 1s is even. If odd parity then odd # of 1s.

However errors often happen in bursts, and then this method may fail up to 50% of the time.

Can also go 2D and place data bits in cols and rows, then you can detect and correct.
### Checksumming methods
Treat d databits as k-bit integers, sum them. Use sum as error detection bits. Internet uses 16-bit ints. Then 1s complement is taken.

This is used in software (transport layer) cuz simple and fast. Not as good as cyclic. But cyclic needs hardware. So cyclic is used at link-layer.
### Cyclic redundancy check (CRC)
Also called polynomial codes.

Want to send d-bit piece of data D. Need to agree on r+1 bit pattern called generator G. MSB of G must be 1.

The sender chooses r bits R, append to D such that the d+r bits are divisble by G without remainder. So the error check is done by checking if dividing by generator gives nonzero.

Append is done by: D 2^r XOR R. So leftshift D to make room for R, and (X)OR it.

This means we must have D 2^R XOR R = nG, since no remainder.

How-to: Take D 2^r and divide by G, then you have n (the result) and R (the remainder).
Then you can take nG and XOR the result with R. Then again you will get D 2^r.
## Multiple access links and protocols
Use multiple access protocols to determine who speaks when etc.
If many nodes speak at the same time, we get collision.

Want that if R bps is max, if one node wants to send that node has R bps throughput. If M nodes want to send they have R/M bps each.
### Channel partitioning protocols
Can do time-division multiplexing (each node gets a time slot per interval).
This has problems. What if only one node has anything to send?

Can also frequency-division multiplex where the channel is divided farily with bandwidth R/N amongst nodes.

Can also do code division multiple access: Now each node has a special code which a receiver must know about. Then we can actually transmit simultaneously.
### Random access protocols
If collision, retransmit after a random delay.

**SLOTTED ALOHA**

Divide time such that a slot equals time to transmit one frame. If node has something to transmit, at beginning of next slot transmit entire frame. If collision, then for each subsequent slot retrenansmit if probability p happens. If not try again next slot.

So we can go full chimpanzee with R bps for each node. A problem is that only when one node is active do things go well, else shit gets tangled, or everyone does nothing because p failed in retransmitting.

Efficiency? Send probability p, not send (1-p). If only one send, then p(1-p)^{N-1} happened between N nodes. Since this can happen to any N node, we have success-scenario Np(1-p)^{N-1}.
Diff wrt to p to get best value to use for p, and let N go to infty to find efficiency (turns out to be 0.37).

**ALOHA**

Slots require all nodes to sync when to start transmitting (start of slot).
If we do not do that we just send shit and if collision, wait a "frame transmission time" unit and do retransmit if p not happen.

This reduced efficiency with half, and is the price to pay for decentralized ALOHA protocol.

**CSMA**

Carrier sense multiple access. Now we sense the channel for usage, if in use we stop. However because propagation takes time collision may happen anyway. Then stop, wait random time prob p etc., retransmit.

How long to wait? A number is picked at random from a set that grows exponentially with number of collisions. Then wait that number scaled with max rate of link.

Efficiency: Low propagation means --> 1, high transmit time --> 1 (since we do productive work).
### Taking turns protocols
**Polling protocol**:

Master node polls slaves round-robin. So no empty slots or collisions (if a node has nothing to say go on, so it's not exacly like time-slot allocation).

Problem: If master fails, we fukd.

Examples: Bluetooth, 802.15. 

**Token-passing protocol**:

No master. You have a token. You always pass the token to the next node in line if you have nothing to say. If you want to say you say a max # of things.

Problem: If a node fails the whole shebang can fail.
### DOCSIS: The link layer protocol for cable internet access
It means data-over-cable service interface specifications.

Uses FDM for downstream channels to houses. TDM with minislots (which are requested by houses) for upstream. 
Cable access network: Connects several thousand residential cable modems to a cable modem termination system.
## Switched local area networks
### Link-layer adressing and ARP

**MAC addresses**

Host network adapters have MAC addresses. Typically 6 bytes long. Does not change (but it can be by software).

So adapters on a LAN put MAC addrs to send stuff to other adapters. FF:FF:FF:FF:FF:FF is the broadcast MAC.

Why use this? To be independent. Network adapters need not only work for IP, so why should they have IP addrs only?

**Address resolution protocol (ARP)**
ARP: Translates IP addr to link-layer addr

So a sending host uses ARP to translate a destination IP addr to a MAC addr, so that it can deliver to the right network adapter?

Maintain an ARP table with IP addrs and MAC addrs and TTLs. Need to know a new MAC addr? Broadcast an ARP query to the broadcast MAC addr.

**Sending a datagram off the subnet**
You specify the MAC addr of the destination, but the IP addr of the router which is between you and the outside subnet.
### Ethernet
**Ethernet frame structure**

* Data field: Up to 1500 bytes
* Dest addr: MAC of dest
* Source addr: MAC of source
* Type field: Multiplexing of network-layer protocols, since other things than IP exist
* CRC: If fail discard.
* Preamble: 8 bytes with static data which is used for sync and "wake-up".

**Ethernet technologies**
Can have different speeds, physical medium...
Need repeater if long distance.

Standardized gigabit ethernet is pretty cool. Uses fiber or cat 5 UTP cabling.
### Link-layer switches
Gets frames link layer and forwards them. Buffers. Transparent to hosts and routers on subnet.

**Forwarding and filtering**

Filtering: Determines if forward or drop

Forwarding: Where to send shit (which outlink)

Both use switch table. If get frame w dest MAC addr, three cases:
* No entry --> broadcast frame
* Is entry and the associated interface is same as from where arrived --> discard
* Is entry, interfaces not same --> forward

**Self-learning**

The switch fills its own switch table.

**Properties of link-layer switching**

* Collisions eliminated: The switch separates the physical possibility of collision.
* Can use different speeds at either side of switches
* Eases net management

**Switches versus routers**

Switches are plugandplay. Routers have optimal routing. Both have traffic isolation.
#Chapter 6: Wireless and mobile networks
## Introduction
Will learn about IEEE 802.11, 3G.

Identify elements in a wireless network:
- Wireless host: End system
- Wireless links: Bringing coverage areas together? Also other hosts.
- Base station:  Send and receive data to and from hosts connected to it. Cell towers and access points.
- Network infrastructure: The larger network

Some types:
- Single-hop, infrastructure-based: Like class room cafè library
- Single-hop, infrastructure-less: Like Bluetooth
- Multi-hop, i: Wireless mesh networks
- Multi-hop, not i: Mobile ad hoc networks, if a vehicle is a node: vehicular ad hoc network.

## Wireless links and network characteristics
We have signal strength attenuation. Interference from other sources. Distorted signals.

We can get a better signal to noise ratio by increasing signal strength, which is costly. This reduces the bit error rate.
## WiFi: 802.11 Wireless LANs
802.11x: The x sets freq range, data rate ...
### The 802.11 architecture
BSS = Basic service set. This is as an example an access point plus several clients.

**Channels and association**

An AP has an SSID and a channel number. The channel number decides where to operate (frequency). 802.11 operates from 2.4 GHz to 2.485 GHz.

APs send beacon frames to announce themselves. SSID and MAC addr.

Can do passive scanning (look for beacon frames) or active scanning = send probe request and listen for responses.
### The 802.11 MAC protocol
Use random access for allowing conversation. CSMA(so idle sensing etc.) with collision avoidance. So not detection. Also use linklayer ACK and retransm.

A source waits a small interval (DIFS) sends data if idle, destination waits a short interval (SIFS) and sends ACK.

Can also do a "Request to send" and "Clear to send" (host to AP, AP to host) scheme to avoid collisions. This can avoid two hosts not seeing each other (and thus not being able to sense idle) while still being connected to same AP and causing collisions.
## The IEEE 802.11 frame
Payload (IP datagram ARP...), CRC. Source addr, dest addr (wireless station to recv), addr to router interface needed to conn to other subnets, seq # to figure out (re)transm, a lot more (duration, encryption? to, from, RTS, CTS ,...)
### Mobility in the same IP subnet
If you move yer lappytoppy from one BSS to another, which has same IP subnet. How the fuck does this maintain TCP connections etc.
NP if switch connects BSSs, but problem if router.
### Advanced features in 802.11
- Rate adaptation (based on lots of ACKs or few ACKs)
- Power saving (go to sleep for 99 msec stay awake for a msec to get beacon frame then sleep again)
## Cellular internet access
### An overview of cellular network architecture
- GSM = Global system for mobile communications.
- 1G: Telephone
- 2G: Digital voice
- 2.5G: Voice and data
- 3G: Higher speed
- Cellular: Because base transceiver stations (towers) are placed in cells to cover area

So you have a set of BSSs which are now hexagon-cells containing towers, which talk to a BSC (base station controller which does paging = assign shit to users) which again talks to a MSC (mobile switching center, which does authentication etc.) which eventually talks to public telephone network.

MSC also does call establishment, handoff, teardown...

GSM 2G uses FDM and TDM (both freq band division and time slots).
### 3G cellular data networks: extending the internet to cellular subscribers
Two nodes:
- Serving GPRS support node: Deliver datagrams to/from mobile nodes
- Gateway GPRS support node: Last point before entering the big interwebs

Now a RNC talks to the towers instead of the BSC.
The RNC can talk to both the packet-switched internet via SGSN, or the circuit-switched cell voice net via MSC.
### On to 4G: LTE
Long Term Evolution. Wow cool man.
Assign time slots over several freqs in a TDM FDM fashion.

# Chapter 7: Multimedia networking
## Multimedia networking applications
Multimedia network app = one that employs audio or video.
### Properties of video
- High bit rate

Video compression uses redundancies:
- Spatial redundancy: The redundancy within a given image. E.g. lots of one color.
- Temporal redundancy: Repetition from image to subsequent images.

Compression is used to make any bit rate, and also to make multiple versions of the same video.

Also on the fly when doing Skype shizz etc.
### Properties of audio
- Audio sampled at some rate
- Each sample is rounded to some value; quantization
- If 8000 samples per sec and 256 possible quantities (1 byte per sample) then 64kbps

This is called PCM = pulse code modulation. Typical for speech.

PCM not much used on interwebs. Then compression is used. MPEG 1 layer 3 == MP3 often used.
At 128 kbps it preserves audio well.
### Types of multimedia network applications
**Streaming stored audio/video**

Stored as in it existed before as in it's not live.
- Streaming: Means playing back a part while getting later parts by dl
- Interactivity: Can pause, reposition playback etc.
- Continuous playout: Want this so pls no "buffering"

Want good throughput.

**Conversational voice/video-over-IP**

Can have some loss; loss-tolerant. Delay-sensitive.

**Streaming live audio/video**

IP multicasting.
## Streaming stored video
Buffering is important. It absorbs varying throughput and delays and such.
### UDP streaming
Tries to match client consumption rate, and pumps out vid chunks.
This can be heavy on networks since to congestino controlino.

Uses special transport packets according to Real-Time Transport Protocl = RTP.

Uses a separate control connectino in order to indicate pause, resume etc.

Problems:
- Hard to maintain constant playback rate
- Needs a media control server (for pause play and that)
- Many firewalls block UDP
### HTTP streaming
Is used. Just HTTP GET the file and dl it asap and start playback when ya have enough in buffer.
Don't need the separate control line thang. Also can be weird without some special techniques since TCP is reliable.

Prefetching: Dl higher than consumption rate.

Remember server has send buffer client has recv buffer.

A HTTP byte-range header can be used to tell server where clients wants to skip to in a vid.
### Adaptive streaming and DASH
Vid encoded in many versions.

DASH = Dynamic adaptive streaming over HTTP.

Manifest file: Has info about the several versions and their URL.
### Content distribution networks
CDN. Many servers in many places, no single point of failure.  Can be private (owned by Google) or third-party (hired by Netflix).

Server placement philosophies:
- Enter deep: Get LOTS of locations in many area, getting as close as possible to end user.
- Bring home: Instead of getting inside access ISP, make a few superclusters.

To serve video interception of DNS is used. 

How to select cluster?
- Geographically closest
- CDN can send probes to local DNSs to do measurements
- Can use IP anycast, which does weird things to determine closest to client not to LDNS
### Case studies: Netflix, YouTube, and Kankan
Netflix uses lots of third-party. Runs cash and registration themselves. Sends about 4 sec chunks of vid at a time. Runs algs to determine quality of next chunk.

Youtube. Directs you to CDN lowest RTT unless it is loaded heavily.
Whole vid is not fetched.

Kankan. Uses P2P. Uses UDP causing lots of traffic in Chinawebz.
## Voice-over-IP
Send at 8000 bytes per sec, make chunk every 20 ms and set it over UDP = 160 bytes.
### Limitations of the best-effor IP service
- Packet loss: If a router has a full buffer or something. Why not TCP? Retransmission is not OK because to high end to end delay.
Can use FEC = forward error correction to help a bit. Also packet loss < 10% might be okay.
- End to end delay: The sum of delays on the way. < 150 msec ok, < 400 msec meh, > 400msec discard.
- Jitter: The effect of varying delays between packets
### Removing jitter at the receiver for audio
- Prepend chunks with timestamps
- Delay playout

Can do fixed playout delay, just wait some time and then playback. If a packet has not arrived on schedule it is lost.

Can also do adaptive playout delay. Then you estimate avg net delay.
### Recovering from packet loss
- Forward error correction: Send more info than needed, and use it to reconstruct damages. Or send an additional shit-version of the audio stream.
- Interleaving: If you group chunks in units of four, and then send a chunk with unit 1, 5, 9, 13 instead of 1, 2, 3, 4 then if a chunk is lost only a single unit from several reconstructed chunks are lost which sounds much better.
# Chapter 8: Security in computer networks
## What is network security
Want:
- Confidentiality: So need encryption
- Message integrity: So need to retransmit fuckups
- End-point auth: How to know if a person is who they say they are
- Operational security: Firewall etc need to not compromised company network etc
## Principles of cryptography
(Note: Not RSA @ 684-688 etc)

We got: Plaintext/cleartext --> encryption alg --> ciphertext.

Ka(m) = key A used to make ciphertext out of message m.

Kb(Ka(m)) = decrypting using key B.

### Symmetric key cryptography
Keys are identical and secret.

Block ciphers: Process data in *k* bits, each possible string maps to some output which maps it back if you know the block cipher table.

Can also break up 64 bits in 8 bit sets, process them, put them back into 64 bits, then loop this for a while.

DES, 3DES, and AES are popular block ciphers.

Cipher block chaining involves doing an XOR operation between the previous ciphertext and the current message.
### Public key encryption
Your public key is known to everyone.
Private only to you.
So anyone can encrypt something using a standard algorithm fed with your public key.
Then using a decryption algorithm and the corresponding private key, you get the message back.

The amazing thing Diffie & Hellman did was to make an algorithm to create such a keypair where one can be freely shared and one had to be private.

How do we know who sent us a message? Digital signature.
## Message integrity and digital signatures
Now we know how to be confidential. How about authentication, integrity?
This means we want to know:
- Who sent msg
- Msg has not been fuked with on the way
### Cryptographic hash functions
Takes an imput m and computes a fixed-size string H(m), a hash.
We also need:
- It is infeasible computationally to find different messages x and y s.t. H(x) = H(y).
 
A checksum does not meet this requirement, as it is not that difficult to make a msg m and hash H(m) also conform to msg n, hash H(n).

Popular today: MD5 (in short: padding, 64-bit representation of msg length, init accumulator, loop and process in words of 16 bits), also SHA-1.
### Message authentication code
How do we do?
- You have msg m. Concat it with s, make m+s (s is the auth code, a string of bits). Calculate the hash H(m+s) (called the message auth code), and send the message tuple (m, H(m+s)).
- Someone receives this msg, to them it looks like (m, h). Since they know secret s as well, they can check if H(m+s) = h.

This fixes the problem of only sending (m, H(m)) where it is easy to stop this midway and send another msg (g, H(g)). That can't be done if the bad person is missing the secret code s.

How do we give two parties a common secret s?
### Digital signatures
You now use the private key to encrypt! Then anyone can decrypt it with your public key. This verifies that you had to be the one to encrypt it.

It's costly to encrypt large messages though... encrypt the hash of it instead (which is fixed length)!

How does one know that a public key is actually the public key of the person/entity you think? Public key certification can be done by a Certification Authority.

You then attach your certificate which you got from a trustworthy CA to your msg/request whatevs. Then a person can check the certificate if it belongs to you.

Wait why can't a bad person send a nice person's certificate?
## Securing e-mail
### Secure e-mail
A nice procedure:
- Sender creates a session key and encrypts msg using that (because encrypting using public key is slow)
- Sender then uses receiver public key to encrypt session key and concats that to package
- Receiver splits what was received and decrypts session key using private key, then decrypt msg using session key

How to add authentication and integrity check?
- Instead of sending the msg like above, send the concated pair m (the msg) and the signed version of the hash of the msg. This means you hash the msg, encrypt using private key, then add that to the unencrypted msg.

The only thing to add now is a CA to verity whose public key it is you are actually getting.
## Securing TCP connections: SSL
SSL = Secure socket layer = enhanced TCP. TSL = modified SSL ver3 also exists and is standardized.

What does it do? Adds confidentiality (encryption), data integrity (who knows CRC maybe?), server authentication (so ya not scammed by poser site), and client authentication (ppl not buying in ya name).
### The big picture
**Handshake**

As a client you need to establish TCP conn, and verify the server is rly the server u want. After that you send a master key used to make symmetric keys between cli/srv for the session.

So do TCP SYN, TCP/SYNACK, TCP ACK to start session. Then do a "SSL hello" where srv returns a certificate. If that checks out then you encrypt a master key using the public key which only the server can decrypt.

**Key derivation**

Four keys are generated from the master key. One each for encrypting, one each for integrity verification.
(But both parties have all four).

**Data transfer**

Wouldn't be smart to encrypt on the fly, how then would one check integrity?

So SSL makes records (splits the stream into known length chunks probably). How then do we know they arrive in the right order? You don't. Therefore also use sequence numbers.
### A more complete picture
Can use nonces (wtf are they?) to defend against someone sniffing the interaction and sending the exact same msgs again (to place more orders etc.).

Nonces are probably some kinda random seed thing which makes two same sequences of actions (ordering an item) not have identical msg contents.
## Network-layer security: IPsec and virtual private networks
IP security protocol = IPsec.

Provide "blanket coverage" at the network layer, so all packets are not private (TCP, UDP, ICMP..).
Can also do auth, integrity, replay-attack prevention.
### IPsec and virtual private networks (VPNs)
Instead of creating a physical private network you do VPN, which uses the public webz to do the same thang.

A gateway router within the VPN makes a IPsec datagram out of a normal IPv4 datagram before entering the world wide web.
## Securing wireless LANs
So the goal is to make things as secure as when using wires, but wirelessly.
### Wired equivalent privacy (WEP)
Authentication: 
- Host request auth by AP
- AP sends 128-byte nonce val
- Host encrypts using symmetric key (known to both beforehand)
- AP decrypts nonce checks if same as sent if yes OK u good

Data encryption happens by using a 40-bit symmetric key plus a 24-bit init vector (which changes each frame) and feeding it into a key sequence generator which outputs encrypted data (also adding CRC).
## Operational security: Firewalls and intrusion detection systems
### Firewalls
Three goals:
- All traffic from out to in and in to out must pass through the firewall
- Only authorized traffic as local security policy dictates can pass
- The firewall is immune to penetration

**Traditional packet filters**

Examines each datagram, checking things like: Source, dest, protocol, port source+dest, TCP flag bits, ...

Example: All TCP starts with ACK set to 0, all other has ACK set 1. So don't want externals connection to internal servers? Drop all incoming with ACK set 0.

**Stateful packet filters**

Tracks all TCP connections using a connection table.

Can then easily block any packet not part of an ongoing conn.

**Application gateway**
This is needed to allow for user logins and sessions and such advanced features.
