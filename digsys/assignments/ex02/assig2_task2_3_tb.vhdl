entity tb is
end tb;

architecture behaviour of tb is
  component Qe
    port(A, QN: in BIT;
         Q: out BIT);
  end component;

  component QNe
    port(B, Q: in BIT;
         QN: out BIT);
  end component;

  for Qe_0: Qe use entity work.Qe;
  signal A, QN, Q : BIT;

  for QNe_0: QNe use entity work.QNe;
  signal B, Q, QN : BIT;

begin
  Qe_0: Qe port map (A => A, QN => QN, Q => Q);
  QNe_0: Qe port map (B => B, Q => Q, QN => QN);

  process
    type pattern_type_Qe is record
      A, QN : BIT;
      Q : BIT;
    end record;

    type pattern_type_QNe is record
      B, Q : BIT;
      QN : BIT;
    end record;

    type pattern_array_Qe is array (natural range <>) of pattern_type_Qe;
    constant patterns_Qe : pattern_array_Qe :=
      -- Enter your test values here, in the order of the record
      ();

    type pattern_array_QNe is array (natural range <>) of pattern_type_QNe;
    constant patterns_QNe : pattern_array_QNe :=
      -- Enter your test values here, in the order of the record
      ();

  begin
    for i in patterns_Qe'range loop
      -- Set all inputs
      A <= patterns_Qe(i).A;
      QN <= patterns_Qe(i).QN;

      B <= patterns_QNe(i).B;
      Q <= patterns_QNe(i).Q;
      wait for 1 ns;
      -- Check all outputs
      assert Q = patterns_Qe(i).Q;
      report "Q wrong val" severity error;
      assert QN = patterns_QNe(i).QN;
      report "QN wrong val" severity error;
    end loop;
    assert false report "end of test" severity note;
    wait;
  end process;
end behaviour;
