library ieee;
use ieee.std_logic_1164.all;

entity circuit_tb is
end circuit_tb;

architecture behaviour of circuit_tb is
  component circuit
    port(A, B: in std_logic; Q, QN: buffer std_logic);
  end component;

  for circuit_0: circuit use entity work.circuit;
  signal A, B, Q, QN : std_logic;
begin
  circuit_0: circuit port map (A => A, B => B, Q => Q, QN => QN);

  process
    type pattern_type is record
      A, B : std_logic;
      Q, QN : std_logic;
    end record;

    type pattern_array is array (natural range <>) of pattern_type;
    constant patterns : pattern_array :=
      (('U', 'U', 'U', 'U'),
       ('1', '0', 'U', 'U'),
       ('1', '0', '0', 'X'),
       ('1', '0', '0', '1'),
       ('0', '0', '0', '1'),
       ('0', '1', '0', '1'),
       ('0', '1', '0', '0'),
       ('0', '1', '1', '0'),
       ('0', '0', '1', '0'),
       ('1', '1', '1', '0'),
       ('1', '1', '0', '0'));

  begin
    -- 0
    A <= patterns(0).A;
    B <= patterns(0).B;
    assert Q = patterns(0).Q
      report "Q(0) wrong" severity error;
    assert QN = patterns(0).QN
      report "QN(0) wrong" severity error;

    -- 1
    A <= patterns(1).A;
    B <= patterns(1).B;
    assert Q = patterns(1).Q
      report "Q(1) wrong" severity error;
    assert QN = patterns(1).QN
      report "QN(1) wrong" severity error;

    -- 2
    A <= patterns(2).A;
    B <= patterns(2).B;
    assert Q = patterns(2).Q
      report "Q(2) wrong" severity error;
    assert QN = patterns(2).QN
      report "QN(2) wrong" severity error;

    -- 3
    A <= patterns(3).A;
    B <= patterns(3).B;
    assert Q = patterns(3).Q
      report "Q(3) wrong" severity error;
    assert QN = patterns(3).QN
      report "QN(3) wrong" severity error;

    -- 4
    wait for 10 ns;
    A <= patterns(4).A;
    B <= patterns(4).B;
    assert Q = patterns(4).Q
      report "Q(4) wrong" severity error;
    assert QN = patterns(4).QN
      report "QN(4) wrong" severity error;

    -- 5
    wait for 10 ns;
    A <= patterns(5).A;
    B <= patterns(5).B;
    assert Q = patterns(5).Q
      report "Q(5) wrong" severity error;
    assert QN = patterns(5).QN
      report "QN(5) wrong" severity error;

    -- 6
    A <= patterns(6).A;
    B <= patterns(6).B;
    assert Q = patterns(6).Q
      report "Q(6) wrong" severity error;
    assert QN = patterns(6).QN
      report "QN(6) wrong" severity error;


    -- 7
    A <= patterns(7).A;
    B <= patterns(7).B;
    assert Q = patterns(7).Q
      report "Q(7) wrong" severity error;
    assert QN = patterns(7).QN
      report "QN(7) wrong" severity error;

    -- 8
    wait for 10 ns;
    A <= patterns(8).A;
    B <= patterns(8).B;
    assert Q = patterns(8).Q
      report "Q(8) wrong" severity error;
    assert QN = patterns(8).QN
      report "QN(8) wrong" severity error;

    -- 9
    wait for 10 ns;
    A <= patterns(9).A;
    B <= patterns(9).B;
    assert Q = patterns(9).Q
      report "Q(9) wrong" severity error;
    assert QN = patterns(9).QN
      report "QN(9) wrong" severity error;

    -- 10
    A <= patterns(10).A;
    B <= patterns(10).B;
    assert Q = patterns(10).Q
      report "Q(10) wrong" severity error;
    assert QN = patterns(10).QN
      report "QN(10) wrong" severity error;

    assert false report "end of test" severity note;
    wait;
  end process;
end behaviour;
