library ieee;
use ieee.std_logic_1164.all;

entity circuit is
  port(A, B: in std_logic;
       Q, QN: buffer std_logic);
end circuit;

architecture behaviour of circuit is
begin
  Q <= A nor QN;
  QN <= B nor Q;
end behaviour;
