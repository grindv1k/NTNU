# Textbook
Volnei A. Pedroni, "Circuit Design and Simulation with VHDL", Second Edition, MIT Press, Cambridge, Massachusetts, USA, 2010.
## Chapters
### Mandatory
Chapter 1 through 11.
### Useful info + designs may be used on the exam
Chapter 12 through 17.

# Misc
* All lectures, all papers handed out in lectures
* All guest lectures
* Assignments and term project
