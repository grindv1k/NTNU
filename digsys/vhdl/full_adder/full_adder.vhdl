entity full_adder is
  -- i0 and i1 inputs, ci is the carry in
  -- s is the sum output, co is the carry out
  port(i0, i1 : in bit; ci : in bit; s : out bit; co : out bit);
end full_adder;

architecture rtl of full_adder is
begin
  -- The sum will output a 1 if we have an odd number of positive inputs (at
  -- least 1)
  s <= i0 xor i1 xor ci;
  co <= (i0 and i1) or (i0 and ci) or (i1 and ci);
end rtl;
