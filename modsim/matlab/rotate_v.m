function rotated_vector = rotate_v(axis, vector, a)
% Input: Axis to rotate about, the vector to rotate, and the angle to
% rotate (in degrees).
if ~isvector(vector)
    error('Input must be vector')
end

if axis == 'z'
R = [cosd(a) -sind(a) 0;
    sind(a) cosd(a) 0;
    0 0 1];
elseif axis == 'y'
R = [cosd(a) 0 sind(a);
    0 1 0;
    -sind(a) 0 cosd(a)];
elseif axis == 'x'
R = [1 0 0;
    0 cosd(a) -sind(a);
    0 sind(a) cosd(a)];
else
    error('Choose axis x, y, or z')
end

rotated_vector = R * vector;

end

