%% 
clear all
close all
clc

vec_i = [3 2 1]';
a = 30; % degs

% Let's use Z-X-Y Euler thang.
vec_b = rotate_v('z', vec_i, a)
vec_b = rotate_v('x', vec_b, a)
vec_b = rotate_v('y', vec_b, a)

% Now can we rotate it back?
vec_c = rotate_v('y', vec_b, -a)
vec_c = rotate_v('x', vec_c, -a)
vec_c = rotate_v('z', vec_c, -a)