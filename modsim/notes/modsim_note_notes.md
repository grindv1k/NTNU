# Notes from the master lecture-notes pdf
Goal: There are 120 pages. There will be these days to study: 5., 6., 7., 8., 9., 10., 11., 12., 13., and 14.
This is 10 days. So about 12 pages per day will suffice. This will cover everything from the lectures.
As a buffer try for 20 pages?

# Pages 01-20 (not including ERK-methods):
## Passivity:
It is simply checking if the energy function is such that energy is absorbed in the system (over time).
```
P = u*y
```
if larger than 0, power is delivered TO system, else it is extracted FROM system.
Integrating you get total energy stored over time, or produced to environment.

* If you parallell-connect passive systems, the result is also passive.
* If you "negative-feedbackly"-connect passive systems, the result is also passive.


## Positive real
Given a system u --> H(s) --> y. If the system is passive, H(s) will be positive real, and vica versa. Cool!.
Checking for positive realness:
1) Check Re(poles) <= 0.
2) Insert s = jw. Check if for all w the real part is >= 0 (do not do this if s = jw = system pole).
3) For poles on im-axis: Do a Res calculation. Need Re(resuling calc) > 0.

## Simulation
* A one step method is one where you can express y{n+1} = y{n} + h f(y{n}, t{n}).
* A method is of order p if we can show the method is "similar" to a Taylor expansion up til a certain order and the last term is O(h^{p+1}). 
Another way of stating it is that the order of the error is equal to h^x where h^x is taken from where the similarity to the Taylor expansion fucks up. The method order then is one less than that x value.

## Stability
* Show that the magnitude of the stability function is leq 1.

## Euler methods
* Euler, where the next value is the previous value plus a time step times the current function value.
* Modified Euler, where you go a half time step ahead and evaluate the function there. 
* Improved Euler, where you use the average of the current and next time stepped function eval.

# Pages 22-40 (including ERK; not including L-stability)
## ERK explicit
### Stabily
* Define test system; rewrite all stage computations in terms of this.
Then rewrite to vector form, and reforumlate y{n+1} as well, combine, and you end up with stabily function.

Since explicity, stabily function goes to inf when lambda h goes to inf.

## Chapter 3: Elektromechanical systems
Inertia motor times angular accel motor equals motor torque minus load torque.

Motor to rotor power equals motor angular velocity times motor torque (this I give to you son)

Rotor to load power is then motor angular velocity times load torque

### Gears
* Linear transforming angular vel with gear turns
* Linearly dependent torque as well (less torque if ang vec went up)
* Rotation to translation: w{m}T{l} = F v (force times velocity. Both are power)

### Constant field DC motors
Armature (the "box" that holds the DC motor within the stator) circuit:
voltage u{a}, current i{a}, resistor R{a}, inductance L{a}, and "rotor" voltage e{a}.

Has torque output T = K{t}i{a}; linear dependence on current.

Energy function shows the system is passive (voltage to current) if the load is viscous.

### Implicit RK methods
Why bother? Stiff systems! Spread in eigenvalues.
A huge spread in eigenvalues would give a shit-tonne of time-steps using Euler.
A definition of stiff systems can actually be systems that suck ass using explicit methods.

Butcher array can now have vals on diagonal.

This one is order 2 and has non-stable area of a unit circle with 1 + j0 as center.

Trapezoid method: Order 2 as well.
Stable: All of left hand plane.

Implicit mid-point method: Also order 2; same stability as trapezoid.

### RK methods: Stability
* Aliasing: Two test systems, different eigenvalues. I think aliasing happens when the slower system is sampled at "perfect intervals" in comparison to the quicker system. Then it can "look normal" but be oh so wrong.

* A-stability: A method is just this if |R(h lambda)| <= 1 for real values of eigs <= 0 for all step sizes.
So just look at the stability graph, is it stable for all Re <= 0?

No explicit method is! Some implicit are!

# Pages 40-61 (including L-stability; not including vectors)
* L-stable: Damps out quick oscillations. You are L-stable if you are A-stable and stabily magnitude (h j w) goes to 0 as w goes to infty.
## Errors and stepsizes and events and stuff
### Padè approx
Want methods to be close to the exact solution, this involves the function e^s.
ERK methods have Taylor expansions of e^s as stabily function, IRK methods have Padè-approx methods.

If Padè approx it is easy to see if L- or A-stable.

### Estimating local err
Not sure what this is about. I suppose you can't know how wrong your method is without having an exact solution. You then want an estimate, and you use this in order to change step sizes.
### Autoadjusting stepsize
Have an algorithm that uses local error estimates. You also have a current step size, and a tolerance. If the largest current error estimate is about the same as tolerance, do nothing. If it's big, reduce h, if it's low increase h.

The algorithm works as a PI-regulator.
### Interpolated solutions, detection of events
What do we want to do? Between t{n} and t{n+1} there was an event. This time is called t'. This must be equal to t{n} + alpha times stepsize. Let's call g(y, t) = 0 the event function. Want to find (g(y{n}(alpha), t{n}+alpha stepsize)) = 0.

## Suddenly motor models
### Compressibility
Given by the bulk modulus. Can make a diffeq involving pressure and volume using this.
## Transmission lines
If the length is kinda small, use the bulk-modulus developed diffeq.

If not then ya gotta take care of propagation shit as well. The propagation is given by sqrt(bulk / density), so it varies. We can use partial diff eqs for this using mass- and momentbalances.

We can then find a characteristic impedance.

Using Laplace we end up finding a propagation-operator. This might be the one used in Tilpdat to calculate over/under-lapping of reflecting waves?

Now using some wave eqs and this operator we get some "nice" equations describing the motion of the waves.
## Friction
Several models:
* Viscous: Linear proportional to velocity
* Coulomb: Like a step-function at v = 0, and constant on either side of the y-axis (different signs)
* Static = stiction: Like coulomb but close to v = 0 the friction is slightly larger (and then eases).
* Stribeck: Empirical. A bit funkier, looks kinda hyperbolic
### Troubling shit concerning v close to 0
If v = 0 then no friction force using coulomb. But then v should increase? OK then v > 0 but then suddenly friction kinda large so v should retard? But then v = 0 and then ...

What do? Karnopp-model. This handles this mess, using saturation and sign functions and blah.
### Friction models and passivity
Is the input, velocity, passive in relation to its output, friction?

Static friction is passive, since the friction and velocity has same sign and therefore pos semidef, and is larger than -E0 = 0 (I think).
## Dynamic fricton models
### Dahls
You now have a differential describing the friction. The static solution ( rate = 0 ) gives the coloumb model.
The dynamic version is a bit funkier and has a time constant. Good for simulation, can have drift.

Passive?
Ya.
### LuGre
A bit more involved. Static solution = Stribeck. Is conditionally passive.
Has same pros and cons as Dahls, but more realistic dynamically.
# Pages 62-81 (including vectors; not including kinematic diffeqs)
## Vectors
So a coordinate-free vector has an arrow above it. This is when you have to describe it in terms of the unit vectors. So the components of the vector are multiplied with these unit vectors.

The coordinatevector then has a bar under it. It is expressed as a (example) 3x1 vector, no arrows in sight (implied?).
## Dyadics
Some background: The link between ang vel and "spinn"=angular momentum is that
ang-mom = M w, where M is the inertia matrix and w is the angular vel vector.

Can now define the dyad M^{arrow}, which is sum i=1-->3 sum j=1--> m{ij}a^arrow{i}a^arrow{j}.
So you get m11, m12, m13; m21, m22, m23 ...
This allows us to write the ang-mom on coordinate-free form as h = M w (everything now coord-free).

I think this is the main point. Having a notation that has cool linear properties and allows us to write stuff in coord-free form.
## Rotation matrix
* Part of SO(3): Is 3x3, R^T R = I, det(R) = 1.
The vector v^a is obtained by multiplying the coord-transform-matrix R^a _ b with v^b.
So this matrix is the coord-transform from b to a.

R^b _ a = (R^a _ b)^T = (R^a _ b)^{-1}

Simple rotation: About an axis (which stays put).
## Transformations
T^a _ b : Coord transform into a from b. These matrices given on the form:

(Rb^a rab^a)

(0 0 0 1)

## Euler angles
Roll = angle about x-axis, pitch = about y-axis, yaw = about z-axis.
## Angle-axis parametrisation
You have a rotation matrix which is orthagonal. Then eig = 1. Then you can find an eigenvector k s.t. using the rotation matrix on this k vector results in k.

Then k is the same in both reference a and ref. b.

Now k then is the rotation axis. Claim "every rotation can be described by a vector k and an angle theta".

Then a vector say q is rotated to from a vector p by multiplying p by cos theta, cross product sin theta k and p etc. etc...
## Euler parameters
Built from angle-axis' k and theta. If you define eta = cos theta/2 and epsilon = k sin theta/2 which are the two parameters in a quaternion) then you have the property that eta squared plus eps times eps is 1.

Building rotation matrices from these paramaters has the properties:
* R(-eta, -epsilon) = R(eta, epsilon)
* R(eta, eps)^T = R(eta, -eps)
## Angular velocity
wab is the ang-vel of b relative to a.

In angle-axis we have that wab is simply d/dt theta times k vector.

A vector differentiated must have the diff in reference to either a or b. 
d/dt u seen from a is d/dt u + wab x u.
# Pages 81-100 (including kinematic diffeqs; not including Lagrange EoM)
* Translation: r dot is v
* Rotation: How to find time derivate of rotation matrix? We can use skew symmteric version of angular velocity.
What about Euler angles (derivatives) and parametrisation version?

## Euler angels
We can find the diff eqs by finding a matrix Ea and then inverting it (does not exist when angle is 90 deg).
This is found by time deriving one angle, then rotating and time der another, then two rotations and the last time deriv.
This is equal to the angular velocity when using Euler angles!
## Euler paramteres
You now have that a rotation matrix changing coords from b to a is given by eta and epsilon. Their derivatives are given by a couple equations involving the angular vel.
## Rigid body kinematics
Wikipedia: The tangential speed of a point r(t) is w x r.

From an inertial frame the velocity of a point p in a rigid body (frame b) if there is a stationary point o in the body is:
v{p} = v{o} + w{ib} x r

If the point r in the body is not stationary then we got
v{p} = v{o} + {b}d/dt r + w{ib} x r

Of course! Just take right hand rule starting with w then r, thumb is then this velocity.

Acceleration of point p is given by accel of point o, then angular acc cross r + angular vel cross (angular vel cross r).

The second term here is tangential acceleration, since it just time diffs the tangential velocity. The second term cross products ang-vel with tangential velocity. What does that give you? Well the ang-vel points upwards, the second term points to the left (it should because sign related) so points inwards.
## Newton Euler EoM for rigid bodies
* mass = integral of dm over body
* center of mass = integral of vectors to points times dm over body, divided by mass (an avg of sorts)

So what does Newton say?
* dm times acc{p} = dforce{p}. Integrating we get m a{CoM} = F{bc} (forces on body ref CoM)

We have N-E EoM about CoM:
* {i}d/dt Pc = F{bc} translation
* {i}d/dt h{b/c} = T{bc} rotation
The first is derivative of bevegelsesmengde, second is diff of spinn and equals "resulting torque" about CoM

## Inertia matrix
* M{b/c} constant when referred to b, not when referred to i.
Parallell axis thm we use the CoM and relate it to somewhere more intersting.
## N-E EoM on coordinate form
We now make matrices and lump the resulting forces and torques together.
You then get the eq m times acc{b} = F{b}, and inertia matrix{b} times ang acc{b} + rotation about CoM = Torques{b}.
## D'Alamberts prinsipp
* Active forces
* Føringskrefter == ? forces; maybe driving forces
You have that m d^2/dt^2 r = F + F{c} (Newton), where F{c} in a train example is the acceleration inwards.
The acc inwards F{c} times a delta times r has dot product zero.

Then F{c} delta r = (m d^2/dt^2 r - F) delta r = 0

Then also the sum of all rk, mk = 0 which is this principle.

Brukt i statics?
# Pages 101-120
Lagrange EoM: d/dt (dL/dqi{dot}) - dL/dqi = taui

For rigid bodies!

Interesting example where pendulum EoM is described both in the inertial ref and body ref.

Interesting example of fluid shizz, strømning dV woah etc.
