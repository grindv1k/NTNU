# Modeling and Simulation for Automatic Control
## 1.1 - 1.3: State-space models, linearization, transfer functions
Throwback regtek: A linear system (Ax + Bu; Cx + Du) is stable when the eigenvalues of A have <= 0 real parts for simple eigenvalues, and < 0 for multiple eigenvalues, and asym stable if ALL Re(eigz) < 0.

Stability for linearized non-lin systems: If lin system asym stable, so is the nonlin.
If the linsys is stable, but with 1 >= pole(s) at im-axis, nonlin MAY be stable. If unstable linsys then unstable nonlinsys.

Impulse response: Comes from u(s) = 1, which comes from laplacing the dirac.
Then y(s) = H(s) --> y(t) = h(t) = implpuse response.

Unit step function has transform 1/s, --> y(s) = H(s) 1/s.

## 2.1 - 2.2: Frequency response, damping fast dynamics
### Freq response
H(jw) gives the frequency response of a system. The magnitude of this is of interest, the phase as well.
A summary of Regtek follows on page 44.
Given L(s), the loop transfer function, we have the following requirements for stability:

|L(jw180)| < 1,     angle L(jwc) > - 180

which is clearly understood (how to use) if reading that page.
### Fast dynamics
If you have a small time constant T{a}, then you can approximate it away from your state space model by doing some assumptions here and there. Good going dawg!
## 2.3 - 2.4: Energy functions, passivity, positive real transfer functions, storage functions, interconnecting passive systems
We try to find an energy function for a system. Looking at the derivative, we find that if it is negative the energy is decreasing.

Lyapunov methods include expressing the system on 1/2 xT P x form, and making sure P is posdef. Then the lower bound for V(x) is proportional in x and the smallest eig of P. This means we need to make the state go to zero.

Passivity theory main point: If a total system can be described as an interconnection of passive subsystems in parallell or in a feedback connection setting, the system as a whole will be passive (= cannot produce energy; will absorb energy).

Passivity as defined looks at the integral of a powerfunction u y. If it is always positive, energy is given to the system and it is passive. If it is always >= -E0, then it may give SOME energy (initially stored) to the outside world, but is passive nonetheless.

Positive realness is linked to passivity. If H(s) pos real, then that system is passive.

A PID controller is passive.

Storage function: This is handy shit. System is passive if dV/dt = u^T y - g(x), g(x) pos-semidef.
## 3.1 - 3.6: Electromechanical systems
### 3.2 Electrical motors
There exists basic equations for electrical motors analogue to ma = F, and you can use gears to have more fun.

You can transform a motor into doing translatory work.

You can look at torque vs. motor speed to investigate stability.

If the voltage and current is of same sign, you deliver power (motor), else you generate.
### 3.3 DC motor; constant field
You have a circuit making a const field or a perm magnet. Then the torque is Kt ia, ia is the armature current (that circuit has Ra, ia, ua, La, and ea). The motor induced voltage ea.

You can then make a nice model. You have that if the load attached is passive, the whole shebang is passive.
### 3.4 DC motor control
You can pretty easily have a desired current, speed, and position in a DC motor.
### 3.5 Motor and load with elastic transmission
What's this about? You can have a motor which has flexibility in the shaft or the gearbox, or the load is connected by wires or some shazz. How to control this?

You then have a "spring and damper" between the motor shaft and the load shaft, which complicates the model a bit.

You get some crazy bode-diagrams out of this.

A tip is to use feedback from the ang-vel of the motor, not the load.

You still have passivity in several cases (resonance in loads, two motors ...).
### 3.6 Motor and load with deadzone in the gear
You then have split cases based on the angle. Have to use event-detection in sim.
## 4.1 - 4.4: Hydraulic motors
Most important: 4.2.2, 4.2.3

### 4.1
Hydrostatic: when motor driven by pressure work of a flowing fluid

Hydrodynamic: when momentum of fluid that flows past is used
### 4.2.2 Flow through a restriction
Here the valve equation is given (vol/sec), turbulent.

Also given the flow if laminar (Reynolds very low, < 10).
### 4.2.3 Regularization of turbulent orifice flow
It is problematic using the valve eq for turbulent flow in simulations; it has a derivative which goes to infty as flow gets low. What do? Make a model that transitions smoothly between laminar and turbulent but is ugly as fuck.
## 4.2 onwards
There are valves with spools n shiz

Models for different setups of hydraulic motors (rotational, elastic loads, linear ...) are given.

Some transfer functions for setups are given. P-controller rotation motor, matched symmetric valve ...

Symmetric cylinder matched and symmetric valve ...

Pump controlled hydraulic drive with P-controller ...
## 4.5 - 4.6: Transmission lines
### 4.5
So if a transmission line is reasonably long, we are scared of reflections and time delays.

PDEs:
* The change of pressure due to time is proportional to the (negative) sonic velocity, the line impedance, and the volumetric change due to position.
* Change in volumetric flow due to time is prop to -c/Z0 times pressure per position - F over density.

Can rewrite this in Laplace domain using the wave prop operator. We now have a model.
You also have to choose a friction model.

The prop operator and impedance changes due to assumptions and friction.

We can find solutions to wave variables if we use the Laplace model. The we get an eq describing wave moving in positive x dir, and one in the neg dir.

You use the boundaries as conditions to making use of this.

The rest of the chapter is going real deep into these models.
### 4.6 Lumped parameter model of hydr line
You can model a hydraulic transmission line by dividing it into sections. Then you take a mass balance for each section.
Additionally you set up a momentum balance.

You can set up state space models for this setup depending on what you want as input, and then you have described how the whole transmission line looks!!?!
## 5: Friction
Static friction models:
* Static frction = elastic deformation of asperities; Dahl effect
* Boundary lubrication = Low velocities give no fluid lubr and shear forces dominate
* Partial fluid lube = STribeck effect
* Full fluid lubr = lubricant film thicker than asperities, no solid contact occurs, viscous.

Coulomb friction is the standard Ff = Fc sgn(v), v != 0 model.

Stiction = spiky force (higher than moving) at rest

Viscous friction = now we scale friction with velocity. Typically it should be of a nonlinear nature.

Stribeck effect = the viscous friction decreases. This happens because the lube film thickness increases with velocity, and the shear forces of the film are smaller in magnitude than those of the asperities.
## Why hard to sim and control?
* Need detection of zero velocity (the model changes then)
* The models do not describe all observed dynamic effects (pre-sliding displacement, frictional lag ...)
## Dynamic
Avoiding some of the above issues (switching) by using a dynamic model. One of these is the Dahl model.
It is a bit simplistic. Does not describe the Stribeck effect.

The LuGre model is a dynamic friction model based on modelling friction as bristles along a surface.

Does not account for true stiction.

A dynamic model trying to capture both true stiction and pre-sliding is the Elasto-Plastic model.
## 6.1 - 6.9: Rigid body dynamics
The cross product u x v (coordfree) is equiv to u{skew}v (coordinate form)

There are a lot of identities concerning vectors and the cross product, dot product.
### 6.3 Dyadics
While vectors, column vectors are used a lot in physics, it turns out some matrices representing pairs of vectors are also useful.
### 6.4 Rotation matrix
Vector superscript means that the vector is given in that reference frame.

R^a{b} coordinate transforms a vector from ref b to ref a.
Another interpretation is that the matrix rotates a vector p to the vector q such that q{b} = p{a}.

The last point I think means this: If you stand in ref a and look at vector p, it is built from your ref a building blocks (unit vectors). There exists a rotation such that if you were looking at vector p from ref b instead, you would use the same amount of ref b unit vectors to arrive at the vector.

There are a number of propeties like R{ba}=R{ab}^-1=R{ab}T, and that it is part of SO(3), and det(R)=1.

Homogeneous transformation matrices:
T{ab} describe pos and orientation of frame b relative to frame a.
### 6.5 Euler angles
A rotation matrix describes the orientation of frame b with respect to frame a.
We want to represent a rotation matrix with a set of parameters.

Roll-pitch-yaw:
Here you describe a rotation by three simple ones.
Troubletown when one angle is 90 degs.
### 6.6 Angle-axis
Euler angles not so fit for some control applications and can complicate expressions. Therefor another representation of rotations is sought.

Here we use an axis which is common to both bases. Rotate around it, and specify the components of k (the common axis) in order to have a complete discription of the rotation.
### 6.7 Euler parameters
These parameters have some nice properties (no singularities, and rational (not trig) expr).
Can be found if you have a rotation matrix by using Shepperd's method.
### 6.8 Angular velocity
The definition comes from the fact that the skew symmetric version satisfies the time derivative of the rotation matrix times the transpose of the rotation matrix.

The angular velocity vector wab describes the angular veloicty of frame b, relative to frame a.

In computing the angular velocities of simple rotations, one arrives at the result that a simple rotation about a constant axis is the time deriv of the angle about that axis (as a vector pointing along the axis).

The angular velocity of a composite rotation matrix is given by the sum of the angular velocities.

### 6.9 Kinematic diff eqs
When using Euler angles you have three simple rotations. The rotation of frame d relative to a is given by the individual contributions, but two of them are not references to frame a. We then have to transform the two.

We end up with equations for each angle in the frame we choose.

Using Euler parameters the result is quite ugly.
Others as well... only Euler angels to save us.
## 7.1 - 7.7: Rigid body dynamics cont.
Now the examples begin.
Newton Euler EoM shown.

Virtual work.
This is introduced because we want to eliminate forces of constraints (which may not be important to actual solutions). 

Generalized coordinates.
## 8.1 - 8.2: Rigid body dynamics cont. the awakening
Here the V = T - U equation is derived, and examples are given.

It is actually based on L'Ambert's principle, so yay that was used.

## 10.4: Reynold's transport theorem
A welcome friend when we have rate change of mass, momentum, or energy in a control volume.

The control volume is material, spatially fixed, or a general CV with moving boundaries.

Material = following a set of particles.
## 11.1 - 11.4: Balance laws cont.
### 11.1: Mass balance
Lots of examples given, basic mass balances in out of pipes, compressible, fixed, material, CV.
A chemical multi-component system example is also included.
### 11.2: Momentum balance
Bernoulli streamline, irrotational, and lots of examples.
### 11.3: Angular momentum balance
Examples.
### 11.4: Energy balance
Very useful examples of energy balance and mass balance in tanks leading to differential eqs. for temperature.
## 14.1 - 14.8: Numerical solution of ODEs
### Key topics
Order, local error, stability function, linear test system, ERK methods, IRK methods, Butcher arrays, stabily regions, stiff systems, aliasing, A-stabily, L-stabily, Padè-approx and RK methods, auto-adjust step size, event detection, multistep solvers (principle), index 1 DAE systems.
### 14.1
Just an introduction.
### 14.2
Global error: Solution error resulting from computation from t0 to tn+1.
Local error: Solution error resulting from computation from tn to tn+1.

For a linearized system we have that it is stable if the magnitude of the stability function is <= 1 for all eigenvalues.
The eigenvalues are taken from the Jacobian.

Linear test function: Take ydot = lambda y. Then use your chosen method on that differential equation. Then put it on yn+1= R(h lambda) yn form. Now you can check if |R| <= 1.
### 14.3 Euler methods
Example 215 gives a very simple 2nd order system example.

Example 217 a bit more involved 2nd order system. It is now confirmed as fuck. If you have a > 1 order system, and several step computations, how to know where to insert k1,1 or k1,2 or k1,3 (when finding k2,1 and k2,2 and k2,3)? Just look at the function definition. If e.g y3dot = y1, then you insert k1,1. If y3dot = y2 + y3, then insert k1,2 and k1,3.
### 14.4 ERK methods
Definition of the explicit ERK methods (0 on the Butcher diagonal and above!) is given. Def of Butcher array as well.

Note ERK has an explicit stability function. Just insert different parts of the Butcher array.

Note also that ERK methods have a "solved" formula for the stabily function (no need for matrices and determinants).
### 14.5 IRK methods
Stiff: Hard to solve with explicit methods (usally (?) due to a large spread in eigenvalues).

These methods have stability regions which are not as enclosed around the origin as the ERK methods. Examples: Implicit Euler stable everywhere except a unit circle around 1, Trapezoidal stable in the whole LHP.

We can also quite easily compute the stabily functions for IRK methods.
### 14.6 Stabily of RK methods
There is a concept called stiffly accurate methods. This means the method will damp out quick oscillations, and the solution will mainly correspond to the slow dynamics.

If Butcher A is nonsingular, and b is equal to the last A row, then stiffly accurate.

The stabily function R(s) is more general and for implicit and explicit RK methods; the Padè approx.

If you have identified the Padè approx of a method, you can easily determine A- and L-stabily using a formula p550.
### 14.7: Auto adjust stepsize
If the Jacobian has varying eigenvalues (large diff in size) then a constant step size is not recommended (as it has to be very small to accomodate fast dynamics).
One should then change the step size "on-the-go" accordingly.

This concept is done by calculating the solutions using methods of order p and another of order p+1. The difference between the two is seen as a measure of error. Then define a tolerance. If the error at the current iterate is "bad" then reduce step size.
### 14.8: Implementation aspects
Using interpolation one can make "dense" outputs which are continuous.

Event detection is done by introducing a function and setting it equal to zero. Inserting numerical solutions and checking for sign changes between steps will indicate an event has happened, and the event itself can be found by solving a "interpolation like" equation between the steps.
## 14.11: Numerical solution of ODEs cont.
A multistep method may use previous function evaluations to approximate the next (it uses history).

They have funky stability regions. Looks like water-leaef-things.
## 14.12: Numerical solution of ODEs cont. the awakening
A index 1 system can be transfoermed into index 1 system by change of vars?

Need the Jacobianiish matrix to be nonsingular.

Probably use the exercise solution for this shizzzzz
