# Overall
* KKT, optimal conditions, QP, MPC, convexity, LP, Nelder-Mead, SQP, MPC in detail, Simplex, feasible points, start conditions, merit function (and details), 1st and 2nd order conditions, LU decomp, transform into std. format, dual (esp. KKT), LICQ, no inequality, LQ change in u constraint, LQ infeasible, LQ what if constr nonlin, classify problems, word problem into LP/QP etc.
# 2009
## Convexity and KKT
* Use KKT, show grad at local min is zero
* Use 2nd order conditions
* Given max-problen, find sol, obj fun val, Lagr mult, is convex?
* Sketch convex feasible set, and eq / ineq constr
* Sketch non-conv
* Properties of convex
* Lagr multipl for ineq when inactive
* Are eq cond always active at local sol
* Last KKT condition what it do
## Optimal control, MPC
* How many free vars in a given setting
* No ineq: What is the Lagr, how KKT look like, using LQ contr what type is that
* Inf horizon. How K computed. Two conditions on closed loop contr being stable
* Why is MPC successful in industry. Challenges? If slowly varying, use PID or MPC?
* What type of opt-problm solved by most MPC
* Why estimator box in figure
* MVs and CVs (manipulated vars, controlled variables (outputs)) in Helileb?
* Optimal feedback guarantee no constraints violated?
* Can badly tuned controllers make things go bad?hjj
## LP
* Define basic feas point
* Simplex method?
* LU-decomposition, how does that look?
* If LU-decomp exists, why efficient?
# 2010
## Nelder-Mead
* Derivative free?
* Given inits, compute reflection point.
* Show how N-M computes next iter-point given f(x{reflect}) = ...
* If we have contraction, what happens
* If nonlin large problem, why use N-M not SQP? Why should we NOT use N-M?
## Optimality conditions and nonlin-problems.
* General form of obj and constr in QP?
* Give examples of convex QP; non-convex QP
* Give two conditions related to f and feasible so we have a convex prblm
* Prove that if f convex then local min = global min
* Given a min-problem, show KKT satisfied. Check 2nd order conditions
* Suppose we use SQP for a given problem: What should merit function be, what is the purpose of mu, will merit decrease, will the obj. fun decrease?
* Sketch a sequence that might have been found by SQP
## Optimal and MPC
* Inf horizon. How K? How to stable? What about eigenvals of A+BK
* Given a discrete non-lin dynamic system. Formulate opt problm
* What do in order to make MV not change too much (LQ)
* Explain MPC use figure
* How to get lower/upper bounds without infeasibling
# 2011
## LP, Simplex
* LP == convex? Strictly?
* Simplex, grad of f or Lagr?
* Compute KKT or this (...)
* Find basic feasz. Check KKT
* Transform LP into standard form
* Dual problem. Show KKT equal
* Discuss primal vs dual correspondencej
## MPC, optimal control
* Solution to inf horiz LQ shown. Prediction horiz divided into two parts. Why constr = 0 after some time, how this affect us
* Discuss the opt problem to compute constr in (...)
* Suggest alg for solving the standard LQ setup if the constraints were nonlin
* How to choose prediction horizon (length)?
* Control input blocking?
* Suggest values for tuning LQ
## Misc
* NeldaerM: Inside contraction?
* Unconstr min problm, optimal step length find it
* LICQ hold given (...), KKT, 2nd order necc, 2nd order suff?
# 2012
## Misc
### Optimization
* General constrained. When is convex
* Reformulate as max problem
* Merit function for SQP
* Prove local min global min
### LP
* Basic feasib
* Transform into standard form
* Dual KKT equal
## QP
* Define active set
* Assume no ineq. KKT conditions. Formulate as matrix eq
* Assume no ineq. Conditions for unique global sol
* Newton dir, descent dir, one step converge
* Formulate word problem as QP
## Optimal control and MPC
* No ineq. Lagr, KKT, type controller
* Inf horiz. How K? Conditions for stable?
* MPC principle
* Success of MPC? Why based on lin models when most stuff is nonlin
* How to handle infeasible
# 2013
## QP
* Make standard form
* Convex?
* KKT derive
* Compute x solution and Lagr mult.
## Optimization problem formulation
* # of decision vars?
* Convexity
* # of eq and ineq constr
* Given a number of problems, classify them and suggest alg
## Misc
* LICQ
* Sign of Lagr mult
* SQP: Merit, mu, exact
## MPC and dynamic optimization
* Why called open loop
* Dimension of z
* Can eliminate decision vars. pros cons
* Write down alg for output feedback lin MPC
* How to avoid infeasible
* What if x{t+1} nonlin
* What if not nonlin but linear time varying
# 2014
## Misc
* Convexity
* Feasible set
* Non-convex == hard?
* Simplex, how test if feasible is solution
* Simplex why LU makes easy
## NLP and SQP
* Give examples of 2 eq constr 3 ineq
* Sketch given eq constr. Convex?
* Sketch ineq. Convex?
* Linearize, show no feasible set
* Rewrite given thing as thing.
* Merit function. Mu, change in mu, exact.
* Maratos effect?
## MPC, dynamic optimization
* Open loop?
* z dim.
* Limit change in u wear and tear.
* How to choose Q and R
* Formulate state space describing estimator. Est. dynamics compare to MPC dynamics?
* Sketch control hierarchy (the triangle thing)
# 2015
## Misc
* Given problems, classify, convex, alg
* Active set method? Two examples pls
* How to start active set method
* LP transform into standard form pls
* Derive KKT pls for a given QP
* Given problem and solution, if you could perturb which constraint u choose
## MPC Optimal control
* MPC explain
* How to choose N
* Infeasible how to
* No ineq. Infty. Type of controller. How to find P. Why posdef P
* Using kalman to est states, what is structure called? Requirments for stabily
## NLP, SQP
* Derive KKT for given NLP
* Compute Hessian
* Given subproblem form, show QP is given by (...)
* Draw feasible reagion QP and contours of obj fun
* Why merit. Suggest one
* Show one iteration enough converge
# 2016
## LP
* Word problem --> standard form
* Simplex, feasible start?
* Convex the word problem?
* LU?
* Given new wordy info: Reformulate, type, convex?
* Given solution and Lagr multipl. What will an increase in ... give us (profit)?
## MPC and optimal control
* Quasi dynamic vs. dynamic optimization
* Given the standard LQ horizon finite problem:
Open loop? Write as LTI. Why max change in input? Type of opt probl? Nonlin instead, type of opt problm, which alg? Infeasible? n = ? Full space vs. reduced space
* Explain MPC through alg and fig
## Rosenbrock
* Compute grad and hess
* Show 1st and 2nd order cond satisf
* Formulate opti problem where Rosenbrock minimized
