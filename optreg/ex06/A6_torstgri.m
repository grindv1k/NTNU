% a6 torstein grindvik
clc
clear all
close all

%% Oppg1
Q = eye(2)*2;
R = 2;
A = [1 0.5; 0 1];
B = [0.125; 0.5];

[K, S, E] = dlqr(A, B, 1/2*Q, 1/2*R);

% K is the optimal gain matrix
% S is the Riccati eq solution
% E are the closed-loop eigenvalues

%% Oppg2
clc
clear all
close all
b = 2;
a = 3;

P = roots([1 1/4-2-9/4 -1/2]);
K = 2*P(1)*(1 + b^2*P(1))^(-1)*a