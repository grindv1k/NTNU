%
%% Assignment 5 revisited
clc
clear all
close all

A = [0 0 0;
    0 0 1;
    0.1 -0.79 1.78];
B = [1;
    0;
    0.1];
C = [0 0 1];

nx = 3;
nu = 1;
N = 30;

Q = [0 0 0;
    0 0 0;
    0 0 2];
r = 1;
R = 2*r;

% blkdiag "concatonates diagonally", so blkdiag(A,B) = [A ...; ... B]
% kron multiplies the 2nd argument into the first, while multiplying sizes.
% This means kron(eye(N), Q) will be 30*3 = 90 in size.
G = blkdiag(kron(eye(N), Q), kron(eye(N), R));
% G should be N*nx + N*nu = 90 + 30 = 120 tall and wide.

% Left side of Aeq; where we have I and -A entries
Aeq_1_I = eye(N*nx);
% diag can take a row vector of ones, and place is on the kth diagonal. -1
% tells it to place them on the diagonal below the main diagonal.
Aeq_1_A = kron(diag(ones(N-1, 1), -1), -A);
Aeq_1 = Aeq_1_I + Aeq_1_A;
% Right side of Aeq; where the -B entries are
Aeq_2 = kron(eye(N), -B);

Aeq = [Aeq_1 Aeq_2];
x = zeros(nx, N + 1);
x0 = [0; 0; 1];
x(:, 1) = x0;

% First make a vector [1; 0; 0; 0; ...] then kron it
Beq = kron([1; zeros(N-1, 1)], A*x(:, 1));

KKT_lhs = [G -Aeq'; Aeq zeros(N*nx)];
KKT_rhs = [zeros(N*nx + N*nu, 1); Beq];

% Solve KKT_lhs z_u_lambda = KKT_rhs
z_u_lambda = linsolve(KKT_lhs, KKT_rhs);

% Add the solved x entries to x.
% We have a column of x entries in z_u_lambda. We reshape every three
% entries into a column, then repeat
x(:, 2:end) = reshape(z_u_lambda(1:(N*nx)), [nx, N]);

% Make a fake input value to match the number of elements in x
u = z_u_lambda(N*nx + 1 : N*nx + N*nu);
u = [u; 0];

y = C*x;

t = 0:1:N;
finesse = 0.1;
t_fine = 0:finesse:N;

y_fine = pchip(t, y, t_fine);

% subplot(3, 2, 1)
% plot(t_fine, y_fine, 'Linewidth', 1.5);
% title('y');
% grid MINOR
%
% subplot(3, 2, 2)
% stairs(t, u, 'Linewidth', 1.5);
% title('u');
% grid MINOR

% Now let quadprog do it
z_u_lambda_qp = quadprog(G, [], [], [], Aeq, Beq);
x_qp = [x0 reshape(z_u_lambda_qp(1:(N*nx)), [nx, N])];
u_qp = z_u_lambda_qp(N*nx + 1 : N*nx + N*nu);
u_qp = [u_qp; 0];

y_qp = C*x_qp;
y_fine_qp = pchip(t, y_qp, t_fine);

subplot(4, 2, 5)
plot(t_fine, y_fine_qp, 'Linewidth', 1.5);
title('y QP');
grid MINOR

subplot(4, 2, 6)
stairs(t, u_qp, 'Linewidth', 1.5);
title('u QP');
grid MINOR

% Now with input constraint:
% -1 <= ut <= 1
LB = [-inf(N*nx, 1)
    -ones(N*nu, 1)];
UB = [inf(N*nx, 1);
    ones(N*nu, 1)];

[z_u_lambda_qp_bounds, ~, ~, iters] = quadprog(G, [], [], [], Aeq, Beq, LB, UB);
x_qp_bounds = [x0 reshape(z_u_lambda_qp_bounds(1:(N*nx)), [nx, N])];
u_qp_bounds = z_u_lambda_qp_bounds(N*nx + 1 : N*nx + N*nu);
u_qp_bounds = [u_qp_bounds; 0];

y_qp_bounds = C*x_qp_bounds;
y_fine_qp_bounds = pchip(t, y_qp_bounds, t_fine);

subplot(4, 2, 7)
plot(t_fine, y_fine_qp_bounds, 'Linewidth', 1.5);
title('y QP with bounds');
grid MINOR

subplot(4, 2, 8)
stairs(t, u_qp_bounds, 'Linewidth', 1.5);
title('u QP with bounds');
grid MINOR

% MPC
timesteps = 30;
x_mpc = [x0 zeros(nx, N - 1)];
u_mpc = zeros(1, N + 1);
for i = 1:timesteps
    Beq_mpc = kron([1; zeros(N-1, 1)], A*x_mpc(:, i));
    z_mpc = quadprog(G, [], [], [], Aeq, Beq_mpc, LB, UB);
    u_mpc(i) = z_mpc(N*nx + 1);
    x_mpc(:, i + 1) = A*x_mpc(:, i) + B*u_mpc(i);
end

y_mpc = C*x_mpc;
y_fine_mpc = pchip(t, y_mpc, t_fine);

subplot(4, 2, 1)
plot(t_fine, y_fine_mpc, 'Linewidth', 1.5);
title('y MPC');
grid MINOR

subplot(4, 2, 2)
stairs(t, u_mpc, 'Linewidth', 1.5);
title('u MPC');
grid MINOR

% MPC imperf model
A_imperf = [0 0 0;
    0 0 1;
    0.1 -0.855 1.85];
B_imperf = [1;
    0;
    0];
x_mpc2 = [x0 zeros(nx, N - 1)];
u_mpc2 = zeros(1, N + 1);
for i = 1:timesteps
    Beq_mpc2 = kron([1; zeros(N-1, 1)], A*x_mpc2(:, i));
    z_mpc2 = quadprog(G, [], [], [], Aeq, Beq_mpc2, LB, UB);
    u_mpc2(i) = z_mpc2(N*nx + 1);
    x_mpc2(:, i + 1) = A_imperf*x_mpc2(:, i) + B_imperf*u_mpc2(i);
end

y_mpc2 = C*x_mpc2;
y_fine_mpc2 = pchip(t, y_mpc2, t_fine);

subplot(4, 2, 3)
plot(t_fine, y_fine_mpc2, 'Linewidth', 1.5);
title('y MPC imperfect model');
grid MINOR

subplot(4, 2, 4)
stairs(t, u_mpc2, 'Linewidth', 1.5);
title('u MPC imperfect model');
grid MINOR

%%

% 3b

% Number of input blocks
inpblk = 5;

% Same bounds different dims
LB_inpblk = [-inf(N*nx, 1)
    -ones(N/inpblk*nu, 1)];
UB_inpblk = [inf(N*nx, 1);
    ones(N/inpblk*nu, 1)];

Aeq_inpblk_2 = kron(eye(N/inpblk), ones(inpblk, 1));
Aeq_inpblk_2 = kron(Aeq_inpblk_2, B);
Aeq_inpblk = [Aeq_1 Aeq_inpblk_2];
G_inpblk = blkdiag(kron(eye(N), Q), kron(eye(N/inpblk), R));
[z_inpblk, ~, ~, inpiter] = quadprog(G_inpblk, [], [], [], Aeq_inpblk, Beq, LB_inpblk, UB_inpblk);
x_inpblk = [x0 reshape(z_inpblk(1:(N*nx)), [nx, N])];
u_inpblk = z_inpblk(N*nx + 1 : N*nx + N*nu/inpblk);
% For the sake of plotting, make u as long as x
u_inpblk = kron(u_inpblk, ones(inpblk, 1));
u_inpblk = [u_inpblk ; 0];

y_inpblk = C*x_inpblk;
y_fine_inpblk = pchip(t, y_inpblk, t_fine);

figure
subplot(3, 2, 1)
plot(t_fine, y_fine_inpblk, 'Linewidth', 1.5);
title('y input blocking (constant width)');
grid MINOR

subplot(3, 2, 2)
stairs(t, u_inpblk, 'Linewidth', 1.5);
title('u input blocking (constant width)');
grid MINOR

% 3c
% Now we will annoyingly need to change the right part of Aeq again
% Lengths: 1 1 2 4 8 14
lengths = [1 1 2 4 8 14];
tmp = zeros(1, 6);
tmp(:, 1) = 1;
Aeq_inpblk2_right_1 = kron(tmp, -B);

tmp = zeros(1, 6);
tmp(:, 2) = 1;
Aeq_inpblk2_right_2 = kron(tmp, -B);

tmp = zeros(lengths(3), 6);
tmp(: , 3) = ones(lengths(3), 1);
Aeq_inpblk2_right_3 = kron(tmp, -B);

tmp = zeros(lengths(4), 6);
tmp(: , 4) = ones(lengths(4), 1);
Aeq_inpblk2_right_4 = kron(tmp, -B);

tmp = zeros(lengths(5), 6);
tmp(: , 5) = ones(lengths(5), 1);
Aeq_inpblk2_right_5 = kron(tmp, -B);

tmp = zeros(lengths(6), 6);
tmp(: , 6) = ones(lengths(6), 1);
Aeq_inpblk2_right_6 = kron(tmp, -B);

Aeq_inpblk2_right = [Aeq_inpblk2_right_1;
    Aeq_inpblk2_right_2;
    Aeq_inpblk2_right_3;
    Aeq_inpblk2_right_4;
    Aeq_inpblk2_right_5;
    Aeq_inpblk2_right_6];

Aeq_inpblk2 = [Aeq_1 Aeq_inpblk2_right];

[z_inpblk2, ~, ~, inpiter2] = quadprog(G_inpblk, [], [], [], Aeq_inpblk2, Beq, LB_inpblk, UB_inpblk);
x_inpblk2 = [x0 reshape(z_inpblk2(1:(N*nx)), [nx, N])];
u_tmp = z_inpblk2(N*nx + 1 : N*nx + N*nu/inpblk);
u_inpblk2 = zeros(N + 1, 1);
u_inpblk2(1) = u_tmp(1);
u_inpblk2(sum(lengths(1:1)) + 1 : sum(lengths(1:2))) = u_tmp(2);
u_inpblk2(sum(lengths(1:2)) + 1 : sum(lengths(1:3))) = u_tmp(3);
u_inpblk2(sum(lengths(1:3)) + 1 : sum(lengths(1:4))) = u_tmp(4);
u_inpblk2(sum(lengths(1:4)) + 1 : sum(lengths(1:5))) = u_tmp(5);
u_inpblk2(sum(lengths(1:5)) + 1 : sum(lengths(1:6))) = u_tmp(6);


y_inpblk2 = C*x_inpblk2;
y_fine_inpblk2 = pchip(t, y_inpblk2, t_fine);

subplot(3, 2, 3)
plot(t_fine, y_fine_inpblk2, 'Linewidth', 1.5);
title('y input blocking (constant width)');
grid MINOR

subplot(3, 2, 4)
stairs(t, u_inpblk2, 'Linewidth', 1.5);
title('u input blocking (varying width)');
grid MINOR

% 3e

for i = 1:timesteps
    Beq_mpc = kron([1; zeros(N-1, 1)], A*x_mpc(:, i));
    z_mpc = quadprog(G_inpblk, [], [], [], Aeq_inpblk2, Beq_mpc, LB, UB);
    u_mpc(i) = z_mpc(N*nx + 1);
    x_mpc(:, i + 1) = A*x_mpc(:, i) + B*u_mpc(i);
end
u_mpc(end) = u_mpc(end - 1);

y_mpc = C*x_mpc;
y_fine_mpc = pchip(t, y_mpc, t_fine);

subplot(3, 2, 5)
plot(t_fine, y_fine_mpc, 'Linewidth', 1.5);
title('y MPC input block (varying width)');
grid MINOR

subplot(3, 2, 6)
stairs(t, u_mpc, 'Linewidth', 1.5);
title('u MPC input block (varying width)');
grid MINOR