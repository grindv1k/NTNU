# 1: Introduction
Main concept: MPC.
# 2: Optimization
Background on optimization.
## 2.1: Classes of optimaztion problems
In optimization problems we have:
* Objective function
* Decision variables (real space most often for us)
* Constraints (equality, inequality)

**First order necessary conditions:**
Assume LICQ holds <--> active constraint grads are lin indep at solution.
Then we will have Lagrange multipliers for the lambdas such that:
* Lagrange grad = 0
* Constraints hold
* Lagrange multipliers are positive for inequalitie constraints
* Lambda times constraint (ineqs) at solution = 0.
The last condition: Either lambda is zero (the inequality is inactive), or the constraint is zero (which it is when active), or possibly both (which would mean the inequality is active but the objective function is insensitive to this fact?)

**Second order sufficient conditions**
Here we need the gradient wrt xx of the L function to be positive definite.
Really it only need be posdef in the directions of the critical cone.

Convex optimization problem: Function convex + feasible set convex.

Further we have the LP problem (everything linear, convx), and the QP problem (obj fun quadratic, convex if Q pos semidef (can be relaxed)).
Notice KKT is sufficient and necessary for LP, also for QP if convex.
## 2.2: Solution methods
Typical algorithm:
* Get init point and termination criteria
* While not terminte; compute next point; repeat

Simplex for LP and active set for QP both require feasible init points.
SQP does not.

When updating the point in an iterative scheme, it usually looks like:

zk+1 = zk + alphak pk

Where alphak is the step size (line search parameter) and pk is the direction to go.
This direction is decided on gradient info which can be: Steepest descent, Newton direction, Hessian. Quasi-Newton (Hesisan approximation) is also used.

Gradients are computed using finite differencing (Taylor, perturbations) and automatic differentiation (chain rule).

Simplex uses structural info. Jumps vertices.

We also have derivative free methods. Nelder-Mead == pattern search method.
# 3: Optimization of Dynamic systems
## 3.1: Discrete time models
This section describes that a discrete model can be found by finding the Jacobian matrices (inserted the stationary values).

This works best if the dynamic model is not insanely nonlinear.
## 3.2: Objective functions for discrete time systems
Here the objective function including all states and control variables on a prediction horizon.
We want to minimize the sum of these contributions.

The initial state is given, it is not a decision variable.
## 3.3: Dynamic optimization with linear models
Now the objective function is formulated using posdef Q and R etc., subject to the model, init stuff, bounds, input rate bounds.

No feedback here! Open loop.

We could also formulate some controlled variable that relies on x.
Perhaps j = Hx. Then reformulate. This would then include 1/2 j^T Qj etc.

The quadratic objective fun is widely used; reasons are because economical thangz are often linear in states and inputs, also because reference tracking is quite ez just but (x-xref) instead of just x in the obj fun.

It is easy to penalize control moves = change in input. Just add a 1/2 deltau^T Rdelta deltau term.
## 3.4: The choice of objective function in optimal control
This section discusses what a good response is. Is it when the Euclidian norm is smallest for u? For x?

Well we should consider both. Also we should be able to scale them. Remember the scaling factors are relative to each other.
### 3.4.1: Norms in the objetive function
The Euclidian norm is the most widely used norm in optimal control.
## 3.5: Optimal open loop optimization examples
Here we are introduced to writing out the state equation for each time instant in a pattern so that Aeq and beq is found.
Also written out are the bounds in terms of z.
## 3.6: Dynamic optimization with nonlinear discrete time models
What if the model is nonlinear? This would mean the same objective function, but xt+1 = g(xt, ut).
We now need an NLP solver. Can use SQP.
# 4: Optimal control
Now we want feedback control and dynamic optimization. MPC!

Then linear MPC. AKA LQ controller. (Just remove ineq constraints).

Then nonlinear MPC.
## 4.1: Model predictive control
So the idea is just to do what has been discussed so far, but at every sampling instant. Use the first control input in the solved sequence. Feed the current state to the next finite horizon problem.

If we use an estimator we now fix disturbances and model errors (to some degree).
## 4.2: Linear MPC
This was the "normal case". QP obj fun, lin constraints. Get state, solve QP from t to t+N, apply ut from solution.
### 4.2.1: Ensuring feasibility at all times
Fuckballs some heavy hitter goddamn shit-tornado made my copter fly too far and it outside what I have said is feasible. It did not know what to do so it ran "Copter.Explode(time.Now())".

To the objective function add:
p^T e + 1/2 e^t S e,

You have now introduced slack variables. You now have a slack variable e and tuning parameters p and the elements of S.


and to the bounds subtract e from xlow and add e to xhigh.

This allows our bounds to be a bit elastic, while we minimize these slackers in the obj fun whenever they are active.
### 4.2.2: Stability of linear MPC
A point is made out of the fact that just because the MPC is feasible at all times (no violations) does not mean it will be stable.

If we add xN = 0 (the last state) as a constraint, we now get closed loop stable if feasible at all times (and no model errors).

Choose a long enough prediction horizon so the dominant dynamics are captured.
### 4.2.3: Output feedback
We of course use an estimator as we do not know the actual state.
The estimator will be based on the Kalman filter, which gives the gain matrix associated with the output and our estimated output.

We make the estimator dynamics a lot quicker than the actual system. This is to minimize coupling between the two.

A little Kalman recap.
The model now includes process noise and the output has measurement noise. The Kalman gain is decided based on the nature of these. If the process noise (covariance) is much larger than the measurement noise (covariance) we rely on measurements. This means a large Kalman gain. If the measurement is very noisy instead, we use little Kalman gain (because we don't want that corruption spilled onto our estimator).

As a note there are also MHE = moving horizon estimators, which estimate states based on a backward horizon.
### 4.2.4: Reference tracking and integral action
To track a reference jt = H xt, you insert (jt++1-jt+1ref) in the obj fun.
Is it feasible? Look at the stationary solution (xt+1 = xt and solve). Then look at the js = Hxs, and substitute in us for xs. Is it possible to make yref out of that equation based on bounds? If not, infeasible.

How can you get integral action (to remove constant disturbances) into MPC?
You extend the model with a disturbance model. Then get an estimate of the controlled variable. Then use THAT in the objective function (along with the reference).
## 4.3: Linear quadratic control
Now to remove inequality constraints!
### 4.3.1: Finite horizon LQ control
Since we have no ineq constraint we can get an explicit solution.
This solution is used by the feedback controller: ut = -Kt xt.

Kt is the feedback gain matrix. It involves the Riccati matrix P, which must  be solved for t = N-1 and backwards.

We see here that r (weighting on input in LQ) is included in Kt, the controller gain. It is in the numerator. This explains how a low value for r makes us use lots of input, and vica versa.
### 4.3.2: Moving horizon LQ control
Now we use this info. Let ut = -Kt xt{hat}.

Then:
* Get xt{hat}
* Compute ut = -Kt xt{hat}

Repeat!

Notice we need not solve any QP problem, as we did when the formulation had inequalities.
## 4.4: Infinite horizon LQ control
Now we want something even easier. ut = -K xt, that is K does not vary with time!
### 4.4.1: State feedback infinite horizon LQ control
It's a thing that Kt eventually settles.

If we then have a long horizon (and assuming A, B, Q, and R time invariant) we can find a value for K that is constant.
Set let the horizon go to infty.

Now we have something that has infinite dimension. Can not solve it by KKT matrix solving.

Also we need the function to be bounded above, < infty. This means input must eventually go to 0.

We need the system to be stabilizable (which is implied by controllability).
Stabilizable means that unstable modes must be controllable.

Now we have LQR, linear quadratic controller.

We need to think about stability. We have already stated that we need (A, B) stabilizable. We also need (A, D) detectable, where Q = D^T D.

If that is the case, the closed loop system will be stable (asym).

Observability: If x{n} can be determined from the system model, its input and outputs.

Detectabiliy (implied by obsv): If all unobservable modes are asym stable.

If not detectable, how could we ensure the states go to zero if we can't know?

Notice that if A = [2 0; 0 0.5] and D = [1 0] then it is detectable. This is because the unobservable mode is less than 1.
### 4.4.2: Output feedback infinite horizon LQ control
Now we get to da gritty bidness.

ut = -K x{hat}.

Estimator bsaed on Kalman and all.

This system can be rewritten into [xt+1; x{hat}t+1] form, and you then see the eigenvalues are decoupled.
### 4.4.3: Stability of linear MPC with infinite horizon LQ control
Now we look back to the golden days of inequalities.

Can we say something sensible about stability here?

Note we must drive the states to the origin, else the objective function is unbounded.

TRIXXXXXXXXXXX: Split the problem into two parts. {1....N-1} and {N.....}

Why does that help? Well the latter horizon is made such that the constraints are inactive!
You can then make a new value function (objective function with a new name) and add it as a term in a standard QP finite horizon problem.

If that is feasible at all times, stability is guaranteed.
## 4.5: Nonlinear MPC
So now we have a finite horizon, and the model is given as xt+1 = g(xt, ut).

So the difference is we need an NLP solver.

State estimation is also harder. One then uses the Extended Kalman filter.
## 4.6: Comments
### 4.6.1: The control hierarchy and MPC
* Process... this is ... the process.
* Sensors, transmitters, actuators... these provide realtime data to the next layer
* Regulatory control. Here you find PIDs that control pressure, flow rate, temperture.... These are embedded in PLCs or DCS (distributed control system). How do the PIDs know their setpoints? Next layer.
* Advanced process control. Here dar be MPCs.
* Scheduling, optimization: Production plans.

Notice measurements come from below, and setpoints and control inputs are sent from the MPC.

Example: Ekofisk has sensors and collections real time data from >10000 tags every second.
If MPC was introduced, it would probably communicate with a database feeding it relevant sensor info (not directly).
It would then optimize some shiz and feed the result (control input) into the database.
This can then be used as setpoints in the regulatory PID zone level thangs.
### 4.6.2: MPC performance
MPC gives you increased disturbance rejection and better tracking performance!!
So industries love it!

Can also then operate closer to the operational limit (since the disturbance variations are mitigated and won't push it over the limit)!
### 4.6.3: Feedforward control
Nice way to improve performance when disturbance measurements are available.
### 4.6.4: MPC models
Experimental models are popular. Use step responses. Then models are formulated on input output form.

First principles models are better at prediciton but harder to develop.
Don't need as much data to to validate etc. then either.
### 4.6.5: Practical MPC formulations
Control input blocking: Reduce runtime by using control inputs for a longer time (so you don't have to calculate a new one EVERY sampling instant). Especially nice in NMPC applications!

Some often seen terms:
* CV = controlled variable, equal to j = H x.
* MV = manipulated variable, equal to u.
* DV = disturbance variable, equal to v (in the extended disturbance model from the feedforward setting).
