# To read:
## N&W
* ~~Chapter 1:    10 pages~~
* ~~Chapter 2:    14 pages~~
* ~~Chapter 3:    12 pages~~
* ~~Chapter 6:    10 pages~~
* ~~Chapter 8:    24 pages~~
* ~~Chapter 9:    05 pages~~
* ~~Chapter 11:   05 pages~~
* ~~Chapter 12:   40 pages~~
* ~~Chapter 13:   20 pages~~
* ~~Chapter 15:   20 pages~~
* Chapter 16:   30 pages
* Chapter 18:   15 pages
* Appendix A:   --

Total:          205 pages
## F&H
* Everything:   80 pages
Total:          80 pages
## Num. pages and planning
All in all this is 285 pages.
Exams is on 20. of May.
Starting 07., and including 19., that is 19-7+1 = 13 days.
285 / 13 ~ 22 pages.

This gonna be a lot of reading.
# Nocedal & Wright
## Chapter 1
* Feasible region = all constr OK
* We got unconstrained, and constrained problems. We got linear, nonlinear. Convex nonconvex.
* Global and local solutions
* Convexity; have definitions on f and region. Also convex prog problm: f is convex, eqs are linear, ineqs are concave.
## Chapter 2
We now deal with unconstrained optimization.
### Solutions
Global min if it's the least fvals globally (can be others if not strict).

If twice cont diffable we can tell solution is local minimizer just by Hesisan and grad. Then grad is zero and Hess is pos semidef.

Remember: Sufficient means it may be solution. It can also not satisfy necessary conditions but still be best solution (since not necessary).
### Algorithms
When dealing with unconstrained stuff, need to supply starting point.

Strategy: Line search. Here we choose a direction and search along it.

Strategy: Trust region. Here we make a new function an approx of f, and search in a small area around there.

Differences: Step length and direction. Line search chooses dir and finds out how long to go, trust region can change dir and find out max distance.

### Search directions
* Steepest descent. The dumb alternative. Only needs grad tho. Can be fuckslow.
* Newton dir. Uses Hessian inverse. So need posdef Hess. Uses step = 1 unless it provides shit (as default). Convergence rate can be quadratic.
* QuasiNewt approxes Hess with Bk (symmetric, posdef by design).
## Chapter 3
How do I choose alphak and pk (step size, search dir) in order to converge quickly from farlands?
### Step length
* Find an interval containing OK lengths
* Find a good step in this interval
### Termination conditions
It is not enough to have a reduction in f (risk never conerging). Need a SUFFICIENT reduction.
This leads us to Wolfe conditions:

1) We make a slope using the current f val and the proposed step length in the search dir times the slope of f and a scaling factor. We need the function val (where we go) to at least be under this line. A larger c1 (scaling factor) is more forgiving. c1 is very small, e-4. 

2) We look at the curvature as well. This rules out extremely small values of alpha. You look at the initial slope. You want the slope where you stop to be worse (flat out), because then you have exhausted your gains.

We can also do STRONG version, where the derivative of the proposed function given the new step is not too positive, hence it cannot be too far from a stationary point.
### Backtracking line search
Evaluate f using the proposed step length. Is it less than a condition with a scaling using step and slope etc.? If yes then return it and use it. Else try a new step length, alpha new = alpha times rho (scales it down).
### Line search Newton with Modification
Here we have any starting point x0. The we factorize the matrix Bk = Hess + Ek, where Ek is nonzero only if it's needed to make Bk positive definite enough.

Then solve BkPk = -grad f(xk) in order to find search dir

Then set x{k+1} by using the search dir and a step length found by Wolfe (or some other).
### Step-length selection algorithms
* Use intial step length 1 for Newton based methods.
* Other methods: A few formulas for intial step length is provided.
## Chapter 6 Quasi-Newton Methods
These methods require only the gradient at each iterate.
A model of the objective function is taken at each iterate.

We use Bk as an approximation to the curvature (or quadratic nature?) of the function at an iterate. It is updated at each iterate, using xk+1 and xk.
### Algorithm 6.1 BFGS method
Note that it is the inverse of Bk that is actually used in the implementation.

The alg:

You start somewhere x0, and you have a tolerance, and an approximation H0, and k = 0.
While the magnitude of the grad of fk is larger than the tolerance (which is the termination requirement), find a place to look == compute search direciton.

The search direction is found by multiplying -Hk with grad fk.
Now set next x equal to current x plus some step times this direction, where the step is found by a line search method satisfying the Wolfe conditions.

Compute the next H, and k++.

Repeat.


Notice that this cannot stop unless we are at some extreme.

It is shown that BFGS and Newton uses 20-30~ iterations on a problem steepest descent uses 5300~ on.

The method is reliant on good approximations of the (inverse) Hessian. It may happen that these approximations are some times shit, but the method has properties that aid "self-healing". 
The wolfe conditions actually help in allowing the method to land at spots which give reasonable information about the curvature.

H0 is often set to I.

It is economical to use a fairly inaccurate line search.
Wolfe conditions set to c1 = e-4, c2 = 0.9.

Also start with step size 1, this aids in superlinear convergence.

Other conditions than Wolfe can be used, but they are shit.
## Chapter 8: Calculating derivatives
* Finite differencing: Taylor based. Observe change in function values due to small perturbations. Then you can estimate infintesimal perturbations; derivatives.
* Autodiff: Decompose into elementary arithmetic operations which you can use chain rule on.
* Symbolic diff: Manipulate algebraic specification....
### 8.1 Finite-difference deriv approx
Finite perturbations, differences in function values!

Instead of using the [f(x + h) - f(x)]/dx we use [f(x+h) - f(x-h)]/2dx, which is more accurate. The first is called forward-difference, the second central difference. 

It's all based on Taylor's theorem.
### 8.2 Automatic differentiation
Take a function and split the operations into a graph, multiplication, trig, exp, addition, ...
Underway you make intermediate variables.

In forward mode you evaluate and carry forward a direction derivative of each intermediate variable in a direction.
## Chapter 9: Derivative-free optimization
This is about not looking to approx the grad but rather using function evals (several points).

We discuss the unconstrained problem min x f(x).

f can be analytically unknown, then this is useful. Or stochastic.
### 9.1: Finite differences and noise
Should always look to do finite-diff and approx grad. But no can do sometimes, maybe because noise (= excessive error in function evals).

It can be shown that if a method is noisy, then finite diff can give directions that are only correct by pure luck.
Noisy here means that the noise term dominates the intrinsic error in finite-differencing (error).
### 9.5: Nelder-Mead method
You have a simplex (not related) set with vertices {z1, ..., zn+1} and the matrix V(S) matrix by taking n edges from a vertix e.g z1: V(S) = [z2 - z1, z3 - z1, ..., zn+1 - z1].
We need V to be nonsingular.

So for R3 we need the four vertices to not be coplanar (so they actually make a triangle-volume-thing and not just a triangle flat).

What do we want from an iteration? To remove the vertex that is most shit.
How?!

Expand! Contract! Reflect!

What do we do these actions on? The line joining the shit vertex with the centroid of the remaining better ones!

What if that didn't work! Well! Shrink all vertices towards the best one!

Remember that f(x1) is least shit, f(xn+1) is most shit.

Algorithm:
Calculate centroid(-1) and check the function value there. This means finding the midway between x1 and x2 (centroid of two best) and then reflecting about this centroid away from x3 == shit.

What does the result look like?

* If it was not worse and not the best, replace x3 by that one. 
* If it was better than x1, check centroid(-2)! Greedy! Checking if it's even greener on that side!
Then if THAT's better, replace x3 by that, if not just use the first one that was a good find.

What if it is shittier than the second shittiest?
Now we contract! (Meaning the triangle will decrease in area since we use less value than || -1 ||
* If it's that shitty but not worse than the WORST, check if it's better to go half the reflected distance instead.
Was that better? Use that value go on.
* If it was worse than we started with, check if we instead of reflecting outside can contract inside.

THAT DIDN'T WORK EITHER!
Well then contract every xi, i = 2, 3, ..., n +1 with a contraction (1/2) towards x1.

Problems?

Can stagnate at nonoptimal points. Just restart!

The avg funval will generally decrease every step (unless that shrinkage step happend the last one). If f convex we always decrease.
## Chapter 11.1: Nonlinear Equations
We have a Newton Method for nonlin eqs. It makes a linear model of the functions ri(x) and their derivatives at the iterate. It then finds the Newton step which minimizes this model.

Start anywhere. However a remote location can make the methods behave weird. A singular Jacobian can happen. Also the Jacobian can be difficult to obtain (nonlin after all), and pk (the Newton Step) can be expensive to obtain when n (the number of nonlin eqs) is large. Also the solution x{star} can be degenerate --> the Jacobian can be singular.

## Chapter 12: Theory of Constrained Optimization
Yay now the good stuff comes.
So we got min f(x) subject to ci(x), i in I and E.
We got the feasible set, which is where all constraints are satisfied.

Necessary: Must be satisfied by any solution point.
Sufficient: Conditions that say x{star} is in fact a solution.

Solutions: Local, strict local, isolated local (= if it's feasible, and in a neighbourhood it is the only local solution).

Smoothness: A diamondshaped thang is OK when the edges are described by smooth happy linearfunctions. Smoothness is ofc importantez.

Remember to rearrange shiaz so that ci(x) >= 0 when i in I.
### 12.1 Examples

Active set: the E indices unioned with i in I such that ci(x) = 0, so all indices really where the constraint is at it's edge.

Then comes the example of the ring with radius sqrt(2). f is x1+x2. Where does the grad of f point? Well the grad is equal to [1 1]^T so it points north-east. Where does the constraint (x1^2 + x2^2 - 2) point? The grad is [2x1 2x2]^T, so it depends. In this problem it always points directly outward from the ring.

But notice at the solution x = [-1 -1] the constraint grad points directly opposite the dir of grad f.

This would mean there should exist some scalar which makes grad f(x{star}) = scalar grad c1(x{star}) right? Well there you go that's the Lagrange multiplier.

This concept of parallellness shows that the only time a direction which when following that direction a small step makes the new x still feasible and also decreases f, is when this Lagrange multiplier exists.

This is summarized in saying that the grad wrt x of the Lagrangian at the solution must be zero.

Notice this is necessary but not sufficient (the upper right part of the ring is the worst solution, but it still satisfies this condition).

What if we looked at the sign of the Lagr multipl? No that's not enough. Then the solution would be different if we looked at f vs. -f.

What if we looked at this ring and filled it to a circle, meaning we have an inequality constraint instead? Then the sign of the Lagr mult is important!

Why is this? Well consider an ineq. constr that is not active. Then any direction satisfies the feasibility demand. Then we demand the Lagr mutl to be zero.
And when active, we want grad f in a small step to be negative (open half space) and grad c in a small step to be >= 0 (either stay zero or be larger than zero) (closed half space). These half-spaces are pointing in different dirs, but they overlap as long as the grads are not parallell.

So then grad f = scalar grad c when pointing in same dir, and that scalar must then be positive! Then there is nowhere to move.

So now we know that if there exists a dir that decreases f and will still be feasible, we go that way. Anywhere we can check our conditions for optimality.

## Chapter 12.2 Tangent cone and constraint qualifications
The situation may arise when a linear approximation can not yield useful info about the original problem. Then we introduce constraint qualifications.

The feasible directions F(x) are direction d such that walking this dir on ci, i in E still gives 0, and for active ineqs still >= 0.

We want to check that F(x) captures the tangent cone, which represents the geometry of the problem.

This is checked most commonly by the LICQ:

You have a point x and an active set A(x). Then the LINEAR INDEPENDECE CONSTRAINT QUALIFICATION says the constraint gradients grad ci(x), i in A should be lin indep.
### Chapter 12.3 First order optimality conditions
They are called this since we look at the properties of the gradients (first derivatives of both f and c).

KKT CONDITIONS!
* Grad x L(x{star}, lambda{star}) = 0 at solution. This means the grads are parallell.
What if they weren't? Well then bastard move in some direction!
* ci(x{star} = 0 for all i in E
* ci(x{ßtar} >= 0 for all i in I
* lambdai{star} >= 0 for all i in I. This ensures we are at a min instead of a max!
* lambdai{star}ci(x{star}) = 0, for all i in E and I. This ensures that for equalities, lambdas can be anything when constraint is satisfied, and for inequalities that lambdas are zero when not active. It can also mean that lambda is zero AND active.
 
 ### Chapter 12.5: Second order conditions
 Why use these? Well the feasible directions for x{star} tells us that moving in such a dir will either have the first order approximations not change or increase.

 If the first order approximations do not change, what will a move in such a dir do to the obj fun f? The second order conditions can tell.

 Theorem 12.5: 2nd O necess. This says if we have x{star} a local sol and the LICQ holds. Then we have the lagr multipl{star} vector which satisfies KKT. Then w^T Hess{xx}Lw >= 0 for all w in the critical cone (which is directions you can go where you still end up in the active set ish).

 Theorem 12.6: Sufficient. Don't need LICQ here, but now the ineq is strict.

 If you satisfy this thm you know you are a local sol (global if convex).

 It seems we are happy to know that the critical cone is the null space of the matrix whose rows are the active constraint gradiants at x{star}.

 Then Z^T Hess{xx}LZ pos semidef = thm 12.5, or pos def = thm 12.6.
 Hmm.

 ### Chapter 12.8 Lagrange Multipliers and Sensitivity
 Lagrange multipliers are an indication of how sensitive the obj fun is to the constraint.
 The obj fun gives no shits about inactive ineq constraints; their Lagr mults are 0.

 It can be shown that if you diff the obj fun wrt a small perturbation, the result is scaled by the lagr mult, which shows this sensitvity connection.
 ### Chapter 12.9 Duality
 The dual problem is constructed from functions and data of the original.
 It is in some cases easier to solve computationally. In some cases it can be used to easily find a lower bound of the objective of the primal problem.

 We look at the case where there are no equality constraints. Then we have min x in R^n f(x) s.t c(x)>=0, where all ineqs are collected in c. Then we have a Lagr mult vector in R^m such that L = f(x) - lambda^T c(x).

 The dual objective fun is then q(lambda) = inf x L(x, lambda). The domain is D = lambda such that q(lambda) > -infty.
 We want the global minimizer. Then f is convex, and -ci(x) convex, and lambdas are positive (just cuz we interested in that). We then have the dual problem:

 max q(lambda) lambda in R^n s.t lambda >= 0.

 Infimum: You have some set A. If A not bounded below, inf A is -infty. Not the case here! If A is empty, then inf A is infty. Not the case! The last case then is that inf A is the greatest possible lower bound.

 So as an example if you take f(x) subject to c(x) and find the lagrangian L, and you know L is convex, then the inf is found by takin partial derivs equal to zero (partial derivs wrt x1 and x2 etc.).
 Then you take these expressions for x1, x2, x3,... and insert into L. Then you find the dual objective. Then you can formulate the dual problem as well.

    You can find general formulations for the LP and QP problem in dual form. The procedure is quite similar for both. Just formulate the problem, and look for the inf x L(x, lambda). Then exclude cases that involve -infty. Then make an argument for convexity (since convex we know where to find lower bound). Then put the dual objective into the dual problem.
## Chapter 13: Linear Programming: The Simplex Method
We look at LP problems, and we transform problems on weird form to standard form by using slack variables.

So instead of min c^Tx st Ax <= b, we write min c^Tx st Ax + z = b, z >= 0.
### 13.1 Ooptimality and duality
We now look at KKT. We are convex, so global is kkkk. We don't care about 2nd order, the Hessian is 0 anyway. We don't care about LICQ as it isn't needed when we have linear constraints.

Now we make a formulation of the Lagrange, using lambda as multiplier for eq constr [Ax-b], and s as multiplier for x >=0.
Then L=c^Tx - lambda^T(Ax - b) - s^Tx.

Then we get:
* A^T lambda + s = c,   (take grad wrt x)
* Ax = b,               (just that constraints must be satisfied)
* x >= 0,               (hmm nonphyiscal if not?)
* s >= 0,               (this might be to ensure we find min not max)
* xi si = 0,            (probably to ensure that ineqs are MEH when not active)

We also find by combining these that c^T x{star} = ... = b^T lambda{star}, which is the dual objetive (the right hand side).

Now we find something sick! We define the dual problem:

max b^T lambda, st A^T lambda <= c. This is the dual problem.
Restate it as:
min -b^T lambda, st c-A^Tlambda >= 0, which fits our earlier setup when we found KKT.
Now use x to denote Lagrange multipliers (for the constraints).
Then you can define the Lagr: -b^Tlambda - x^T(c - A^T lambda). If you then do the KKT dance, and let s = c - A^Tlambda, you get the same setup as above!!! THE CONDITIONS ARE IDENTICAL.

Also strong duality means if either problem has finite solution, so does the other, and objective values are equal. If either unbounded, the other is infeasible.
### 13.2 Geometry of the feasible set
We now assume A has full rank.

Each iterate of simplex gives a basic feasible point. What does that mean?
A vector x is a BFP if feasible (obv) and if there is a subset weird-B of the index set {1, 2, ..., n} such that weird-B has m indices, index i not in weird-B means that xi = 0 (which means xi >= 0 only inactive if i in weird-B), the m x m matrix B defined by B = [Ai] i in weird-B is nonsingular.

So what the hell does this mean? Make a square matrix out of columns of A. The column(s) you did not add need to have their x values set 0.

This set weird-B is a basis for the problem. The subset matrix B is the basis matrix.
When will it converge to a solution?
* The problem has basic feasible points
* At least one is a basic optimal point == a solution and a BFP

So when does this happen?
* Nonempty feasible region --> 1 >= # of BFP
* Solutions exist, >= 1 is a BFP
* If problem feasible and bounded, it has an optimal sol.

Also all BFP are vertices for the feasible polytope {x | Ax = b, x >= 0}, and vica versa.
### 13.3 The simplex method
What happens?
* We most often change one component of the basis weird-B
* We most often decrease objective function
* We most often go to a neighbour BFP = vertex
The issue: How to know which index to remove from weird-B (and which to put in)?
Some KKT shiz and such gets us somewhere.

Example:
You have R^2, two eq constr and x >= 0.

Remember we have the LP problem:

min c^T x,      subject to Ax = b, x >= 0.
But we reformulate shiz into:
* L = c^Tx - lambda^T(Ax - b) - s^Tx

* A^T lambda + s = c,   (take grad wrt x)
* Ax = b,               (just that constraints must be satisfied)
* x >= 0,               (hmm nonphyiscal if not?)
* s >= 0,               (this might be to ensure we find min not max)
* xi si = 0,            (probably to ensure that ineqs are MEH when not active)

What do:
* Have a start basis, see what the constraints now equal (with other x-values = 0), find lambda, find s{n}. 
Finding lambda: B^T lambda = cb
* Check objective value.
* Are the elemnts of s{n} negative?
* Choose entering variable (next basis index) from one of the negative s entries. Obtain d.
* Conclude unboundedness?
* Ratio calculation, find p, corresponding index (to be swapped out), and xi+.
* Update weird B and weird N, and move to next iteration.

* Terminate? Yes when s_n positive.
* Unbounded? Yes if d <= 0.

### 13.4 Linalg in simplex
To be efficient we use LU factorization of B since we all the time use B to calc lambda and d.

How this work again? ok say you want to find: Bd = Aq.
If B = LU, then you first find: Ld{bar} = Aq (easy since L lower triangular), then find Ud = d{bar} (easy cuz U upper triang).
Then there of course are tricks to updating as one only switches on column balbalablalbal.
### 13.5 (From starting the simplex method)
We need a basic feasible point. We also need a basis such that B is nonsingular and xb = inv(B)b >= 0 and xn = 0. Is this hard? Yes it is! Its difficulty is equal to actually solving a LP.

How is this approached? A two-phase approach.
* 1) Set up an LP and solve using simplex, this LP has ez init 
* 2) Set up another LP with solution of 1) as init point. After this is solved extract solution.

There can be so called degenerate steps in simplex. This means the objecctive does not decrease. Can be useful anyway since it "sets the stage".

Cycling means returning to an earlier basis. Never converging, sad!
There exists a trick where you add a perturbation to the problem which guarantees no cycling, and you can remove its effect in postprocessing.
### 13.8: Where does the simplex method fit?
The simplex method is an active set method, it estimates (using weird-B) which indices it belives are inactive for the solution of the LP.

We make modest changes to the index sets, a swap between weird-B and weird-N.

The principles are quite similar when doing nonlinear problems. Not as efficient though. Simplex is often very efficient. However one can find examples where it is complete shit. Therefor researchers found interior-point methods.

## Chapter 15
### 15.3 Elimination of variables
It's a good thing to eliminate variables. Makes life lazier.

There is a formula for nonlinear problems with equality constraints. This formula transforms the problem into an unconstrained problem.

Hard stuff! Is it needed at all?

### Chapter 15.4 Merit functions
Problem: An algorithm solving nonlin prog problem, but takes a step that decr obj fun but increases violation of constraints. Yes or no?

We need to balance these competing goals. We need to "filter" it or have some merit tell us if it's ok or not.

A typical merit function is one that kinda looks like Legrange.

theta1(x; mu) = f(x) + mu sum{i in E} |ci(x)| + mu sum{i in I} [ci(x)]{-}, where the last means the max of{0, -ci(x)}. So the last term is only accepted if the inequality is negative (not when it's satisfied or active).

Mu here is the penalty weighting. The more weighting, the more important it is to NOT violate the constraints.
As an example if mu = 0 then we only care about f(x) and pretend there are no constraints.

What does it mean that a merit function is exact?
It means there exists a threshold weight such that using larger weight leads to the fact that a local minimizer of the merit function is then a local solution of the NLP.

It can be shown that under certain circumstances this threshold value is given by the largest L-multipl{star} (in magnitude).

Another exact: theta2(x; mu) = f(x) + mu || c(x) ||{2}.

Now a line search method will accept a step if it provides a sufficient decrease in the merit function. 
### 15.5 The Maratos effect
This effect was the fact that some methods COULD converge quickly but they reject steps that would make good progress towards the solution.

This happens when a great step would cause an increase in both obj fun and constraint violations.
What can we do?
* Use a merit function non suffering from Maratos; theta{f} = Fletcher 
* Second order correction
* Allow merit to not only decrease (nonmonotome)
## Chapter 16: Quadratic Programming
Now we have linear constraints, but the objective function is quadratic.

Convexity is decided by G being positive (semi)def.
Then it's not much harder than a LP!
### 16.1: Eq constrained QP
We now write min x q(x) = 1/2 x^T G x + x^T c,  s.t. Ax = b.
Then A is filled with rows of eq constraints.

Now we can see that L = 1/2 x^T G x + x^T c - lambda(Ax - b), and then
grad{x} L = Gx + c - A^T lambda, and we want this to be zero at the solution.

Then we get that Gx{star} -A^T lambda{star} = -c. Also Ax{star} = b from the formulation of the problem.

This KKT matrix form.

It can be rewritten to a more computationally friends form. 

Thm 16.2: A full rank. Z^T G Z pos def. Then x{star} satisfying the KKT matrix is unique global sol.

Z is a matrix n x (m - n) where the cols are a bsis for the null space of A; Z full rank and AZ = 0.
### 16.2: Direct solution of KKT system
There are fancyschmanzy ways of efficiently solving this KKT matrix; set methods which involve factorization. 
(Skip 'Schur...', 'Null-space method')
### 16.4: Ineq constrained QP
Now just set the Lagr to L = 1/2 x^T G x + x^T c - sum{i in both} lambda{i}(a{i}^T x - b{i}), so this basically encompasses everything.

Now KKT will give ya:
* Gx{*} + c - sum{i in A(x{*}) lambda{i}{star} a{i} = 0
* a{i}^T x{star} = b{i}, active set
* a{i}^T x{star} >= b{i}, i in I not in active set
* lambda{i}{star} >= 0, i in I inactive 

We do not need to think of LICQ here.
Now if G pos semidef, global solution.

Degeneracy may cause trouble. This can happen when active constraint grads are lindep at solution.
### 16.5: Active set methods for convex QP
(Skip 'updating factorizations')
It begins! This is the method where we have blocking constraints and such geschmazzle.
We just need six hundred pages of theory first!

Now what are we trying to solve? A QP problem with equality and ineq constraints.
We only consider the convex case.

Now what is different in comparison the simplex method? We are still using an active set. The iterates and the solution need not be vertices now.

We will use all eq constr and some ineq constr at each iterate, and use those to solve a quad subproblem. Weird-Wk is the working set.

Weird-Wk must have lin indep grads.

Now when we are at an iterate xk in our working set, we check if it minimizes the Q subproblem. If not, compute a step p by solving Q subp.

This solution pk is found by e.g. solving the KKT matrix. Now we set xk+1 = xk+pk if feasible wrt to all constriants. If not we choose the largest alplha we can in from 0 to 1.

We have a blocking constraint, which is the constraint that limits alpha (step size). If alpha = 1 or there are no new constraints active, then we did not have a blocking constraint.

The working set is updated by including one of the blocking constraints.

It is easy to see when you find an x that minimizes the Q subproblem, because the subproblem p is then 0.

Maybe this is the global solution (since we are working on a convex problem)? Well then you have to look at the signs of multipliers for the inequalities. If all nonneg, then the 4th KKT condition is OK.

On the other hand if there is a neg multiplier, drop that index from working set. A following thm shows that the next direction p will still be feasible wrt to the dropped set.

Thm 16.6 says that if pk nonzero and OKs 2nd order conds, then q(.) strictly decr along that dir.

### The algorithm; 16.3 -- Active set method for convex QP
Wow the alg makes sense!
* Init: Compute a feasible starting point x0, set weird-W0 to be subset of active constraints at x0.
* Solve to find pk == direction (KKT matrix problem thing cuz only eq constr)
* If dir pk == 0; check the lagr multipliers.
* If all lagr mutlipliers for ineqs in working set are nonnegative, then damn son KKT ok2k we DONE
* If there are neg lagr multipls, find the index of the most neg one, update xk+1=xk, and the working set drops that found index
* Now if pk was not 0, find the step size by an explicit formula and update xk+1=xk + alpha pk
* Now if there were blocking constraints, the next working set should add one of them
* If there was not, then just set the next working set to be equal to the current and move on

So how does one find that first feasible point?
One makes a feasibility linear program and solves it. The working set weird-W0 is also taken from this.
### 16.8: Perspectives and software
There exists software.

## Chapter 18: SQP
We now want to solve nonlin progs that have constraints which may also be nonlin.
### 18.1: Local SQP
Now we have min f(x),   c(x) = 0.

So what do we do? We of course model this problem as a QP subproblem, then use the minimizer to define xk+1.
What's the problem? Making a good model so that a good step for the nonlin opt problem is found.

So what strategy to do?
Well make a KKT matrix: [grad L,    c(x)]^T = 0. Notice grad L = grad f(x) - A(x)^T lambda, where A(x) is the Jacobian of the constraints.

But this KKT matrix is nonlinear! That sounds hard! Use Newton's method. This uses the Jacobian of the KKT and the KKT itself to find steps for xk and lambdak to take.

For algorithms that can eventually tackle ineqs, we use an SQP framework.
The algorithm:
Then you need init x0, lambda0, and until convergence, eval fk, grad fk, hess{xx}Lk, ck, Ak,
and solve a QP program which gives you steps for xk+1 and lambdak+1.

### 18.2 Preview of SQP practical
Two ways of making an SQP method for solving general nonlin problem.

The first we just saw, it solved a QP and takes active set at solution as a guess of optimal active set. This is called IQP = inequality constrained QP.
The other is EQP = eq constrained QP. That uses working set and everything is an eq constr.
### 18.3: Algorithmic development
* Want subproblems to be feasible ofc
* Want alternative cheap Hessian approxybalubas
* Want step-acceptance mechanisms

So what can happen when we linearize the nonlin constraints? We can get an infeasible subproblem.
We then need to introduce penalty parameters to make space for such sillyness.

Also Hessian can be expensive, so can use a Quasi-Newton approx. So use BFGS formulae to get a new Bk+1.
Some modification is done so that it MUST be posdef. If it was not we could get into trouble town.

Merit functions. These are used for step-acceptance (if trust-region method), or step size (in line search method).

The Maratos effect. Use second order correction to fix that.

### 18.4: A practical line search SQP method
* Choose eta in 0-->0.5, and tau in 0-->1. Choose x0, lambda0.
* Eval f0, grad f0, c0, A0.
* If quasi-N, choose approx B0 (n x n, symm, posdef), if not compute the Hess{xx}L0.
* Start loop until convergence test.
* Compute pk, lambda{hat} the correspond multipl
* p{lambda} = lambda{hat} - lambdak
* Choose penalty muk
* Set step alphak = 1
* While merit funciton does not have sufficient decrease, reduce step length using tau{alpha}
* Set xk+1 and lambdak+1.
* Eval stuff needed for approxing Bk+1.

## Appendix A
Wow 35 pages long. Exclude: Sherman-M-W formula, Interlacing Eigs, Error analysis, Sequences, Rates of Conv, Impl fun.

Symmetric matrix pos def by checking eigs are pos.

A matrix is orthogonal if QQ^T = Q^TQ = I, so the inverse is its transpose.

Null space: Null(A) = {w | Aw = 0}.
Range space: Range(A) = {w | w = Av,    for some vector v}.

LU factorization: A in R{n x n}
PA = LU.

P is an n x n perm matrix (rearranged identity matrix). L unit lower triangular, so lower triang but diag elements == 1.
U is upper triangular.

This can be used to solve Ax = b efficiently. As such:
1) b{bar} = Pb
2) Lz = b{bar}
3) Solve Ux = z, to find x.

Conditioning: If the answer x changes a lot due to a small change in A or b, then ill-conditioned.

Mean value thm: f(x + p) = f(x) + grad f(x + alpha p)^T p
So the function value at some point ahead is equal to the current point, plus using the slope somewhere in between and extending it the same length p.

# Foss & Heirung
