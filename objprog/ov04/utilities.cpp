#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cctype>
#include "utilities.h"

int randomWithLimits(int lowerLimit, int upperLimit)
{
    int randomNumb = 0;
    do
    {
        randomNumb = std::rand();
        randomNumb = randomNumb % (upperLimit + 1);
    }
    while(!( (randomNumb >= lowerLimit) && (randomNumb <= upperLimit)) );

    return randomNumb;
}

void swapNumbers(int* numA, int* numB)
{
    std::cout << "numA: " << *numA << "\nnumB: " << *numB << std::endl;
    int temp = *numA;
    // temp peker på samme som pekeren til A
    *numA = *numB;
    // peker til A peker nå på samme som peker til B
    *numB = temp;
    // peker til B peker nå på det peker til A peker på
    std::cout << "||| ---- EXTREME SWAPMAGIC ----|||";
    std::cout << "\nnumA: " << *numA << "\nnumB: " << *numB << std::endl;
}

void printArray(int array[], int sizeOfArray)
{
    std::cout << "[";
    for(int i = 0; i < (sizeOfArray - 1); i++)
    {
        std::cout << array[i] << " ";
    }
    std::cout << array[sizeOfArray - 1] <<  "]\n";
}

void randomizeArray(int array[], int sizeOfArray)
    // Fills array with random ints between 0 and 100 (inclusive).
{
    for(int i = 0; i < sizeOfArray; i++)
    {
        array[i] = randomWithLimits(0, 100); 
    }
}

void sortArray(int array[], int sizeOfArray)
{
    int sorted = 0;
    int index = 0;
    while(!sorted)
    {
        if(array[index + 1] < array[index])
        {
            int temp = array[index];
            array[index] = array[index + 1];
            array[index + 1] = temp;
            index = 0;
            // start over!
        }
        else
            index++;

        if((index + 1) == sizeOfArray)
            sorted = 1;
    }
}

double medianOfArray(int array[], int sizeOfArray)
{
    double median;
    // assume sorted
    if((sizeOfArray % 2) == 0)
    {
        // even array
        median = (double) (array[sizeOfArray/2] + array[sizeOfArray/2 - 1]) / 2;
    }
    else
        median = (double) array[(sizeOfArray - 1) / 2];

    return median;
}

void randomizeCString(char array[], int sizeOfArray) {
    // Make sure no stray \0 is found
    for(int i = 0; i < sizeOfArray - 1; i++)
    {
        array[i] = '-';
    }

    // http://www.asciitable.com/
    // A == 65
    // B == 66
    // ..
    // F == 70
    for(int i = 0; i < (sizeOfArray - 2); i++)
    {
        array[i] = randomWithLimits(65, 70);
    }
    array[sizeOfArray - 1] = '\0';
}

void randomizeCString2(char array[], int sizeOfArray, char lowerBoundChar, char upperBoundChar) {
    // Make sure no stray \0 is found
    for(int i = 0; i < sizeOfArray - 1; i++)
    {
        array[i] = '-';
    }

    // http://www.asciitable.com/
    // A == 65
    // B == 66
    // ..
    // F == 70
    for(int i = 0; i < (sizeOfArray - 1); i++)
    {
        array[i] = randomWithLimits( (int) lowerBoundChar, (int) upperBoundChar);
    }
    array[sizeOfArray - 1] = '\0';
}

void readInputToCString(char array[], char lowerBoundChar, char upperBoundChar, int sizeOfArray)
{
    for(int i = 0; i < sizeOfArray - 1; i++)
    {
        array[i] = '-';
    }

    int index = 0;
    array[sizeOfArray-1] = '\0';



    // Use int instead of char for ease of use.
    int lowerCharLimitInInt[2] = { (int) tolower(lowerBoundChar), (int) toupper(lowerBoundChar) };
    int upperCharLimitInInt[2] = { (int) tolower(upperBoundChar), (int) toupper(upperBoundChar) };

    char charFromUser;
    while(array[index] != '\0')
    {
        std::cin >> charFromUser; 
        if((charFromUser >= lowerCharLimitInInt[0] && charFromUser <= upperCharLimitInInt[0]) ||
                (charFromUser >= lowerCharLimitInInt[1] && charFromUser <= upperCharLimitInInt[1])) 
            // Checks if input is between e.g. [a --- c] || [A --- C] 
        {
            array[index] = charFromUser;
            index++;
        }
        else
        {
            std::cout << "Char: " << charFromUser << " --- ikke i din gitte range! Ble ikke lagt til.\n";
        }
    }
}

int countOccurencesOfCharacter(char array[], int sizeOfArray, char targetChar)
{
    int targetHits = 0;
    for(int i = 0; i < sizeOfArray; i++)
    {
        if(array[i] == targetChar)
            targetHits++;
    }
    //std::cout << "Antall hits av " << targetChar << ": " << targetHits << ".\n";
    return targetHits;
}

void printNumberOfBackslashes(int howManyPerLine, int lines, bool prependNewline, bool appendNewline)
{
    if(prependNewline)
        std::cout << "\n";

    for(int j = 0; j < lines; j++)
    {
        if(j >= 1)
            std::cout << "\n";

        for(int i = 0; i < howManyPerLine; i++)
        {
            std::cout << "\\";
        }
    }

    std::cout << "\n";

    if(appendNewline)
        std::cout << "\n";
}
