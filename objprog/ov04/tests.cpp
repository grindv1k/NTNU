#include <iostream>
#include <cmath>
#include "tests.h"
#include "utilities.h"

using namespace std;

void testPart1()
{
    int v0 = 5;
    int increment = 2;
    int iterations = 10;
    incrementByValueNumTimes(&v0, increment, iterations);
    cout << "v0: " << v0 << " increment: " << increment 
        << " iterations: " << iterations << " result(v0): " << v0 << endl;
}

void testPart2()
{
    int sizeOfThisArray = 9;
    int percentages[sizeOfThisArray];
    randomizeArray(percentages, sizeOfThisArray);
    printArray(percentages, sizeOfThisArray);
    std::cout << "Printed randomized array. Now calling swapnumbers to swap first two values ... \n";
    swapNumbers(&percentages[0], &percentages[1]);
    printArray(percentages, sizeOfThisArray);
    std::cout << "Now sorting array ...\n";
    sortArray(percentages, sizeOfThisArray);
    printArray(percentages, sizeOfThisArray);
    std::cout << "Finding median ...\n" << medianOfArray(percentages, sizeOfThisArray) << std::endl;
}

void testPart3()
{
    int sizeOfGrades = 8*1 + 1;
    char grades[sizeOfGrades];
    readInputToCString(grades, 'a', 'f', sizeOfGrades); 
    //randomizeCString(grades, sizeOfGrades);
    std::cout << grades << std::endl;
    // int inputStringArraySize = 16;
    // char inputStringArray[inputStringArraySize];
    // readInputToCString(inputStringArray, 'a', 'r', inputStringArraySize);
    // std::cout << inputStringArray << std::endl;
    // std::cout << "Antall hits av bokstav 'N': \n";
    // countOccurencesOfCharacter(inputStringArray, inputStringArraySize, 'N');
    // oppg. h)
    int gradeCount[] = {
        countOccurencesOfCharacter(grades, sizeOfGrades, 'A'),
        countOccurencesOfCharacter(grades, sizeOfGrades, 'B'),
        countOccurencesOfCharacter(grades, sizeOfGrades, 'C'),
        countOccurencesOfCharacter(grades, sizeOfGrades, 'D'),
        countOccurencesOfCharacter(grades, sizeOfGrades, 'E'),
        countOccurencesOfCharacter(grades, sizeOfGrades, 'F'),
    };
    double averageGrade = ( (gradeCount[0] * 1 + gradeCount[1] * 2 + gradeCount[2] * 3 + gradeCount[3] * 4
                + gradeCount[4] * 5 + gradeCount[5] * 6) / (double)
            (gradeCount[0] + gradeCount[1] + gradeCount[2] + gradeCount[3] + gradeCount[4] + gradeCount[5]));
    int roundedAverage = round(averageGrade);
    std::cout << "Snitt (A = 1, F = 6): " << averageGrade << ".\n";
    std::cout << "Snitt: " << (char) (roundedAverage + 'A' - 1) << ".\n";



}

void incrementByValueNumTimes(int* startValue, int increment, int iterations)
{
    for(int i = 0; i < iterations; i++)
    {
        *startValue += increment;
    }
}
