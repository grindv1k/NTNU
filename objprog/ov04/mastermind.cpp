#include <iostream>
#include "utilities.h"
#include "mastermind.h"

void playMastermind()
{
    const int SIZE = 4;
    const int LETTERS = 6;
    int ALLOWEDTRIES = 6;

    char code[SIZE + 1];  
    char guess[SIZE + 1];  
    int numberOfRightGuessesInPosition = 0;
    int numberOfRightGuesses = 0;

    randomizeCString2(code, (SIZE + 1), 'A', 'A' + (LETTERS - 1));

    // Oppgave d)
    //Juks!
    std::cout << "Autogenerert kode: " << code << ".\n";

    // Oppgave e)
    std::cout << "Du skal nå gjette en kode på " << SIZE << " bokstaver (A -- F).\n";
    std::cout << "Du har " << ALLOWEDTRIES << " forsøk.\n";
    printNumberOfBackslashes(65, 2, true, true);

    do
    {
        std::cout << "Gjett kode på " << SIZE << " tegn: \t";
        readInputToCString(guess, 'A', 'A' + (LETTERS - 1), (SIZE + 1));

        numberOfRightGuessesInPosition = checkCharactersAndPosition(guess, code, SIZE);
        numberOfRightGuesses = checkCharacters(guess, code, SIZE, LETTERS);

        std::cout << "Riktig char og pos: \t\t" << numberOfRightGuessesInPosition << std::endl;
        std::cout << "Riktig char uavhengig av pos: \t" << numberOfRightGuesses;
        std::cout << "\t" << --ALLOWEDTRIES << " forsøk igjen!";
        if(ALLOWEDTRIES <= 0)
        {
            printNumberOfBackslashes(65, 10, true, false);
            std::cout << "\t\tDu tapte!\n";
            printNumberOfBackslashes(65, 10, false, true);
            break;
        }

        printNumberOfBackslashes(65, 2, true, true);
    }
    while(numberOfRightGuessesInPosition < SIZE);

    if(numberOfRightGuesses == SIZE)
        std::cout << "Gratulerer! Du klarte koden.\n";

}

int checkCharactersAndPosition(char guess[], char code[], int sizeOfArrays)
{
    int correctCharAndPos = 0;
    for(int i = 0; i < (sizeOfArrays); i++)
    {
        if (guess[i] == code[i])
            correctCharAndPos++;
    }
    return correctCharAndPos;
}
int checkCharacters(char guess[], char code[], int sizeOfArrays, int numberOfPossibleLetters)
{
    int correctChars = 0;

    for(char checkChar = 'A'; checkChar < ('A' + numberOfPossibleLetters - 1); checkChar++)
    {
        int numberOfGuessOfChar = countOccurencesOfCharacter(guess, sizeOfArrays + 1, checkChar);
        int numberOfCodeOfChar = countOccurencesOfCharacter(code, sizeOfArrays + 1, checkChar);

        if(numberOfGuessOfChar <= numberOfCodeOfChar)
            correctChars += numberOfGuessOfChar;
        else
            correctChars += numberOfCodeOfChar;
    }
    return correctChars;
}
