#include "matrix.h"

using namespace std;

// CONSTRUCTORS
Matrix::Matrix()
{
    mPtr = nullptr; 
    rows = 0;
    columns = 0;
}

Matrix::Matrix(unsigned int nRows, unsigned int nColumns)
{
    rows = nRows;
    columns = nColumns;

    mPtr = new double*[nRows];
    for(unsigned int i = 0; i < nRows; i++)
    {
        mPtr[i] = new double[nColumns];
        for(unsigned int j = 0; j < nColumns; j++)
        {
            mPtr[i][j] = 0.0;
        }
    }
}

Matrix::Matrix(unsigned int nRows)
{
    rows = nRows;
    columns = nRows;

    mPtr = new double*[nRows];
    for(unsigned int i = 0; i < nRows; i++)
    {
        mPtr[i] = new double[nRows];
        for(unsigned int j = 0; j < nRows; j++)
        {
            mPtr[i][j] = 0.0;
        }
    }
}

Matrix::~Matrix()
{
    if(!this->isValid())
    {
        //cout << "Du unngikk å prøve å destruere en ugyldig matrise?" << endl;
        return;
    }
    //cout << "Destructor ble kalt på\n" << *this << endl;

    for(int i = 0; i < (*this).getHeight(); i++)
    {
        delete[] mPtr[i];
    }
    delete[] mPtr;

    mPtr = nullptr;

}

// MEMBER FUNCTIONS
double Matrix::get(unsigned int row, unsigned int col) const
{
    if(mPtr == nullptr)
        return 0;
    return mPtr[row][col];
}

void Matrix::set(unsigned int row, unsigned int col, double value)
{
    if(mPtr == nullptr)
        return;
    mPtr[row][col] = value;
}

void Matrix::setPtrToNullPtr()
{
    mPtr = nullptr;
}

bool Matrix::isValid() const
{
    if(mPtr == nullptr)
        return false;
    else
        return true;
}

int Matrix::getHeight() const
{
    return rows;
}

int Matrix::getWidth() const
{
    return columns;
}

// Oppg 3g)
Matrix testCopyConstr(Matrix m)
{
    return m;
}

std::ostream& operator<<(std::ostream& os, const Matrix& obj)
{
    if(!obj.isValid())
    {
        cout << "Du prøvde å skrive ut en ugyldig matrise." << endl;
        return os;
    }
    for(int i = 0; i < obj.getHeight(); i++)
    {
        os << "[\t";
        for(int j = 0; j < obj.getWidth(); j++)
        {
            os << obj.get(i, j) << "\t";
        }
        os << "]\n";
    }
    return os;
}

// Oppg 4)
Matrix::Matrix(const Matrix &rhs) : mPtr(nullptr)
{
    if(!rhs.isValid())
        cout << "(copyconstructor) Du brukte copy constructor på en ikke-valid matrise!\n";
    // Matrix A2(A) <--- her er A == rhs

    rows = rhs.getHeight();
    columns = rhs.getWidth();
    // Har nå størrelsen på A.
    //cout << "Størrelse: " << rows << "x" << columns << endl;

    // THIS betyr nå f.eks A2, da THIS er en referanse til objektet som får sin konstruktør kalt.

    mPtr = new double*[rows];
    for(unsigned int i = 0; i < rows; i++)
    {
        mPtr[i] = new double[columns];
        for(unsigned int j = 0; j < columns; j++)
        {
            //this->mPtr[i][j] = rhs.get(i, j); 
            mPtr[i][j] = rhs.get(i, j);
        }
    }
    // DELETE
}

Matrix& Matrix::operator=(Matrix rhs) 
{
    if(!rhs.isValid())
        cout << "(= operator) Du brukte copy constructor på en ikke-valid matrise!\n";

    rows = rhs.getHeight();
    columns = rhs.getWidth();


    mPtr = new double*[rows];
    for(unsigned int i = 0; i < rows; i++)
    {
        mPtr[i] = new double[columns];
        for(unsigned int j = 0; j < columns; j++)
        {
            mPtr[i][j] = rhs.get(i, j); 
            //std::swap(mPtr[i][j], rhs.mPtr[i][j]);
            // any difference? 
        }
    }

    // Delete fordi det ble opprettet en lokal kopi av rhs, som da har gått gjennom copy-constructor og fått tildelt eventuelt wasted minne.
    // Iterasjon?
    //delete[] rhs.mPtr;
    //rhs.~Matrix();
    // UNØDVENDIG! Destruktør auto-kalt på rhs.

    return *this;
    // Her returnerer man selve objektet. Returtypen er Matrix& pga da kan man gjøre A = B = C = ... , men hvorfor funker det når man returnerer Matrix (ikke &Matrix)? 
}

Matrix& Matrix::operator+=(const Matrix &rhs)
{
    if(this->getHeight() != rhs.getHeight() || this->getWidth() != rhs.getWidth())
    {
        this->~Matrix();
        return *this;
        // maybe?
    }


    for(unsigned int i = 0; i < rows; i++)
    {
        for(unsigned int j = 0; j < columns; j++)
        {
            mPtr[i][j] = mPtr[i][j] + rhs.get(i, j); 
        }
    }

    return *this;
}

Matrix Matrix::operator+(const Matrix &rhs)
{
    Matrix m(*this);

    m += rhs;

    return m;
}
