#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

class Matrix
{
    private:
        // MEMBER VARIABLES
        unsigned int rows;
        unsigned int columns;
        double** mPtr;

    public:
        // CONSTRUCTORS, DESTRUCTOR
        Matrix();
        Matrix(unsigned int nRows, unsigned int nColumns);
        explicit Matrix(unsigned int nRows);
        ~Matrix();

        // MEMBER FUNCTIONS
        double get(unsigned int row, unsigned int col) const;
        void set(unsigned int row, unsigned int col, double value);
        void setPtrToNullPtr();
        bool isValid() const;
        int getHeight() const;
        int getWidth() const;

        // OVERLOAD
        Matrix operator+(const Matrix &rhs);
        Matrix& operator+=(const Matrix &rhs);
        Matrix& operator=(Matrix rhs);
        Matrix(const Matrix &rhs); 
};

std::ostream& operator<<(std::ostream& os, const Matrix& obj);

// For oppg 3g)
Matrix testCopyConstr(Matrix m);

#endif
