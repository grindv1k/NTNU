#include <iostream>
#include "oppg1.h"

using namespace std;

void fillInFibonacciNumbers(int result[], int length)
{
    int f1 = 0;
    int f2 = 1;

    if(length <= 0)
        return;
    else if(length == 1)
        result[0] = f1;
    else if(length == 2)
    {
        result[0] = f1;
        result[1] = f2;
    }
    else
    {
        result[0] = f1;
        result[1] = f2;
        for(int i = 2; i < length; i++)
        {
            result[i] = result[i - 1] + result[i - 2];
        }
    }
}


void printArray(int arr[], int length)
{
    cout << "\n[ ";
    for(int i = 0; i < length; i++)
    {
        cout << arr[i] << " ";
    }
    cout << "]\n";
}

void createFibonacci()
{
    // 1. Spør brukeren hvor mange tall som skal genereres
    int howLong;
    cout << "createFibonacci() -- hvor mange tall skal genereres?\n";
    cin >> howLong;

    // 2. Alloker minne til en tabell som er stor nok til tallrekka
    int* tablePtr = new int[howLong];

    // 3. Fylle tabellen med funksjonen fillInFibonacciNumbers
    fillInFibonacciNumbers(tablePtr, howLong);

    // 4. Skriv ut resultatet til skjerm med printArray
    printArray(tablePtr, howLong);

    // 5. Frigjør minnet du har reservert
    delete[] tablePtr;
}
