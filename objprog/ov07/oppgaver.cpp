#include <iostream>
#include "oppgaver.h"
#include "oppg1.h"
#include "matrix.h"

using namespace std;

void oppgave1()
{
   createFibonacci(); 
}
void oppgave2()
{
    // Opprett Matrise med alle konstruktører
    Matrix a;
    Matrix b(3, 5);
    Matrix c(4);

    // Skriv ut verdien fra hver matrise
    cout << "Matrise a (default constructor):\n" << a << endl;
    cout << "Matrise b(3, 5):\n" << b << endl;
    cout << "Matrise c(4):\n" << c << endl;

    // Test medlemsfunksjoner
    b.set(1, 1, 33.3);
    b.set(2, 4, 77.7);

    cout << "Setter matrise b slik: b[1][1] = 33.3, b[2][4] = 77.7\n"
        << "Printer så matrisen igjen:\n" << b << endl 
        << "Prøver så b.get(2, 4):\n" << b.get(2, 4) << endl
        << "Prøver b.getHeight() og b.getWidth():\n" <<
        b.getHeight() << "\t" << b.getWidth() << endl << "Alt er nå testet.\n";
}
void oppgave3()
{
    // a) -- Lag to matriser A og B, forskjellige verdier
    Matrix A(2, 2);
    A.set(0, 0, 3.4);
    A.set(0, 1, 2.0);
    A.set(1, 0, 0.8);
    A.set(1, 1, 8.9);
    cout << "Lager matrisen A(2, 2), og setter verdier. Ser slik ut:\n" << A << endl;

    Matrix B(2, 2);
    B.set(0, 0, 6.6);
    B.set(0, 1, 7.7);
    B.set(1, 0, 9.9);
    B.set(1, 1, 2.2);

    // Litt teori i a)
    cout << "Lager matrisen B(2, 2), og setter verdier. Ser slik ut:\n" << B << endl;

    cout << "\nNå settes A = B.\nOperatoren '=' lager en binær kopi.\n";
    A = B;
    cout << "Om man nå endrer verdiene i B, endrer man også de i A, pga pointers.\n";

    // b) -- Lag matrise C, ved å skrive:
    Matrix C(A);
    // Dette er vel det samme som å skrive Matrix C, og så C = A? NEI!
    // Kopikonstruktør.
    // cout << "\nSkrev nå MatrixC(A). Sjekker om den er lik A:\n" << C << endl;
    // cout << "\nSkrev nå MatrixC(A). Sjekker getHeight():\n" << C.getHeight() << endl;

    // c) -- Lag matrise D, ved:
    Matrix D = B;
    // Kopikonstruktør.

    // d) -- A = B brukte assignment operator. Kun den.
    // e) -- Lager nå kopikonstruktørdeklarasjon. Viste seg at M C(A) og 
    // M D = B begge kalte kopikonstuktørfunksjonen.

    // f) -- Nei.

    // g) -- Lager Matrix testCopyConstr(Matrix m)
    // Tjohei! Funket ikke pga deleted copy constructor.
}

void oppgave4()
{
    // Implementer kopikonstruktør til Matrix-klassen

    Matrix A(2, 2);
    A.set(0, 0, 3.4);
    A.set(0, 1, 2.0);
    A.set(1, 0, 0.8);
    A.set(1, 1, 8.9);
    cout << "Lager matrisen A(2, 2), og setter verdier. Ser slik ut:\n";
    cout << "A:\n" << A << endl;

    Matrix A2(A);
    cout << "Lager matrisen A2(2, 2), ved å skrive matrix A2(A):\n";
    cout << "A2 :\n" << A2 << endl;

    cout << "Endrer nå et element i A for å sjekke om spooky\n";
    A.set(0, 0, 400.123);

    cout << "A:\n" << A << endl;
    cout << "A2 :\n" << A2 << endl;

    cout << "Bruker nå = operatoren. Lager en matrise B, og skriver så B = A2\n";
    Matrix B;

    B = A2;

    cout << "B:\n" << B << endl;
    cout << "A2 :\n" << A2 << endl;
} 
void oppgave5()
{
    cout << "Oppgave 5. Lager tre 2x2 matrix, A, B, C.\n";
    Matrix A(2);
    Matrix B(2);
    Matrix C(2);

    A.set(0, 0, 1.0);
    A.set(0, 1, 2.0);
    A.set(1, 0, 3.0);
    A.set(1, 1, 4.0);

    B.set(0, 0, 4.0);
    B.set(0, 1, 3.0);
    B.set(1, 0, 2.0);
    B.set(1, 1, 1.0);
    
    C.set(0, 0, 1.0);
    C.set(0, 1, 3.0);
    C.set(1, 0, 1.5);
    C.set(1, 1, 2.0);
    cout << "A: \n" << A << endl;
    cout << "B: \n" << B << endl;
    cout << "C: \n" << C << endl;
    Matrix D(3);

    A += B;
    cout << "Prøver nå A += B:\nA =\n" << A << endl;
    B += C;
    cout << "Prøver nå B += C:\nB =\n" << B << endl;

    cout << "Prøver så å ta += mellom en 2x2 og en 3x3 matrise:\n";
    C += D;
    cout << "C = \n" << C << endl;


    cout << "Matrisen C er ugyldig. Bruker derfor matrisen E.\n";
    Matrix E(2);
    E.set(0, 0, 1.0);
    E.set(0, 1, 3.0);
    E.set(1, 0, 1.5);
    E.set(1, 1, 2.0);
    cout << "A:\n" << A << "B:\n" << B << "E:\n" << E << endl;
    cout << "Prøver til slutt A += B + E:\n";

    A += B + E;

    cout << "A:\n" << A << endl;
}
