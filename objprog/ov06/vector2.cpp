#include <cmath>
#include "vector2.h"

// CONSTRUCTORS
Vector2::Vector2()
{
    (*this).vValues[0] = 0.0;
    (*this).vValues[1] = 0.0;
}

Vector2::Vector2(double v11, double v21)
{
    (*this).vValues[0] = v11;
    (*this).vValues[1] = v21;
}

// OVERLOADS
ostream& operator<< (ostream& outputStream, const Vector2& v)
{
    outputStream << "[ " << v.vValues[0] << " ]\n"
        << "[ " << v.vValues[1] << " ]\n";

    return outputStream;
}

// MEMBER FUNCTIONS
void Vector2::set(double v11, double v21)
{
    (*this).vValues[0] = v11;
    (*this).vValues[1] = v21;
}

double Vector2::get(int whichValue) const
{
    return (*this).vValues[whichValue];
}

double Vector2::dot(const Vector2 &rightHandSide) const
{
    return (*this).vValues[0] * rightHandSide.vValues[0] +
        (*this).vValues[1] * rightHandSide.vValues[1];
}

double Vector2::lengthSquared() const
{
    return (*this).dot(*this);
    //return (*this).vValues[0] * (*this).vValues[0] + (*this).vValues[1] * (*this).vValues[1];
}

double Vector2::length() const
{
    return sqrt(lengthSquared());
}
