#ifndef VECTOR2_H_
#define VECTOR2_H_

#include <iostream>
#include "matrix2x2.h"

using namespace std;

class Vector2
{
    private:
        double vValues[2];

    public:
        void set(double v11, double v21);
        double get(int whichValue) const;

        double dot(const Vector2 &rightHandSide) const;
        double lengthSquared() const;
        double length() const;

        // Constructor
        Vector2();
        Vector2(double v11, double v21);

        // Overload
        friend ostream& operator <<(ostream& outputStream, const Vector2& v);
};

#endif
