#include <iostream>
#include "vector2.h"
#include "matrix2x2.h"

using namespace std;

// Class functions
double Matrix2x2::get(int row, int column) const
{
    return mValues[row][column];
}

void Matrix2x2::set(int row, int column, double value)
{
    mValues[row][column] = value;
}


// Constructors
Matrix2x2::Matrix2x2()
{
    mValues[0][0] = 1.0;
    mValues[1][0] = 0.0;
    mValues[0][1] = 0.0;
    mValues[1][1] = 1.0;
}

// Default constructor sets matrix equal to identity matrix
Matrix2x2::Matrix2x2(double m11, double m12, double m21, double m22)
{
    mValues[0][0] = m11;
    mValues[0][1] = m12;
    mValues[1][0] = m21;
    mValues[1][1] = m22;
}

void Matrix2x2::printMatrix()
{
    cout << "[" << mValues[0][0] << "\t" << mValues[0][1] << "]\n"
        << "[" << mValues[1][0] << "\t" << mValues[1][1] << "]\n\n";
}

std::ostream& operator<<(std::ostream& out, const Matrix2x2& matrix)
{
    return out << "[" << matrix.mValues[0][0] << "\t" << matrix.mValues[0][1] << "]\n"
        << "[" << matrix.mValues[1][0] << "\t" << matrix.mValues[1][1] << "]\n\n";
}

Matrix2x2& Matrix2x2::operator+=(const Matrix2x2& m1)
{
    (*this).mValues[0][0] += m1.mValues[0][0];
    (*this).mValues[1][0] += m1.mValues[1][0];
    (*this).mValues[0][1] += m1.mValues[0][1];
    (*this).mValues[1][1] += m1.mValues[1][1];

    return *this;
}


Matrix2x2& Matrix2x2::operator-=(const Matrix2x2& m1)
{
    (*this).mValues[0][0] -= m1.mValues[0][0];
    (*this).mValues[1][0] -= m1.mValues[1][0];
    (*this).mValues[0][1] -= m1.mValues[0][1];
    (*this).mValues[1][1] -= m1.mValues[1][1];

    return *this;
}

Matrix2x2& Matrix2x2::operator*=(const Matrix2x2& m1)
{
    return *this = (*this) * m1;
}

Matrix2x2 Matrix2x2::operator+(const Matrix2x2& m1)
{
    return *this += m1;
}

Matrix2x2 Matrix2x2::operator-(const Matrix2x2& m1)
{
    return *this -= m1;
}

Vector2 Matrix2x2::operator*(const Vector2& v1)
{
    Vector2 v;
    v.set( 
         (*this).mValues[0][0] * v1.get(0) +
         (*this).mValues[0][1] * v1.get(1), 
         (*this).mValues[1][0] * v1.get(0) +
         (*this).mValues[1][1] * v1.get(1));
    return v;
}

Matrix2x2 Matrix2x2::operator*(const Matrix2x2& m1)
{
    Matrix2x2 m = Matrix2x2(0, 0, 0, 0);

    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            for(int k = 0; k < 2; k++)
            {
                m.mValues[i][j] = m.mValues[i][j] + (*this).mValues[i][k] * m1.mValues[k][j];
            }
        }
    }

    return m;
}

double Matrix2x2::det() const
{
    return (*this).mValues[0][0] * (*this).mValues[1][1] - (*this).mValues[0][1] * (*this).mValues[1][0];
}

Matrix2x2 Matrix2x2::inverse() const
{
    Matrix2x2 m = Matrix2x2(0, 0, 0, 0);

    if( (*this).det() == 0 )
    {
        cout << "Ikke inverterbar!\n";
        return m; 
    }
    else 
    {
        double determinant = (*this).det();
        m.mValues[0][0] = (1 / determinant) * (*this).mValues[1][1];
        m.mValues[1][0] = -(1 / determinant) * (*this).mValues[1][0];
        m.mValues[0][1] = -(1 / determinant) * (*this).mValues[0][1];
        m.mValues[1][1] = (1 / determinant) * (*this).mValues[0][0];

        return m;
    }
}
