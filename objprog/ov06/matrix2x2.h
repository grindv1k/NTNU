#ifndef MATRIX2X2_H_
#define MATRIX2X2_H_

#include<iostream>
#include "vector2.h"

class Vector2;

class Matrix2x2
{
    private :
        double mValues[2][2];
        friend std::ostream& operator<<(std::ostream& out, const Matrix2x2& matrix);

    public :
        double get(int row, int column) const;
        void set(int row, int column, double value);
        void printMatrix();
        double det() const;
        Matrix2x2 inverse() const;

        Matrix2x2();
        // Default constructor sets matrix equal to identity matrix
        Matrix2x2(double m11, double m12, double m21, double m22);

        Matrix2x2& operator+=(const Matrix2x2& m1);
        Matrix2x2& operator-=(const Matrix2x2& m1);

        Matrix2x2 operator+(const Matrix2x2& m1);
        Matrix2x2 operator-(const Matrix2x2& m1);
        Matrix2x2 operator*(const Matrix2x2& m1);
        Vector2 operator*(const Vector2& v1);

        Matrix2x2& operator*=(const Matrix2x2& m1);
};

#endif
