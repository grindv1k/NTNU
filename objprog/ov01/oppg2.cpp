#include<iostream>

////////////////////////////////////////////
////////////////////////////////////////////

int maxOfTwo(int a, int b);
int fibonacci(int n);
int squareNumberSum(int n);
void triangleNumbersBelow(int n);
bool isTriangleNumber(int number);
bool isPrime(int n);
void naivePrimeNumberSearch(int n);
int findGreatestDivisor(int n);
void compareListOfNumbers(int l[], int sizeOfInputArray);

////////////////////////////////////////////
////////////////////////////////////////////

int main() {

	maxOfTwo(3, 10);
	maxOfTwo(100, 10);

	fibonacci(10);

	squareNumberSum(4);

	triangleNumbersBelow(100);
	
	isTriangleNumber(4);
	isTriangleNumber(91);
	isTriangleNumber(10);

	isPrime(3);
	isPrime(100);
	isPrime(39);
	isPrime(30);

	naivePrimeNumberSearch(20);

	findGreatestDivisor(20);
	findGreatestDivisor(39);
	findGreatestDivisor(71);
	findGreatestDivisor(9);
	
	int intArray[5] = {1, -5, 300, 0, 0};
	int size = 5;
	compareListOfNumbers(intArray, size);

	int intArray2[7] = {0, 0, 0, 400, 400, 13, -900};
	int size2 = 7;
	compareListOfNumbers(intArray2, size2);


	return 0;
}


////////////////////////////////////////////
////////////////////////////////////////////

int maxOfTwo(int a, int b){
	if (a > b) {
		std::cout << "A > B" << std::endl;
		return a;
	}
	else {
		std::cout << "B < A" << std::endl;
		return b;
	}
}

////////////////////////////////////////////
////////////////////////////////////////////

int fibonacci(int n){
	int a = 0;
	int b = 1;
	int temp = 0;
	std::cout << "Fibby number: " << std::endl;
	for (int x = 1; x < n + 1; x++) {
		std::cout << x << ": " << b << std::endl;
		temp = b;
		b = a + b;
		a = temp;
	}
	std::cout << "----" << std::endl;
	return b;
}

////////////////////////////////////////////
////////////////////////////////////////////

int squareNumberSum(int n){
	int totalSum = 0;
	for (int i = 0; i < n + 1; i++) {
		totalSum = totalSum + i*i;
		std::cout << i*i << std::endl;
	}
	std::cout << totalSum << std::endl;
	return totalSum;
}

////////////////////////////////////////////
////////////////////////////////////////////


void triangleNumbersBelow(int n){
	int acc = 1;
	int num = 2;
	std::cout << "Triangle numbers below " << n << ": " << std::endl;
	while (acc + num < n) {
		acc += num;
		num++;
		std::cout << acc << " ";
	}
	std::cout << std::endl;
}

////////////////////////////////////////////
////////////////////////////////////////////

bool isTriangleNumber(int number){
	int acc = 1;
	while (number > 0){
		number -= acc; 
		acc++;
	}
	if (number == 0){
		return true;
	}
	else {
		return false;
	}
}

bool isPrime(int n){
	for(int i = 2; i < n; i++) {
		if((n % i) == 0){
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////
////////////////////////////////////////////

void naivePrimeNumberSearch(int n){
	for(int number = 2; number < n; number++){
		if(isPrime(number))
			std::cout << number << " is a prime" << std::endl;
	}
}

////////////////////////////////////////////
////////////////////////////////////////////

int findGreatestDivisor(int n){
	for(int divisor = n-1; divisor >= 0; divisor--){
		if( (n % divisor) == 0) {
			return divisor;
		}
	}
	return 1;
}

////////////////////////////////////////////
////////////////////////////////////////////

void compareListOfNumbers(int l[], int sizeOfInputArray){
	int r[3] = {0, 0, 0}; 
	for(int i = 0; i < sizeOfInputArray; i++) {
		if(l[i] < 0){
			r[0] += 1;
		} else if(l[i] == 0){
			r[1] += 1;
		} else {
			r[2] += 1;
		}
	}
	std::cout << r[0] << " numbers were below zero" << std::endl;
	std::cout << r[1] << " numbers were zero" << std::endl;
	std::cout << r[2] << " numbers were greater than zero" << std::endl;
}
