// Oversett følgende til Matlab:
#include <iostream>

bool isFib(int n);
int userNumber = 0;

int main() {
	std::cout << "Enter number ... " << std::endl;
	std::cin >> userNumber;
	if(isFib(userNumber)){
		std::cout << "Your number was Fib" << std::endl;
	} else if(!isFib(userNumber)){
		std::cout << "Your number was not Fib" << std::endl;
	}

	return 0;
}

bool isFib(int n){
	int a = 0;
	int b = 1;
	while (b < n){
		int temp = b;
		b = a + b;
		a = temp;
	}
	if (b == n){
		return true;
	}
	else{
		return false;
	}
}
