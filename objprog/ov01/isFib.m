function [ bool ] = isFib( intToCheck )
% Check if "intToCheck" is a Fib. number. Return result (as bool?)
a = 0;
b = 1;
while b < intToCheck
    temp = b;
    b = a + b;
    a = temp;
end

if b == intToCheck
    bool = true;
else 
    bool = false;
end

