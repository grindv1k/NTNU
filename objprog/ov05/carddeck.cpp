#include <iostream>
#include <cstdlib>
#include "carddeck.h"
#include "card.h"
#include "enums.h"

using namespace std;

CardDeck::CardDeck()
{
    currectCardIndex = 0;

    for(int i = 0; i < 13; i++)
    {
        cards[i + 0*13] = Card(CLUBS, static_cast<Rank>(i+2));
        cards[i + 1*13] = Card(DIAMONDS, static_cast<Rank>(i+2));
        cards[i + 2*13] = Card(SPADES, static_cast<Rank>(i+2));
        cards[i + 3*13] = Card(HEARTS, static_cast<Rank>(i+2));
    }
}

void CardDeck::swap(int index1, int index2)
{
    // Should be between 0 (including) and 51 (including).
    Card temp = cards[index1];
    cards[index1] = cards[index2];
    cards[index2] = temp;
}

void CardDeck::print()
{
    for(int i = 0; i < 52; i++)
    {
        cout << i << ": " << cards[i].toString() << endl;
    }
}
void CardDeck::printShort()
{
    for(int i = 0; i < 52; i++)
    {
        cout << i << ": " << cards[i].toStringShort() << endl;
    }
}

void CardDeck::shuffle()
{
    for(int i = 0; i < 10000; i++)
    {
        swap(rand() % 52, rand() % 52);
    }
}

Card CardDeck::drawCard()
{
    return cards[currectCardIndex++];
}
