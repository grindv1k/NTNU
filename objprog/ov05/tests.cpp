#include <iostream>
#include <cstdlib>
#include "card.h"
#include "tests.h"
#include "enums.h"
#include "carddeck.h"
#include "blackjack.h"

using namespace std;

void testOppg2()
{
    //CardStruct testCard;
    //testCard.cardSuit = DIAMONDS;
    //testCard.cardRank = SEVEN;

    // Initialize like this?
    // CardStruct quickCard = {SPADES, ACE};
    // Works fine.

    //string toStr = toString();
    //string toStrShort = toStringShort();

    //// OUT
    //cout << "toString(testCard) returnerer:\n" << toStr << endl;
    //cout << "toStringShort(testCard) returnerer:\n" << toStrShort << endl;
}

void testOppg3()
{
    //Card myCard = Card();
    //myCard.getSuit();
}

void testOppg4()
{
    srand (time(NULL));

    CardDeck bestDeck = CardDeck();
    bestDeck.print();
    bestDeck.printShort();
    bestDeck.shuffle();
    bestDeck.print();
    cout << "Du trekker nå det øverste kortet." << endl;
    Card topCard = bestDeck.drawCard();
    cout << "Kortet var: " << topCard.toString() << endl;
    cout << "Du trekker et til." << endl;
    topCard = bestDeck.drawCard();
    cout << "Kortet var: " << topCard.toString() << endl;
}

void testOppg5()
{
    srand (time(NULL));

    Blackjack newGame;
    newGame.playGame();
}
