#ifndef CARDDECK_H_
#define CARDDECK_H_

#include "card.h"

class CardDeck
{
    private:
        Card cards[52];
        int currectCardIndex;
        // Member functions
        void swap(int index1, int index2);

    public:
        // Member functions
        void print(); 
        void printShort();
        void shuffle();
        Card drawCard();

        // Constructors
        CardDeck();
};

#endif
