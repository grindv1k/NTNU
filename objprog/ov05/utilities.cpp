#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string intToString(int number)
{
    std::stringstream ss;
    ss << number;
    return ss.str();
}

void printNumberOfBackslashes(int howManyPerLine, int lines, bool prependNewline, bool appendNewline)
{
    if(prependNewline)
        cout << "\n";

    for(int j = 0; j < lines; j++)
    {
        if(j >= 1)
            cout << "\n";

        for(int i = 0; i < howManyPerLine; i++)
        {
            cout << "\\";
        }
    }

    cout << "\n";

    if(appendNewline)
        cout << "\n";
}
