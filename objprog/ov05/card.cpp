#include <iostream>
#include <string>
#include "enums.h"
#include "utilities.h"
#include "card.h"

// Member functions

string Card::toString()
{
    if(invalid == false)
    {
        return 
            rankToString(cRank) +
            " of " +
            suitToString(cSuit);
    }
    else
        return "Ugyldig kort";
}

string Card::toStringShort()
{
    string str;

    switch (cSuit)
    {
        case CLUBS :
            str = "C";
            break;
        case DIAMONDS :
            str = "D";
            break;
        case HEARTS :
            str = "H";
            break;
        case SPADES :
            str = "S";
            break;
    }

    str += intToString(cRank);

    return str;
}

void Card::initialize(Suit s, Rank r)
{
    cSuit = s;
    cRank = r;
    invalid = false;
}

Suit Card::getSuit()
{
    return cSuit;
}

Rank Card::getRank()
{
    return cRank;
}

// Constructors

Card::Card(Suit s, Rank r)
{
    initialize(s, r);
}

// Other functions

string suitToString(Suit suit)
{
    string s;
    switch(suit)
    {
        case CLUBS :
            return s = "Clubs";
        case DIAMONDS :
            return s = "Diamonds";       
        case HEARTS :
            return s = "Hearts";
        case SPADES :
            return s = "Spades";
    }
}
string rankToString(Rank rank)
{
    string s;
    switch(rank)
    {
        case TWO :
            return s = "Two";
        case THREE :
            return s = "Three";
        case FOUR :
            return s = "Four";
        case FIVE :
            return s = "Five";
        case SIX :
            return s = "Six";
        case SEVEN :
            return s = "Seven";
        case EIGHT :
            return s = "Eight";
        case NINE :
            return s = "Nine";
        case TEN :
            return s = "Ten";
        case JACK :
            return s = "Jack";
        case QUEEN :
            return s = "Queen";
        case KING :
            return s = "King";
        case ACE :
            return s = "Ace";
    }
}
