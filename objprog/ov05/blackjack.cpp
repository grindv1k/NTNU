#include <iostream>
#include "card.h"
#include "utilities.h"
#include "enums.h"
#include "carddeck.h"
#include "blackjack.h"

using namespace std;

bool Blackjack::isAce(Card *card)
{
    if( (*card).getRank() == ACE)
        return true;
    else
        return false;
}

int Blackjack::getCardValue(Card *card)
{
    int cardValue = (*card).getRank();
    if(cardValue <= 10)
        return cardValue;
    else if( (cardValue < 14) && (cardValue > 10) )
        return 10;
    else
        return -1;
}

int Blackjack::getPlayerCardValue(Card *card)
{
    int cardValue;

    if(isAce(card))
    {
        int choice = 0;
        do
        {
            cout << "Skal ess være verdt 1 eller 11? Skriv ønsket verdi: ";
            cin >> choice;
        } while( (choice != 1) && (choice != 11) );
        cardValue = choice;
    }
    else
        cardValue = getCardValue(card);

    return cardValue;
}

int Blackjack::getDealerCardValue(Card *card, int dealerHand)
{
    int cardValue;

    if(isAce(card))
    {
        if( (dealerHand + 11) > 21 )
            cardValue = 1;
        else
            cardValue = 11;
    }
    else
        cardValue = getCardValue(card);

    return cardValue;
}

bool Blackjack::askPlayerDrawCard()
{
    bool choice = true;;
    char input;

    cout << "Vil du trekke et kort til? (y/n) ";
    cin >> input;
    do
    {
        if( (input == 'y') || (input == 'Y') )
            choice = true;
        else if( (input == 'n') || (input == 'N') )
            choice = false;
    } while( (input != 'n') && (input != 'N') && (input != 'y') && (input != 'Y') );

    return choice;
}
void Blackjack::drawInitialCards()
{
    Card pCard1 = deck.drawCard();
    Card pCard2 = deck.drawCard();
    playerCardsDrawn = 2;
    playerHand = getPlayerCardValue(&pCard1);
    cout << "Du trekker ditt første kort.\n";
    cout << "Player: " << playerHand << " --- " << pCard1.toString() << endl;
    cout << "Du trekker ditt andre kort.\n";
    playerHand += getPlayerCardValue(&pCard2);
    cout << "Player: " << playerHand << " --- " << pCard2.toString() << endl;

    Card dCard1 = deck.drawCard();
    Card dCard2 = deck.drawCard();
    dealerCardsDrawn = 2;
    dealerHand = getDealerCardValue(&dCard1, 0);
    cout << "Dealer: " << dealerHand << " --- " << dCard1.toString() << endl;
    dealerHand += getDealerCardValue(&dCard2, dealerHand);
    cout << "Dealer trekker et kort til, og har nå " << dealerCardsDrawn << " kort.\n";
}

void Blackjack::playGame()
{    
    GameState state = GAMEONGOING;
    bool playerWantedCard;

    deck = CardDeck();
    deck.shuffle();
    drawInitialCards();

    Card cardDrawn;

    while(state == GAMEONGOING)
    {
        playerWantedCard = askPlayerDrawCard();

        if(playerWantedCard)
        {
            cardDrawn = deck.drawCard();
            playerHand += getPlayerCardValue(&cardDrawn);
            playerCardsDrawn++;
            cout << "Player: " << playerHand << " --- " << cardDrawn.toString() << endl;
        }

        if(playerHand > 21)
        {
            state = TIMETODECIDE;
        }

        if( (dealerHand < 17) && (state == GAMEONGOING) )
        {
            cardDrawn = deck.drawCard();
            dealerHand += getDealerCardValue(&cardDrawn, dealerHand);
            dealerCardsDrawn++;
            cout << "Dealer trekker et kort til, og har nå " << dealerCardsDrawn << " kort.\n";
        }

        if(dealerHand > 21)
        {
            state = TIMETODECIDE;
        }

        if( !playerWantedCard && dealerHand >= 17)
        {
            state = TIMETODECIDE; 
        }
    }

    //// GAME IS OVER; SHOW HANDS
    cout << "GAME OVER!\n";
    printNumberOfBackslashes(50, 2, false, true);
    cout << "Player: " << playerHand << endl;
    cout << "Dealer: " << dealerHand << endl;

    if( (playerHand == 21) && (playerHand == dealerHand) && (playerCardsDrawn == 2) && (dealerCardsDrawn > 2) )
    {
        // Player has blackjack 2 cards
        cout << "Spilleren vant ved blackjack!\n"; 
    }
    else if( (playerHand == 21) && (playerCardsDrawn == 2) && (playerHand > dealerHand) )
    {
        // Player has blackjack 2 cards
        cout << "Spilleren vant ved blackjack!\n"; 
    }
    else if( playerHand == dealerHand )
    {
        // Equal value but not player blackjack
        cout << "Spilleren tapte ved lik verdi uten blackjack!\n";
    }
    else if( playerHand > 21)
        cout << "Spilleren tapte, over 21!\n";
    else if( dealerHand > 21)
        cout << "Spilleren vant, dealer over 21!\n";
    else if( playerHand > dealerHand )
        cout << "Spilleren vant ved høyere verdi!\n";
    else
        cout << "Spilleren tapte ved lavere verdi!\n";
}
