#ifndef CARD_H_
#define CARD_H_

#include <string>
#include "enums.h"

using namespace std;

class Card 
{

    private:
        Suit cSuit;
        Rank cRank;
        bool invalid;

    public:
        // Member functions
        void initialize(Suit s, Rank r);
        string toString();
        string toStringShort();
        Suit getSuit();
        Rank getRank();
        // Constructors
        Card(){invalid = true;};
        Card(Suit s, Rank r);
};

string suitToString(Suit s);
string rankToString(Rank r);

#endif
