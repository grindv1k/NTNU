#ifndef BLACKJACK_H_
#define BLACKJACK_H_

#include "carddeck.h"
#include "card.h"

class Blackjack
{
    private:
        CardDeck deck;
        int playerHand;
        int dealerHand;
        int playerCardsDrawn;
        int dealerCardsDrawn;
    public:
        bool isAce(Card *card);
        int getCardValue(Card *card);
        int getPlayerCardValue(Card *card);
        int getDealerCardValue(Card *card, int dealerHand);
        bool askPlayerDrawCard();
        void drawInitialCards();
        void playGame();
};

#endif
