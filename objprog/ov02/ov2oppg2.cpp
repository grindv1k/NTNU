#include <iostream>
 
void inputAndPrintInteger();
int intputInteger();
void inputIntegersAndPrintSum(int int1, int int2);
bool isOdd(int integ);
void printHumanReadableTime(int timeInSec);
void inputIntegersUsingLoopAndPrintSum();
double inputDouble();
void nokToEuro();

int main() {
	///// Oppgave 1
	// Oppgave a)
	//inputAndPrintInteger();

	// Oppgave b)
	//int nmbr = intputInteger();
	//std::cout << "Du skrev: " << nmbr << std::endl;

	// Oppgave c)
	//int a = intputInteger();
	//int b = intputInteger();
	//inputIntegersAndPrintSum(a, b);

	// Oppgave d) 
	//Ke for nokke	

	// Oppgave e)
	//for (int i = 10; i < 15; i++) {
	//if(isOdd(i)) {
	//std::cout << i << " er et oddetall." << std::endl;
	//} else {
	//std::cout << i << " er et partall." << std::endl;
	//}
	//}

	// Oppgave f)
	printHumanReadableTime(14401);
	
	//// Oppgave 2
	// Oppgave a)
	//inputIntegersUsingLoopAndPrintSum();

	// Oppgave b)
	// Dersom man vet hvor mange tall: for-løkke. Kan jo da bestemme antall iterasjoner ut i fra informasjonen.
	// Dersom ikke, bruke while, da man kan bruke f.eks null som "utgang" og la loopen ellers gå "evig".

	// Oppgave c)
	//double sum = inputDouble();
	//std::cout << "Summen ble: " << sum << "." << std::endl;

	// Oppgave d)
	//nokToEuro();
	
	// Oppgave e)
	// Holder på med desimaltall, bruker derfor double all the way.
	// Integers kan gå fint vha implisitt casting, men blir raskt skummelt med runding ved deling.

	return 0;
}

void inputAndPrintInteger(){
	int number = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> number;
	std::cout << "Du skrev: " << number << std::endl;
}

int intputInteger(){
	int intgr = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> intgr;

	return intgr;
}

void inputIntegersAndPrintSum(int int1, int int2){
	int sum = int1 + int2;
	std::cout << "Summen av tallene: " << sum << std::endl;
}

bool isOdd(int number){
	if((number % 2) == 0)
		return false;
	else
		return true;
}

void printHumanReadableTime(int inputSecs){
	int hours = 0, minutes = 0;
	while (inputSecs >= 3600) {
		hours++;
		inputSecs -= 3600;
	}
	while (inputSecs >= 60) {
		minutes++;
		inputSecs -= 60;
	}
	std::cout << "H: " << hours << " M: " << minutes << " S: " << inputSecs << std::endl;
}

void inputIntegersUsingLoopAndPrintSum(){
	int sum = 0;
	int lastNumber = 0; 
	std::cout << "Skriv heltall som skal summeres. Skriv til slutt 0 for å avslutte." << std::endl;
	do{
		std::cin >> lastNumber;
		sum += lastNumber;
	}
	while(lastNumber != 0);
	std::cout << "Summen ble: " << sum << "." << std::endl;	
}

double inputDouble() {
	double sum = 0.0;
	double lastNumber = 0.0;
	std::cout << "Skriv desimaltall som skal summeres. Skriv til slutt 0 for å avslutte." << std::endl;
	do{
		std::cin >> lastNumber;
		// Håper at 0 blir implicitly casted til double.
		sum += lastNumber;
	}
	while(lastNumber != 0.0);
	return sum;
}

void nokToEuro(){
	double nokToConvert = 0;
	double converted = 0;
	std::cout << "Du skal nå legge sammen pengene i NOK som du vil gjøre om til Euro." << std::endl;
	do{
		nokToConvert = inputDouble();
		if(nokToConvert < 0)
			std::cout << "Prøv igjen. Summen må være positiv." << std::endl;
	}
	while(nokToConvert < 0);

	converted = nokToConvert / 9.64;

	// Desimaler
	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(2);

	std::cout << nokToConvert << " NOK blir: " << converted << " Euro." << std::endl;
}
