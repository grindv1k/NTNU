#include <iostream>
#include <cmath>
#include <cassert>

//#define NDEBUG
// For å IKKE la assert avslutte programmet, fjern kommentaren til NDEBUG.

void inputAndPrintInteger();
int intputInteger();
void inputIntegersAndPrintSum(int int1, int int2);
bool isOdd(int integ);
void printHumanReadableTime(int timeInSec);
void inputIntegersUsingLoopAndPrintSum();
double inputDouble();
void nokToEuro();
void timesTable();
double discriminant(double a, double b, double c);
void printRealRoots(double a, double b, double c);
void solveQuadraticEquation();
void calculateLoanPayments();

int main() {
	///// Oppgave 1
	 //Oppgave a)
	//inputAndPrintInteger();

	// Oppgave b)
	//int nmbr = intputInteger();
	//std::cout << "Du skrev: " << nmbr << std::endl;

	// Oppgave c)
	//int a = intputInteger();
	//int b = intputInteger();
	//inputIntegersAndPrintSum(a, b);

	// Oppgave d) 
	//Ke for nokke	

	// Oppgave e)
	//for (int i = 10; i < 15; i++) {
	//if(isOdd(i)) {
	//std::cout << i << " er et oddetall." << std::endl;
	//} else {
	//std::cout << i << " er et partall." << std::endl;
	//}
	//}

	// Oppgave f)
	//printHumanReadableTime(14401);

	//// Oppgave 2
	// Oppgave a)
	//inputIntegersUsingLoopAndPrintSum();

	// Oppgave b)
	// Dersom man vet hvor mange tall: for-løkke. Kan jo da bestemme antall iterasjoner ut i fra informasjonen.
	// Dersom ikke, bruke while, da man kan bruke f.eks null som "utgang" og la loopen ellers gå "evig".

	// Oppgave c)
	//double sum = inputDouble();
	//std::cout << "Summen ble: " << sum << "." << std::endl;

	// Oppgave d)
	//nokToEuro();

	// Oppgave e)
	// Holder på med desimaltall, bruker derfor double all the way.
	// Integers kan gå fint vha implisitt casting, men blir raskt skummelt med runding ved deling.

	//// Oppgave 3
	//Oppgave a)
	int valg = 0;
	do{
		std::cout << "Velg funksjon: " << std::endl;
		std::cout << "0) Avslutt" << std::endl;
		std::cout << "1) Summer to tall\n";
		std::cout << "2) Summer flere tall\n";
		std::cout << "3) Konverter NOK til Euro\n";
		std::cout << "4) Løs andregradsligning\n";
		std::cout << "5) Regn ut årlige innbetalinger av lån\n";
		std::cout << "Angi valg (0-5):\n";
		std::cin >> valg;
		switch(valg){
			case 0:
				break;
			case 1:
				inputIntegersAndPrintSum(intputInteger(), intputInteger());	
				break;
			case 2:
				inputIntegersUsingLoopAndPrintSum();
				break;
			case 3:
				nokToEuro();
				break;
			case 4:
				solveQuadraticEquation();
				break;
			case 5:
				calculateLoanPayments();
				break;
			default:
				break;
		}

		// Lag litt luft
		if(valg != 0)
			std::cout << std::endl << std::endl;

	}
	while(valg != 0);

	//timesTable();

	//// Oppgave 4
	// Oppgave a)
	//int discrimResult = discriminant(1, 2, 3);

	// Oppgave b)
	//printRealRoots(1, 2, 4);
	//printRealRoots(4, 4, 1);
	//printRealRoots(8, 4, -1);

	//// Oppgave 5
	// Implementert i menyen (over).
	
	return 0;
}

void inputAndPrintInteger(){
	int number = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> number;
	std::cout << "Du skrev: " << number << std::endl;
}

int intputInteger(){
	int intgr = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> intgr;

	return intgr;
}

void inputIntegersAndPrintSum(int int1, int int2){
	int sum = int1 + int2;
	std::cout << "Summen av tallene: " << sum << std::endl;
}

bool isOdd(int number){
	if((number % 2) == 0)
		return false;
	else
		return true;
}

void printHumanReadableTime(int inputSecs){
	int hours = 0, minutes = 0;
	while (inputSecs >= 3600) {
		hours++;
		inputSecs -= 3600;
	}
	while (inputSecs >= 60) {
		minutes++;
		inputSecs -= 60;
	}
	std::cout << "H: " << hours << " M: " << minutes << " S: " << inputSecs << std::endl;
}

void inputIntegersUsingLoopAndPrintSum(){
	int sum = 0;
	int lastNumber = 0; 
	std::cout << "Skriv heltall som skal summeres. Skriv til slutt 0 for å avslutte." << std::endl;
	do{
		std::cin >> lastNumber;
		sum += lastNumber;
	}
	while(lastNumber != 0);
	std::cout << "Summen ble: " << sum << "." << std::endl;	
}

double inputDouble() {
	double sum = 0.0;
	double lastNumber = 0.0;
	std::cout << "Skriv desimaltall som skal summeres. Skriv til slutt 0 for å avslutte." << std::endl;
	do{
		std::cin >> lastNumber;
		// Håper at 0 blir implicitly casted til double.
		sum += lastNumber;
	}
	while(lastNumber != 0.0);
	return sum;
}

void nokToEuro(){
	double nokToConvert = 0;
	double converted = 0;
	std::cout << "Du skal nå legge sammen pengene i NOK som du vil gjøre om til Euro." << std::endl;
	do{
		nokToConvert = inputDouble();
		if(nokToConvert < 0)
			std::cout << "Prøv igjen. Summen må være positiv." << std::endl;
	}
	while(nokToConvert < 0);

	converted = nokToConvert / 9.64;

	// Desimaler
	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(2);

	std::cout << nokToConvert << " NOK blir: " << converted << " Euro." << std::endl;
}

void timesTable(){
	std::cout << "Ønsket bredde? ";
	int width = intputInteger();
	std::cout << "Ønsket høyde? ";
	int height = intputInteger();

	for(int i = 1; i <= height; i++){

		for(int j = 1; j <= width; j++){
			std::cout.width(5);
			std::cout << std::right <<  i*j << std::cout.fill(' ');
		}

		std::cout << std::endl;
	}
}

double discriminant(double a, double b, double c){
	return (b*b - 4*a*c);
}

void printRealRoots(double a, double b, double c){
	assert (a != 0);
	double resultOfDisc = discriminant(a, b, c);

	// Pen output med kun 3 desimaler.
	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(3);

	std::cout << "Gitte konstanter er (a, b, c):\n";
	std::cout << a << "   " << b << "   " << c << std::endl;

	double solution1 = 0.0;
	double solution2 = 0.0;

	if(resultOfDisc == 0){
		std::cout << "1 løsning! Dobbel rot." << std::endl;
		solution1 = (-b / (2*a));
		std::cout << "Løsningen er: x = " << solution1 <<  std::endl;
	} else if (resultOfDisc < 0) {
		std::cout << "0 løsninger (reelle). Avslutter ... " << std::endl;
	} else {
		std::cout << "2 løsninger." << std::endl;
		solution1 = (-b+sqrt(resultOfDisc))/(2*a);
		solution2 = (-b-sqrt(resultOfDisc))/(2*a);
		std::cout << "Løsningene er: x1 = " << solution1 << ", x2 = " << solution2 << "." << std::endl;
	}
	std::cout << std::endl;
}

void solveQuadraticEquation(){
	std::cout << "Skriv inn tre tall, hhv. a, b, og c.\nDisse vil være konstanter i ligningen:\nax^2+bx+c=0\n";
	double a = 0.0, b = 0.0, c = 0.0;
	std::cin >> a;
	assert (a != 0);
	std::cin >> b;
	std::cin >> c;
	printRealRoots(a, b, c);
}

void calculateLoanPayments(){
	double lånebeløp = 0.0;
	double rente = 0.0;

	std::cout << "\nLånebeløp: ";
	std::cin >> lånebeløp;
	std::cout << "Rente (oppgi i prosentform): ";
	std::cin >> rente;

	double innbetaling = 0.0;
	double resterendeLån = lånebeløp;
	std::cout << "År\t\t";
	std::cout << "Innbetaling\t\t";
	std::cout << "Gjenstående lån\n";
	for(int i = 0; i < 10; i++){
		innbetaling = lånebeløp/10.0 + (rente/(double)100.0)*resterendeLån;
		resterendeLån -= innbetaling;
		std::cout << i+1 << "\t\t";
		std::cout << innbetaling << "\t\t\t";
		std::cout << resterendeLån << std::endl;
	}
}
