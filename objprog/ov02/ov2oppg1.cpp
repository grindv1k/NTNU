#include <iostream>

void inputAndPrintInteger();
int intputInteger();
void inputIntegersAndPrintSum(int int1, int int2);
bool isOdd(int integ);
void printHumanReadableTime(int timeInSec);

int main() {

	// Oppgave a)
	//inputAndPrintInteger();

	// Oppgave b)
	//int nmbr = intputInteger();
	//std::cout << "Du skrev: " << nmbr << std::endl;

	// Oppgave c)
	//int a = intputInteger();
	//int b = intputInteger();
	//inputIntegersAndPrintSum(a, b);

	// Oppgave d) 
	//Ke for nokke	

	// Oppgave e)
	//for (int i = 10; i < 15; i++) {
	//if(isOdd(i)) {
	//std::cout << i << " er et oddetall." << std::endl;
	//} else {
	//std::cout << i << " er et partall." << std::endl;
	//}
	//}

	// Oppgave f)
	printHumanReadableTime(14401);

	return 0;
}

void inputAndPrintInteger(){
	int number = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> number;
	std::cout << "Du skrev: " << number << std::endl;
}

int intputInteger(){
	int intgr = 0;
	std::cout << "Skriv inn et tall: ";
	std::cin >> intgr;

	return intgr;
}

void inputIntegersAndPrintSum(int int1, int int2){
	int sum = int1 + int2;
	std::cout << "Summen av tallene: " << sum << std::endl;
}

bool isOdd(int number){
	if((number % 2) == 0)
		return false;
	else
		return true;
}

void printHumanReadableTime(int inputSecs){
	int hours = 0, minutes = 0;
	while (inputSecs >= 3600) {
		hours++;
		inputSecs -= 3600;
	}
	while (inputSecs >= 60) {
		minutes++;
		inputSecs -= 60;
	}
	std::cout << "H: " << hours << " M: " << minutes << " S: " << inputSecs << std::endl;
}
