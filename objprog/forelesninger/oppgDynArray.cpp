#include <iostream>

using namespace std;

int* returnDynArrayPointerOfSize(int size);

int main()
{
    const int size = 20000;
    int* myArray = returnDynArrayPointerOfSize(size); 
    myArray[0] = 1234;
    myArray[size - 1] = 1111;
    for(int i = 0; i < size; i++)
    {
       cout << &myArray[i] << endl; 
    }

    return 0;
}

int* returnDynArrayPointerOfSize(int size)
{
    return new int[size];
    // Delete? 
}
