#include <iostream>
#include <cmath>
#include "cannonball.h"
#include "utilities.h"

#define PI 3.14159265

double acclY()
{
	// Antar positiv Y oppover
	return -9.81;
}

double velY(double initVelocityY, double time)
{
	return initVelocityY + acclY()*time;
}

double posY(double initVelocityY, double time)
{
	return initVelocityY*time + acclY()*time*time/2;
}

double posX(double initVelocityX, double time)
{
	// Anta akselerasjon i x-retning er null.
	return initVelocityX*time;
}

void printTime(double time)
{
	int hours = 0, minutes = 0;
	while (time >= 3600) {
		hours++;
		time -= 3600;
	}
	while (time >= 60) {
		minutes++;
		time -= 60;
	}
	std::cout << "H: " << hours << " M: " << minutes << " S: " << time << std::endl;
}

double flightTime(double initVelocityY)
{
	return -initVelocityY/acclY()*2;
}

void getUserInput(double *theta, double *absVelocity)
{
	std::cout << "Skriv vinkel(double) i grader: "; 
	std::cin >> *theta;
	std::cout << "Skriv lengden på hastighetsvektor(double): "; 
	std::cin >> *absVelocity; 	
}

double getVelocityX(double theta, double absVelocity)
{
	return absVelocity*cos(theta*PI/180);
}

double getVelocityY(double theta, double absVelocity)
{
	return absVelocity*sin(theta*PI/180);
}

void getVelocityVector(double theta, double absVelocity, double *velocityX, double *velocityY)
{
	*velocityX = getVelocityX(theta, absVelocity);
	*velocityY = getVelocityY(theta, absVelocity);
}

double getDistanceTraveled(double velocityX, double velocityY)
{
	double time = flightTime(velocityY);
	return time*velocityX;
}
double targetPractice(double distanceToTarget, double velocityX, double velocityY)
{
	// Negative if too far, positive if too short. Could be changed to abs?
	double actualDistance = getDistanceTraveled(velocityX, velocityY);	
	return distanceToTarget - actualDistance;
}


void playTargetPractice()
{
	double angle = 0.0;
	double velocity = 0.0;
	int targetDistance = randomWithLimits(100, 1000);
	for(int i = 9; i >= 0; i--)
	{
		getUserInput(&angle, &velocity);
		double userVeloX = getVelocityX(angle, velocity);
		double userVeloY = getVelocityY(angle, velocity);
		double actualDistance = getDistanceTraveled(userVeloX, userVeloY);
		double missedBy = fabs(targetDistance - actualDistance);
		if(missedBy <= 5.0)
		{
			std::cout << "Congrats! Ya won!" << std::endl;
			return;
		}
		std::cout << "Missed by: " << missedBy << " you need to be within 5m. Tries left: " << i << "\n\n";
	}
	std::cout << "You lost. Too bad!" << std::endl;
	return;
}
