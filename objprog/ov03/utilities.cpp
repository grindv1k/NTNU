#include <cstdlib>
#include <random>
#include <cmath>
#include "utilities.h"

int randomWithLimits(int lowerLimit, int upperLimit)
{
	int randomNumb = 0;
	do
	{
	randomNumb = std::rand();
	randomNumb = randomNumb % (upperLimit + 1);
	}
	while(!( (randomNumb >= lowerLimit) && (randomNumb <= upperLimit)) );

	return randomNumb;
}

int modernRandomWithLimits(int lowerLimit, int upperLimit)
{
	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_int_distribution<int> uniform(lowerLimit, upperLimit);

	return uniform(generator);
}
