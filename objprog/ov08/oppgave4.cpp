#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include "oppgave4.h"

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::ifstream;
using std::ofstream;
using std::stringstream;

void oppg4a()
{
    makeInputAllLowerCaseAndNoWeirdChards();
    int nrOfWords = countWords();
    map<string, int> wordMap;  
    wordMap = countOccurenceOfEachWord();

    // TESTING
    wordMap["testword"] = 10;
    //

    string longestWord = findLongestWord();
    int nrOfLines = findNumberOfLines();

    cout << "Text statistics:\n";
    cout << "Longest word: " << longestWord << endl;
    cout << "Number of words: " << nrOfWords << endl;
    cout << "Number of lines: " << nrOfLines << endl;
    printPls(wordMap);

    cout << "Oppgave 4 -- a) utført.\n";
}

void makeInputAllLowerCaseAndNoWeirdChards()
{
    ifstream is; 
    ofstream os;

    is.open("texty.txt");
    os.open("oppg4aOut.txt");

    if(is.is_open() && os.is_open())
    {
        char character;
        string line;

        while(getline(is, line))
        {
            for(long i = 0; i < line.length(); i++)
            {
                character = tolower(line[i]);
                if( (int) character <= 122 && (int) character >= 97)
                    os << character;
                else if( (int) character == 32) // 32 is Ascii for space(bar)-char
                    os << character;
            }
            os << endl;
        } 
    }
    else
        cout << "(makeInputLow) Klarte ikke åpne en av filene." << endl;

    is.close();
    os.close();
}

int countWords()
{
    int nrOfWrds = 0;

    ifstream is;

    is.open("oppg4aOut.txt");

    if(is.is_open())
    {
        string line;
        while(getline(is, line))
        {
            // Antall ord på line = spaces + 1
            // Antar da at ingen ord har mer enn 1 space between words
            nrOfWrds++;

            for(unsigned long i = 0; i < line.length(); i++)
            {
                if( (int) line[i] == 32) // 32 is Ascii for space(bar)-char
                    nrOfWrds++;
            }
        }
    }

    is.close();

    return nrOfWrds;
}

map<string, int> countOccurenceOfEachWord()
{
    map<string, int> wordOcMap;

    string word = "";

    ifstream is;
    is.open("oppg4aOut.txt");

    if(is.is_open())
    {
        string line;
        while(getline(is, line))
        {
            stringstream ss(line);    
            while(ss >> word)
            {
                if(wordOcMap.find(word) == wordOcMap.end())
                    wordOcMap[word] = 1;
                else
                {
                    wordOcMap[word] = wordOcMap[word] + 1;
                }
            }
        }
    }

    is.close();

    return wordOcMap;
}

string findLongestWord()
{
    string word = "";
    string wordFromStream;

    ifstream is;

    is.open("oppg4aOut.txt");

    if(is.is_open())
    {
        string line;
        while(getline(is, line))
        {
            stringstream ss(line);    
            while(ss >> wordFromStream)
            {
                if(wordFromStream.length() > word.length())
                    word = wordFromStream;
            }
        }
    }

    is.close();

    return word;
}

int findNumberOfLines()
{
    int nrOfLines = 0;

    ifstream is;

    is.open("oppg4aOut.txt");

    if(is.is_open())
    {
        string line;
        while(getline(is, line))
            nrOfLines++;
    }

    is.close();

    return nrOfLines;
}

void printPls(map<string, int> m) 
{
    map<string, int>::const_iterator iter;
    for (iter = m.begin(); iter != m.end(); iter++)
    {
        cout << iter->first << ": " << iter->second << endl;
    }
}
