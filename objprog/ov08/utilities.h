#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <string>

using namespace std;

string intToString(int number);
void printNumberOfBackslashes(int howManyPerLine, int lines, bool prependNewline, bool appendNewline);
void clearScreen();

#endif
