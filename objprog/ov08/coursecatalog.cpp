#include <map>
#include <iostream>
#include "coursecatalog.h"

using std::map;
using std::string;
using std::cout;
using std::endl;

void CourseCatalog::add(string key, string value)
{
    courses[key] = value;
}

string CourseCatalog::get(string key)
{
    return courses[key];
}

void CourseCatalog::printAll() 
{
    map<string, string>::const_iterator iter;
    cout << "KURS/EMNER:\n";
    for (iter = courses.begin(); iter != courses.end(); iter++)
    {
        cout << iter->first << " - " << iter->second << endl;
    }
}

void CourseCatalog::erase(string key)
{
    courses.erase(key);
}
