#ifndef OPPGAVE4_H_
#define OPPGAVE4_H_

#include <map>
#include <string>

using std::map;
using std::string;

void oppg4a();
void makeInputAllLowerCaseAndNoWeirdChards();
int countWords();
map<string, int> countOccurenceOfEachWord();
string findLongestWord();
int findNumberOfLines();
void printPls(map<string, int> m);

#endif
