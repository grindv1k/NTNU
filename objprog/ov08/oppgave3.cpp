#include <iostream>
#include "coursecatalog.h"
#include "oppgave3.h"

using namespace std;

void oppg3a()
{
    // Deklarer klassen ...
}

void oppg3b()
{
    // Implementer klassen ...
}

void oppg3c()
{
    // Test klassen!
    CourseCatalog NTNU;
    NTNU.add("TDT4110", "Informasjonsteknologi grunnkurs");
    NTNU.add("TDT4102", "Prosedyre- og objektorientert programmering");
    NTNU.add("TMA4100", "Matematikk 1");

    NTNU.printAll();

    cout << "Oppgave 3 -- c) nå utført.\n";
}

void oppg3d()
{
    CourseCatalog NTNU;
    NTNU.add("TDT4110", "Informasjonsteknologi grunnkurs");
    NTNU.add("TDT4102", "Prosedyre- og objektorientert programmering");
    NTNU.add("TMA4100", "Matematikk 1");

    cout << "Vil nå endre TDT4102 value til C++.\n";
    cout << "Forsøk 1:\n";
    NTNU.add("TDT4102", "C++");

    NTNU.printAll();

    cout << "Forsøk 1 funket!\n";
    cout << "Oppgave 3 -- d) nå utført.\n";
}

void oppg3e()
{
    cout << "VALGFRI ... \n";
    cout << "Oppgave 3 -- e) nå utført.\n";
}
