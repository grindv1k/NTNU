#ifndef COURSECATALOG_H_
#define COURSECATALOG_H_

#include <map>
#include <string>

using std::map;
using std::string;

class CourseCatalog
{
    private:
        map<string, string> courses;

    public:
        void add(string key, string value);
        string get(string key);
        void printAll();
        void erase(string key);

        // Just use default constructor
};

#endif
