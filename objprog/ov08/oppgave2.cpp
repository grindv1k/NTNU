#include <iostream>
#include <fstream>
#include "oppgave2.h"

using namespace std;

void oppg2a()
{
    // Tekstfil: .../ov08/texty.txt
    ifstream is; 

    is.open("texty.txt");

    if(is.is_open())
    {
        int cCounter[26];
        for(int i = 0; i < 26; i++)
            cCounter[i] = 0;

        char charFromStream;
        while(is >> charFromStream)
        {
            if ( (int) tolower(charFromStream) >= 97 && (int) tolower(charFromStream) <= 122)
            {
                cCounter[charFromStream - 'a']++;
            }
        }

        int totalChars = 0;
        for(int i = 0; i < 26; i++)
            totalChars = totalChars + cCounter[i];

        cout << "Total # of (counted) chars: " << totalChars << endl;

        for(int i = 0; i < 26; i++)
        {
            cout << (char) (i + (int) 'a') << ": " << cCounter[i] << " " << endl;
        }
    }
    else
        cout << "Klarte ikke åpne fil for lesing." << endl;

    is.close();

    cout << "Oppgave 2 -- a) utført.\n";
}
