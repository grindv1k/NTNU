#include <fstream>
#include <iostream>
#include <string>
#include "oppgave1.h"

using namespace std;

void oppg1a()
{
    ofstream outStream;

    //outStream.open("~/NTNU/objprog/ov08/oppg1aOut.txt");
    // FUNKER IKKE!! Bruk open som under.
    outStream.open("oppg1aOut.txt");

    string fromInput;
    string exitWord = "quit";


    if(outStream.is_open())
    {
        cout << "Skriv ord, de blir lagt i ov08/oppg1aOut.txt:\n";
        do
        {
            cin >> fromInput;
            if(fromInput.compare(exitWord))
                outStream << fromInput << endl;

        } while(fromInput.compare(exitWord));
    }
    else
        cout << "Klarte ikke åpne fil for skriving." << endl;

    outStream.close();

    cout << "Oppgave 1 -- 1) utført.\n";
}


void oppg1b()
{
    ifstream inStream;
    ofstream os;

    inStream.open("texty.txt");
    os.open("oppg1bOut.txt");

    if(inStream.is_open() && os.is_open())
    {
        int lineNumber = 1;
        string readString;

        while(inStream >> readString)
        {
        getline(inStream, readString);

        os << lineNumber << ": " << readString << endl;

        lineNumber++;
        }
    }
    else if(!inStream.is_open() && os.is_open())
        cout << "Klarte ikke åpne fil for lesing." << endl;
    else if(inStream.is_open() && !os.is_open())
        cout << "Klarte ikke åpne fil for skriving." << endl;
    else
        cout << "Klarte ikke åpne noen av filene." << endl;

    inStream.close();
    os.close();

    cout << "Oppgave 1 -- b) utført.\n";
}
