#pragma once
#include "Matrix.h"

class Vector: public Matrix {
    unsigned int rows, columns;
    double * data;
    public:
        Vector();
        explicit Vector(unsigned int nRows);
        Vector(const Matrix & other);
};
