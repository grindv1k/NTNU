#include <cassert> // assert()
#include <cstdlib> // rand()
#include <iostream> // cout (testing...)
#include "Minesweeper.h"

Minesweeper::Minesweeper(int width, int height, int mines) {
    // Set things
    rows = height;
    columns = width;
    gameOver = false;

    // Allocate memory and set tiles to closed and no mine
    tiles = new Tile*[height];
    for(int i = 0; i < height; i++)
    {
        tiles[i] = new Tile[width];
        for(int j = 0; j < width; j++)
        {
            setTile(false, false, i, j);
        }
    } 

    // Do some random "algorithm" to set mines
    // Will never end if # mines is higher than height*width
    assert(height*width > mines);

    int i = 0, j = 0;

    while(mines > 0)
    {
        // Get random tile
        i = rand() % height;    
        j = rand() % width;

        if(!isTileOpen(i, j) && !isTileMine(i, j))
        {
            setTile(false, true, i, j); 
            mines--;
        }
    }
}

Minesweeper::~Minesweeper() {
    for(int i = 0; i < rows; i++)
    {
        delete[] tiles[i];
    }
    delete[] tiles;

    tiles = nullptr;
}

bool Minesweeper::isGameOver() const {
    if(gameOver == true)
        return true;
    return false;
}

void Minesweeper::setGameOver(bool b)
{
    gameOver = b;
}

bool Minesweeper::isTileOpen(int row, int col) const {
    if(tiles[row][col].open == true)
        return true;
    return false;
}

bool Minesweeper::isTileMine(int row, int col) const {
    if(tiles[row][col].mine == true)
        return true;
    return false;
}

void Minesweeper::openTile(int row, int col) {
    if(isTileOpen(row, col))
        return; 
    else if(isTileMine(row, col))
        setGameOver(true);
    else
    {
        setOpen(true, row, col);
        if((numAdjacentMines(row, col) == 0) && (row != 0) && (col != 0) && (row != (getHeight() - 1)) && (col != (getWidth() - 1)))
        {
            int pls = 3;
            while(pls > 0)
            {
              int i = -1 + (rand() % 3);
              int j = -1 + (rand() % 3);
              if(!isTileOpen(row + i, col + j))
                openTile(row + i, col + j);
              pls--;
            }
            //openTile(row, col + 1);
            //openTile(row - 1, col + 1);
            //openTile(row - 1, col);
            //openTile(row - 1, col - 1);
            //openTile(row, col - 1);
            //openTile(row + 1, col - 1);
            //openTile(row + 1, col);
            //openTile(row + 1, col + 1);
        }
    }
}

void Minesweeper::setMine(bool mine, int row, int col)
{
    tiles[row][col].mine = mine; 
} 
void Minesweeper::setOpen(bool open, int row, int col)
{
    tiles[row][col].open = open; 
}
void Minesweeper::setTile(bool open, bool mine, int row, int col)
{
    setMine(mine, row, col);
    setOpen(open, row, col);
}

int Minesweeper::getHeight() const
{
    return rows;
}
int Minesweeper::getWidth() const
{
    return columns;
}

int Minesweeper::numAdjacentMines(int row, int col) const {
    int num = 0;

    if(row == 0)
    {
        // Du befinner deg øverst

        if(col == 0)
        {
            // Du befinner deg øverst til venstre
            num = isTileMine(row, col + 1) + isTileMine(row + 1, col + 1) + isTileMine(row + 1, col);
        }
        else if(col == (getWidth() - 1))
        {
            // Du befinner deg øverst til høyre
            num = isTileMine(row, col - 1) + isTileMine(row + 1, col - 1) + isTileMine(row + 1, col);
        }
        else
        {
            // generelt øverst
            num = isTileMine(row, col + 1) + isTileMine(row, col - 1) + isTileMine(row + 1, col - 1) + isTileMine(row + 1, col) + isTileMine(row + 1, col + 1);
        }
    } else if(row == (getHeight() - 1))
    {
        // Du befinner deg nederst

        if(col == 0)
        {
            // Du befinner deg nederst til venstre
            num = isTileMine(row, col + 1) + isTileMine(row - 1, col + 1) + isTileMine(row + 1, col);
        }
        else if(col == (getWidth() - 1))
        {
            // Du befinner deg nederst til høyre
            num = isTileMine(row - 1, col) + isTileMine(row - 1, col - 1) + isTileMine(row, col - 1);
        }
        else
        {
            // Generelt nede 
            num = isTileMine(row, col + 1) + isTileMine(row - 1, col + 1) + isTileMine(row - 1, col) + isTileMine(row - 1, col - 1) + isTileMine(row, col - 1);
        }
    } else if(col == 0)
    {
        // Du befinner deg til venstre 

        if(row == 0)
        {
            // Du befinner deg oppe til venstre
            // Denne skal være dekt
            assert(1 < 0);
        }
        else if(row == (getWidth() - 1))
        {
            // Nede til venstre
            // Skal være dekt
            assert(1 < 0);
        }
        else
        {
            // Genrelt venstre
            num = isTileMine(row, col + 1) + isTileMine(row - 1, col + 1) + isTileMine(row - 1, col) + isTileMine(row + 1, col) + isTileMine(row + 1, col + 1);
        }
    } else if(col == (getHeight() - 1))
    {
        // Du befinner deg til høyre

        if(row == 0)
        {
            // Høyre oppe hjørnet
            // Dekt
            assert(1 < 0);
        }
        else if(row == (getWidth() - 1))
        {
            // Høyre nedre hjørne
            // Dekte
            assert(1 < 0);
        }
        else
        {
            // Generelt høyre
            num = isTileMine(row - 1, col) + isTileMine(row - 1, col - 1) + isTileMine(row, col - 1) + isTileMine(row + 1, col - 1) + isTileMine(row + 1, col);
        }
    } else
    {
        // Somewhere in ze middddd
        num = isTileMine(row, col + 1) + isTileMine(row - 1, col + 1) + isTileMine(row - 1, col) + isTileMine(row - 1, col - 1) + isTileMine(row, col - 1) +
            isTileMine(row + 1, col - 1) + isTileMine(row + 1, col) + isTileMine(row + 1, col + 1);
    }

    return num;
}
int Minesweeper::numAdjacentOpen(int row, int col) const
{
    int num = 0;

    if(row == 0)
    {
        // Du befinner deg øverst

        if(col == 0)
        {
            // Du befinner deg øverst til venstre
            num = isTileOpen(row, col + 1) + isTileOpen(row + 1, col + 1) + isTileOpen(row + 1, col);
        }
        else if(col == (getWidth() - 1))
        {
            // Du befinner deg øverst til høyre
            num = isTileOpen(row, col - 1) + isTileOpen(row + 1, col - 1) + isTileOpen(row + 1, col);
        }
        else
        {
            // generelt øverst
            num = isTileOpen(row, col + 1) + isTileOpen(row, col - 1) + isTileOpen(row + 1, col - 1) + isTileOpen(row + 1, col) + isTileOpen(row + 1, col + 1);
        }
    } else if(row == (getHeight() - 1))
    {
        // Du befinner deg nederst

        if(col == 0)
        {
            // Du befinner deg nederst til venstre
            num = isTileOpen(row, col + 1) + isTileOpen(row - 1, col + 1) + isTileOpen(row + 1, col);
        }
        else if(col == (getWidth() - 1))
        {
            // Du befinner deg nederst til høyre
            num = isTileOpen(row - 1, col) + isTileOpen(row - 1, col - 1) + isTileOpen(row, col - 1);
        }
        else
        {
            // Generelt nede 
            num = isTileOpen(row, col + 1) + isTileOpen(row - 1, col + 1) + isTileOpen(row - 1, col) + isTileOpen(row - 1, col - 1) + isTileOpen(row, col - 1);
        }
    } else if(col == 0)
    {
        // Du befinner deg til venstre 

        if(row == 0)
        {
            // Du befinner deg oppe til venstre
            // Denne skal være dekt
            assert(1 < 0);
        }
        else if(row == (getWidth() - 1))
        {
            // Nede til venstre
            // Skal være dekt
            assert(1 < 0);
        }
        else
        {
            // Genrelt venstre
            num = isTileOpen(row, col + 1) + isTileOpen(row - 1, col + 1) + isTileOpen(row - 1, col) + isTileOpen(row + 1, col) + isTileOpen(row + 1, col + 1);
        }
    } else if(col == (getHeight() - 1))
    {
        // Du befinner deg til høyre

        if(row == 0)
        {
            // Høyre oppe hjørnet
            // Dekt
            assert(1 < 0);
        }
        else if(row == (getWidth() - 1))
        {
            // Høyre nedre hjørne
            // Dekte
            assert(1 < 0);
        }
        else
        {
            // Generelt høyre
            num = isTileOpen(row - 1, col) + isTileOpen(row - 1, col - 1) + isTileOpen(row, col - 1) + isTileOpen(row + 1, col - 1) + isTileOpen(row + 1, col);
        }
    } else
    {
        // Somewhere in ze middddd
        num = isTileOpen(row, col + 1) + isTileOpen(row - 1, col + 1) + isTileOpen(row - 1, col) + isTileOpen(row - 1, col - 1) + isTileOpen(row, col - 1) +
            isTileOpen(row + 1, col - 1) + isTileOpen(row + 1, col) + isTileOpen(row + 1, col + 1);
    }

    return num;
}
