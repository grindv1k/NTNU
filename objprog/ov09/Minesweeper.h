#pragma once

struct Tile {
    bool open;
    bool mine;
};

class Minesweeper {
    private:
        // Legg til de medlemsvariablene og hjelpefunksjonene du trenger
        Tile** tiles;
        int rows;
        int columns;
        bool gameOver;

    public:
        Minesweeper(int width, int height, int mines);
        ~Minesweeper();

        bool isGameOver() const;
        void setGameOver(bool b);

        bool isTileOpen(int row, int col) const;
        bool isTileMine(int row, int col) const;

        void openTile(int row, int col);

        void setTile(bool open, bool mine, int row, int col);
        void setMine(bool mine, int row, int col);
        void setOpen(bool open, int row, int col);

        int getHeight() const;
        int getWidth() const;

        int numAdjacentMines(int row, int col) const;
        int numAdjacentOpen(int row, int col) const;

        // Vi slår av autogenerert kopikonstruktør og tilordningsoperator for å unngå feil
        Minesweeper(const Minesweeper &) = delete;
        Minesweeper &operator=(const Minesweeper &) = delete;
};

