# 1: Vektoranalyse
## 1.1: Koordinatsystemer
- Sylindrisk: r radielt utover, phi i retn der vinkelen øker raskest, z som i vanlig xyz.
- Sfærisk: r avstand origo, phi lengdegrad (vinkel ut i fra x == greenwich), theta vinkel ned fra nordpolen.
## 1.2: Skalare funksjoner og vektorfelt
- Skalare funksjoner tar R^n og mapper til R.
- Vektorfelt legger en vektor i hvert punkt i R^2 (i eksemplene her).
Vektorfelt kan være konst, divergere, eller ha curl.
## 1.3: Integraler og notasjon.
- Linjeintegral av vektorfelt: La A (vektorfelt) representere en kraft på et legeme. Flytt legemet dl. Da er A dl arbeidet. Derfor gir integralet over kurven C av A dl være arbeidet over strekningen.
- Flateintegral: Gir strøm/fluks av vektorfeltet gjennom flaten. Dersom lukket peker flatenormalen utover.
- Volumintegral: Integrer en funksjon/variabel over hvert vol-element i volumet. F.eks vil integralet av massetetthet over et volum gi tot masse.
## 1.4: Gradient
Gradienten til h(x,y) er definert som vektoren med partiellderiverte av hver input.
En vektor.

Skrives nabla f.
## 1.5: Divergens
Et mål på utstrømning av et vektorfelt ut fra et punkt.
Definisjon: Fluks ut av en en inf small boks (flateintegral) delt på volument når volumet går mot null.

Skrives nabla prikk f. Er derfor en skalar. Hvis A er vektorfeltet, får vi komponentene Ax, Ay, og Az, som skal partielldiffes med sine tilhørende inputs og summeres.
## 1.6: Divergensteoremet
Enkelt. Fluksen ut av et lukket areal er lik summen av alle divergenser i volumet som flaten omslutter.

Så du kan uttrykke et flateintegral som volumintegral og omvendt.

Alle bidrag mellom små elementer inne i den lukkede flaten vil kansellere hverandre, untatt på randen.
## 1.7: Curl
Et vektorfelt [Ax Ay Az]' roterer hvor mye rundt et punkt (og retning)? Curl.

Man finner det ved å se på et bidrag over et kvadrat med areal delta S som lukker kurven C. Man finner altså curl om èn akse, så får man en vektor.

Def: Curl A = nabla x A.

Merk: Curl til gradient er null, og gradient til curl er null.
## 1.8: Stokes’ teorem
Sirkulasjonen av A rundt en lukket kurve C er sum av alle små sirkulasjoner i arealet kurven omslutter.

Dette linker integralet av et vektorfelt langs en lukket kurve i feltet opp mot all curl man finner i flatelementene i arealet.

Et konservativt vektorfelt tilfredsstiller at curlen er null. Da er integralet av vektorfeltet i en lukket kurve også lik null. Dette betyr at integralet fra a til b i feltet er uavhengig av vei!
## 1.9: Laplace-operatoren ∇^2
Er definert i alle koord syst og for både skalare funksjoner og vektorfelt.

Merk at disse burde alltid sjekkes før bruk.
## 1.10: Helmholtz’ teorem
Et resultat av teoremet: Et vektorfelt kan skrives som en gradient pluss en curl.

## 1.11: Vektorformler
For kartesisk, sylindrisk, og sfærisk koordinatsystem finner man her:
- Gradient
- Divergens
- Curl
- Nabla for skalar og vektorfelt

Generelt finner man for integraler:
- Kobling mellom volum- og flateintegral for skalare funksjoner
- Divergensteoremet: Volum+divergens --> fluks gjennom lukket flate
- Kobling mellom volum+curl og kryssprod flateelementer + vektorfelt
- Stokes' teorem: Kobling mellom curl gjennom en flate og fluks gjennom lukket kurve

Ellers generlle vektoridentiteter.

# 2: Elektrostatikk
## 2.1: Coulombs lov
Test-punktladning q. Hvilken kraft virker på den gitt en annen punktladning Q (vakuum)?
Denne loven gir svaret.

Dette definerer permittiviteten i vakuum, epsilon0.

Legg sammen vektorielt (totalkraft).

Funfact: Elektrisk kraft ca 10^39 ganger sterkere enn gravitasjonskraften.

Ved å innføre integralet (volum) og å bruke ladningstetthet (ladn / vol) så kan man integrere for å finne bidraget fra all ladning i rommet på testladningen.

Dette resultatet viser at det alltid er proporsjonalt med egen ladning. Dersom man fjerner denne påvirkningen får man definisjonen på elektrisk felt: E = F/q (sett fra observasjonspunktet).

Da igjen blir kraften F = qE.
### Eksempel 2.1 Tre punktladninger
Du legger sammen Coulombs lov om krefter osv vektorielt.
### Eksempel 2.2 Elektrisk vs gravitasjon
Elektrisk ~10^39 ganger større.
### Eksempel 2.3 Sirkulær linjeladning
Coulombs lov omgjort til E-felt (E=F/q) kan uttrykkes både for volum, flater, og linjer.
Hvis volum, så rho dv. Hvis flate så rho{s} dS. Hvis linje, så Q' dl.

Integralet er E = integral{type} Enhetsvektor {tetthetstype} {d type} / 4 pi eps0 R^2.

Så ved å bruke linjetypen kan du finne E-felt langs z-akse indusert av en ring med ladningstetthet Q'. Enhetsvektor R{hatt} = R{vec}/R.

Merk at feltet går mot feltet til punktladning for z --> stor!
## 2.2: Statiske elektriske felt er konservative -- skalarpotensialet
Lærer nå at et elektrisk felt er i volt/meter.

Å integrere feltet fra A til en referanse gir da skalarpotensialet i A (ift. ref).

Har sammenheng mellom elektrisk felt og potensial:

E = -grad V. Så hvordan finne E-felt? Finn potensial, og så bruk sammenhengen over.

Et E-felt står normalt på ekvipotensialflater, samme som at en bekk renner normalt ned fra koter.
Peker da veien som minker raskest (der skalarpot. minker mest).

En dipol er en neg pktladn -Q og Q i avstand d fra hverandre.
### Eksempel 2.4 Potensial sirkulær linjeladning
Hva er potensialet til den sirkulære linjeladn. forrige eksempel?

Bruk formelen for potensial i formelsamling. Alt blir konstant, enkelt integral.

Kan da finne E-feltet igjen via denne.
### Eksempel 2.5 Dipol
En dipol har to bidrag til et observasjonspunkt, èn med Q og èn med -Q, og to distanser. Ved litt triksing kan man skrive dette om, og så finne E-feltet.

Vi finner da at E-feltet kun avhenger av dipolmomentet p, som er vektoren d fra -Q til Q skalert med Q.
### Eksempel 2.6 Potensialet i tre punkter
Hvis du vet potensialet i to punkter, og så vet du senere kun at den ene har økt +3V, så må den andre også ha gjort det.

Potensialforskjeller avhenger ikke av referansen. Det er det samme som at høyde-forskjellen mellom to hyller endres ikke selv om du måler fra gulvet hos begge eller fra taket hos begge.
## 2.3: Gauss’ lov
Får nå sammenhengen mellom all ladning omsluttet av en flate og fluks av elektrisk felt ut av samme flate.

### Hvordan brukes dette i f.eks koaksialkabel?

Symmteri:
- E-feltet kan ikke avh. av phi, rotasjonssymmteri
- E-feltet kan ikke avh. av z, uendelig lang
Lag en sylinder rundt innerleder med radius r, større enn innerleder radius a og ytterlederradius b.

Vet altså at E peker i radiell retning, og må være en funksjon av r.

Sentrert sylinder rundt innerleder gir konstant E-felt og samme retning som dS elementer.

Bruk Gauss' lov på denne. Hvor mye ladning omsluttes? Ladning per lengde ganger lengden av sylinder.

Får da et E-felt inne mellom lederene, og null utenfor.

Ved å anta V0 skalarpot mellom lederene kan man uttrykk E(r) med V0, r, b, og a.

### Uendelig plan

Hvis planet har positiv ladningstetthet peker E-feltet bort fra planet, negativ ladningstetthet peker E-feltet mot planet.

E-feltet må være vertikalt, så kun bunn- og topplokk vil gi et bidrag (fra en sylinder som penetrerer planet).
### Eksempel 2.7 Koakskabel- E-felt.
- Innerleder V = V0.
- Ytterleder V = 0.

Symmetri: E-feltet radielt rettet. Hvorfor? Hvis man snur kabelen slik at z --> -z måtte E i z-retning blitt annerledes, men endrer ingenting. Samme med phi.

Hva med magnitude, endres den ved phi eller z? Nei samme greia. Uendelig lang, kan ikke variere med z. Vrioperasjon = gjør ingenting.

Videre:

Integrer en sylinderflate mellom lederene. Da får du størrelsen på E-feltet.
Fordi du har argumentert med retning får du da E(r) i r-retning.

Potensial: Husk at V0 = integral fra {a} til {ref = b}.

Kan da sette inn i E-felt for å ikke avhenge av ladningstetthet osv.
### Eksempel 2.8 Elektrisk felt kule
Symmetri tilsier at feltet må være radielt rettet, og størrelsen avhenger av r.

Videre bruk Gauss, og husk at overflate kule er 4 pi r^2.
### Eksempel 2.9 Uendelig plan
Grei. Flateladningstetthet rho{s}. Integerer slik at Gauss høyre side er rho{s} ganger delta S, hvor delta S er arealet til en sylinder du integrerer over.

E-feltet må gå i z-retning pga uendelig plan, og kun avhenge av z.

Endrer retning over og under planet.

## 2.4: Dielektriske medier
Er et medie som får negative/positive ender og kan bli tiltrukket/frastøtt ladninger.

Deler opp i:
- Bundne ladninger = ladninger i mediet som er dipoler
- Frie ladninger = alt annet

Mål: Få Gauss' lov med frie ladninger på høyresiden.

Interesting: Vannmolekyl er dipol pga. oksygenatomet drar elektroner mer mot seg (og O siden er da mer negativ) enn hydrogenatomet gjør.

Polarisering: Et medie hvor alle ladn er bundne er rent dielektrisk. Et materiale utsettes for et elektrisk felt, dipolene vil rettes inn etter feltet. Da vil overflaten kunne bli positiv på en side, og negativ på andre. Inne i midten vil ting være netto null.

Feltet D er e0E + P, hvor P er polariseringen (totalt dipolmoment).
Integralet av D dS over lukket S er lik Q fri i S.

- Lineært medium: P og E sammenheng lineær, P = e0 xe E hvor xe er elektrisk susceptibilitet.
- Isotropt medium: Sammenhengen er uavhengig av retning (motsatt av anisotrop).
- xe og er (er = epsilon{r} = 1 + xe = relativ permittivitet) uavh av pos: homogent

e{r} er 1 for vakuum, og høyere for alt annet. F.eks 5 for kvarts.

Bundne ladninger er nå bakt inn i e{r}.

Div D = rho, en av Maxwells.

Curl E = 0, en av Maxwells.

Ser at E = D / eps. Da vil man få at E-feltet er redusert med 1/eps{r} i forhold til vakuum. Dette skjer fordi dipoler rettes inn med negativ rumpe mot en punktladning Q, og skaper da et counter-field mot Q som ellers ville skapt et sterkere E-felt.

Til høyere eps{r} til mer reduksjon.
### Eksempel 2.10 Punktladning med dielektrisk medium rundt
Gauss (men med D ikke E) gir D 4 pi r^2 = Q.

Kan da finne E = D / eps = D / eps0 epsr. Radielt rettet of course.

Hvorfor er E-feltet 1/epsr mindre enn i vakuum? Fordi dipolene lager et mot-felt.

Hva om Q var inni en kule med eps?

Samme resultat som over på innsiden av kula, mens utenfor ville det blitt som vanlig Gauss (E-felt).
### Eksempel 2.12 Koakskabel med dielektrikum mellom lederene
Bare bruk Gauss med D som forventet, og så finner du E ved D/eps.

Så om du vet V0 kan du eliminerere Q'.

Litt vanskeligere:

Hva om et luftgap mellom innerleder og dielektrikum? Altså
- e(r) = e0,    a < r < a + d
- e(r) = ere0,  a + d < r < b.

Du bruker Gauss akkurat som før, ingenting har endret seg ladningsmessig.

For å finne V0 integerer du også som vanlig fra a til b E(r) dr, men husk å dele opp integralet slik at e(r) funker.

Såå egentlig ikke mye nytt! Resultatet viser at luftgapet er {er} ganger større. Kan da raskt få gjennomslag!
## 2.5: Grensebetingelser for E og D
Grense mellom to medier. Finner man da enkelt feltene på andre siden?

Har at E1t = E2t, dvs. tangentialkomponentene er like (de som går langs flaten).

Har at D1n - D2n = ps. Derfor hvis begge materialer er dielektrisk, er D1n = D2n.
## 2.6: Poissons og Laplace’ ligning
Du har Gauss' lov på diff-form, divergensen til D er lik rho.
I kombinasjon med at E-feltet er lik negativ gradient av skalarpotensialet får vi at 
nabla kvadrert skalarpotensial = negativ rho delt på absolutt permittivitet.

Det er Poissons ligning.

Dersom du har et volum v med abs. perm. eps, og rho = 0 så vil ikke skalarpotensialet V ha lokale maksima eller minima i v, alle er på randen S. Hvorfor?

Anta et lokalt maksima. Da vil grad V være rettet mot punktet. Da vil E-feltet være rettet ut av punktet. Det gjelder også D = eps E. Da må det være fri ladning i området (Gauss' lov). That can't be right siden rho = 0!
### Eksempel 2.13 Ledende kule, potensial
Kan bruke Laplace' ligning i sfæriske koordinater. Ladningstetthet er null, så høyre side null. Så trenger du å finne to konstanter etter to integrasjoner. Du vet V(a) (radius = a, der er V = V0), og V(infty) = 0.

It all works out!
### Eksempel 2.14 pn-overgang
- n: Har elektroner som kan bevege seg fritt
- p: Har "hull" som elektroner kan sette seg i

Blir derfor en positiv ladningstetthet i n-sonen, og motsatt for p.

Får derfor elektrisk felt!

Vet at div D = rho, og rho avhenger av x.

Kan da uttrykke E(x) (som et integral). Kan da uttrykke V(x) med dette igjen.
## 2.7: Ideelle ledere
- E-feltet er null i en ideell leder. Hvis ikke ville E-feltet flyttet på ladninger til de falt i ro. Elektrostatisk induksjon.
- rho = 0 i en idell leder. All overskuddsladn på overflate. Tettheten av overskuddsladn kalles indusert flateladningstetthet. Fordi det svarer ofte til et påtrykt felt.
- Lederen er ekvipotensial. Bare prøv å integrer E-felt mellom to punkter for å finne skalarpotensialet... blir jo null.
- Et = 0 rett utenfor, følger av grensebetingelse og at E = 0 i lederen.
- Dn = rhos. Dn er el flukstetthet utenfor. Ingen flukstetthet på innsiden. Rhos er overflateladningstethet.

Random thang: Fordi rhos = eps E(z = 0+) i ledende-plan-speilladningsmetoden så ble det innsett at man lager et E-felt ved å samle ladning et sted. Hvis du har 10 ladning, vil det f.eks gi eps = 1 og E = 10 eller eps = 2 og E = 5. Derfor vil mindre abs perm gi høyere E-felt.

Fant også ut: Potensialet ved overflaten er 0, og gradienten peker oppover. Derfor peker E-feltet nedover mot flaten.
### Eksempel 2.16
Netto ladning på figur pos el neg? Går flere negative flukslinjer enn positive inn i lederen, så det må være en netto negativ leder.
### Eksempel 2.17 Ledende kule
Har E feltet utregnet. Via Gauss kan vi finne et uttrykk for Q = ... 
Flateladningstetthet rho{s} = tot ladn / geometri.
Får da en sammenheng mellom V0 og Q. Får da E-feltet som forventet

Får også at |E| = V0/a, så for et gitt potensial blir E-feltet rett utenfor stort hvis radius er liten.
### Eksempel 2.19 Faradaybur
Leder med hull.

Lederen har konstant potensial (ideell). Da må E = -nabla V = 0 inni.

Så innsiden er elektrisk isolert fra det som skjer på utsiden!!
### Eksempel 2.20 Speilladningsmetoden
I stedet for å finne indusert flateladningstetthet fra Q i høyde z, regn ut E-feltet via speilladning.

Da er -Q plassert i -h.

Har fortsatt V = 0 i z = 0, som vi ville ha. For z > 0 er er ladningsfordelingen det samme. Sååå ok2k?

Selve ladningstettheten finnes via rho{s} = Dn = eps E.
## 2.8: Kapasitans
Igjen se figur 2.31. E-feltet peker ut av der ladning er samlet. Her er også et positivt skalarpotensial, i forhold til lederen under med V = 0 (hvor E-feltet strømmer til, negativt ladd nedre leder).
Da ser man igjen at man integrerer opp skalarpotensialet langs E-feltstrømningen.

C = Q/V.

Får kretsligning: I = dQ/dt = d(CV)/dt = C dV/dt

Generelt: Hvorfor vil høyere eps gi lavere E-felt? Fordi dipolene stiller seg inn, gir bunden flateladning, som stopper litt av E-felt. E-feltet søker jo "minuser", og polariseringen gir en "minus-kant" øverst der E-feltet stammer fra og en "pluss-kant" nederst der E-feltet er på vei, og dermed blir det som en hindring.

En annen måte å tenke på det på er at "pluss-kanten" setter opp et motfelt som har lyst å gå andre retningen enn "hovedfeltet".
### Eksempel 2.21 Kapasitansen til en parallellplatekondensator
Du har gitt areal S, +Q på øverste plate og V, -Q på nederste plate og V = 0, og eps i mellom.

What do? Finn uttrykk for potensialet gitt høyden via Poissons ligning. Da finner du også E-feltet.

Må relatere E-feltet til Q. Bruk Dn = rho{s}.

Så integrerer du rho{s} (som blir å integrere eps E, og E = V/d. Dette gir Q.

Goal!
### Eksempel 2.22
Feltet E = V/d i parallellplatekondensator uavh. av permittiviteten eps.

Men kun dersom vi er tilkoplet en spenningskilde!

Hvis vi lader opp til Q0 og tar vekk spgskilde og deretter setter eps i mellom, så vil ikke ladningen endre seg (stuck). Kapasitansen er jo gitt ved eps S / d, så den har blitt eps{r} større.

Potensialdiff må også endres, V = Q/C, og C ble endret.

Da E = V/d i denne settingen, må også E ha gått ned.

Hvorfor? Fordi dipolene setter opp et mot-E-felt.

Hva om spgskilden er koplet til? Da vil V alltid være konstant. Da tvinger kilden feltet til å være konstant ved å ta bort/legge til ladning for at det skal være sånn.
### Eksempel 2.23 Parallellkobling av kondensatorer
Lett! Vi har C = Q/V, og Q = Q1+Q2+Q3... så vi kan skrive C = Q1/V + Q2/V + ...
dermed at C = C1 + C2 + C3 ...
### Eksempel 2.24 Seriekopling av kondensatorer
Må anta at det ikke går feltlinjer fra èn til annen.

Får at netto ladning over hver er null, når man har +Q på ene siden og -Q på andre.

Da blir spg over hver cap lik Vc1 = Q/C1, Vc2 = Q/C2.... Så C = Q/V = Q / (Vc1 + Vc2 + ...).
### Eksempel 2.25 Kap. per lengde koaks
C' [F/m] = ?

Blir jo bare C' = Q'/V0.

Kan da regne ut V0 som før, og bruke eps i mellom lederene. Så bare sett inn. Ser at eps0 også har enhet [F/m].
## 2.9: Energi i elektriske felt
Energi i kondensator: 1/2 C V^2

Energitetthet: 1/2 D prikk E
### Eksempel 2.26 Energi per lengde koaks
Ved å bruke 1/2 D prikk E = 1/2 eps E^2 som er energi per volum, kan man ta integralet mellom lederene og langs hele kabelen = We. We' = We/l.

Eventuelt brukt formelen We' = 1/2 C'V0^2.
## 2.10: Strømtetthet og resistans
J = strømtetthet = N q v, N er antall ladningsbærere per volumenhet, q er ladningen per bærer, v er snitthastighet.

I = integral J prikk dS over S.

Ohms lov:
J = sigma E,    sigma = konduktivitet (hvor godt leder mediet strøm). Så en ideell isolator har sigma = 0, og en ideell leder har sigma = infty.

Effekttap per volumenhet er gitt av pj = J prikk E.

Ladning er bevart. Derfor er -dQ/dt = I,    hvis I er en positiv strøm ut av et område S.
Dermed får vi:

lukket integral over S av J prikk dS = -d/dt integral over v av rho dv.

Vi integerer altså strømtettheten over flaten S for å få hele strømmen, og sier at den er lik "tap" av ladning i volumet v.
Dette er Kirchhoffs strømlov.

Kan også skrives på differensialform vha. divergensteoremet, og ved å "fjerne" integralet.
Får da

nabla prikk J = - partielldiff mhp t av rho.

Så hvis strømmen er konstant blir høyresiden null.
### Eksempel 2.29 Driftshastigheten til ekeltroner
Du vet strøm og areal av tverrsnitt i kobber.

I / S er C/s 1/m^2. Trenger å dele vekk C, gjøres ved å dele på e = absoluttverdi til elektronladning, og dele på N [m^-3] konsentrasjons av ladningsbærer i kobber.

Får ca. en halv mm per sec. Signalhastigheten er dog rask.
### Eksempel 2.30 Resistans til motstandstråd
Vet R = V/I. J = I/S.

Kan da ta V = integral over lengden E dz, sette inn J/sigma for E, og bytte inn JS = I og dette resultatet inn i R = V/I og få R = l/sigma S.

Kan også finne effekttap enkelt ved Pj = integral J prikk E over volum. Vet at J og E er samme vei, og konstante over et dv så ting blir ez.
### Eksempel 2.32 Kulemotstand og jording
Ledende kule i midten omgitt av ledende materiale med konduktivitet sigma. Hva er da resistansen mellom indre og ytre skall (radius a og b)?

Symmetri --> J er funksjon av r og radielt rettet.

E = J/sigma.

V = integerer E dr fra a til b

R = V/I, nå er V ok og I antatt fra start.
### Eksempel 2.33 Halvkule
Nå er J=I/S også radiell, men S endret til halvkule. Får da også litt annerledes E-felt.

Kan gjøre det samme og finne R.

Kan da finne ut om man står med distansen r med et ben og r+d med det andre hva potensialforskjellen blir mellom. Kan bli stort! Pass deg for lyn.
# 3: Magnetostatikk
## 3.1: Biot–Savarts lov
En lov som innebærer to punktladninger, deres hastighetsvektorer og avstandsvektoren, litt skalering med tanke på avstand og vakuum, og ut får du en kraftvektor.

En stilig sak at dersom ladningene beveger seg i samme retning skapes en tiltrekkende kraft, og da en frastøtende kraft dersom motsatt retning.

Vi kan også finne flukstettheten B, som blir skapt av en ladning Q1 og dens hastighet v1.

Om vi har beskrevet B kan vi da finne kraftvektoren på en annen ladning med en hastighet med et enkelt kryssprodukt.

Det er stress å vite hastigheten til alle punktladninger. Man kan heller relatere strømtetthet til flukstetthet. Da integrerer man strømtettheten over et volum (eller strømmen over en kurve, eller strømflatetettheten over en flate).

Fra en sirkulær strømsløyfe kan man bruke høyrehåndsregelen at flukstettheten peker i retning tommel hvis hånd krummer langs strømbanen.
### Eksempel 3.1 Magnetisk flukstetthet fra sirkulær strømsløyfe
Vil først uttrykke en dB.

Vet at dB = mu0/4pi I dl x R/R^2 fra formelheftet. Trenger egentlig bare integrere, funker fint når ting er så integral-konstante.

Bruker også ofc at symmetri fører til at det må gå i z-retning.
### Eksempel 3.2 Forholdet mellom Fm og Fe
Kraften that is. Viser at elektrisk kraft er ekstremt mye sterkere, med mindre vi går mot lyshastighet.
## 3.2: Magnetiske krefter
Lorentz-kraft: Tar høyde for både kraft fra B-felt og E-felt.

Magnetisk moment, dipolmoment: m = I S, hvor S er arealet til sløyfa og retning gitt ved høyrehåndsregel (krum hånd langs strømretning, S peker da tommel).

Videre kan man da få mekanisk moment til sløyfa: T = m x B.

### Eksempel 3.3 
Har -q gående i v(0) hastighet i planet, og B-felt som går inn i planet uniformt.

Hva skjer?

Fra formler får du -q v x B = F, som går innover som en sentrepetalaks.

Hva hvis E-felt også går langs B?

F = Q(E + v x B). Ser at denne kraften går i samme retning som E-feltet. Men Q = -q, så vi får en spiralbane ut av planet, med klokka.
### Eksempel 3.5 Hall effekt
Hall-effekten går ut på at en strøm gjennom en rektanguler boks hvor vi har satt opp et B-felt normalt på strømmen vil bli påvirket en magnetisk kraft som bøyer banen til ladningene. Da blir det skeivfordeling av + og - og et E-felt blir satt opp og da en potensialdifferanse.

Får altså en potensialdiff, og kan finne ut fortegnet til ladningsbærerene.
## 3.3: Magnetisk fluks ut av en lukket flate er null -- vektorpotensial
Vi har at phi = integralet av B over S. Dersom S er lukket, er denne fluksen lik null.

Dette betyr at B-flukslinjer biter seg selv i halen.

B kan representeres ved nabla x A, et vektorpotensial. Why bother? Utregninger blir mye ezr.
Man slipper integrasjon ved kryssprodukter og such thangs.
## 3.4: Amperes lov
Denne sier at sirkulasjon av magnetisk flukstetthet rundt lukket sløyfe C er prop med total strøm gjennom S (areal lukket av sløyfa).

Resten av kapittelet:

- En solenoide har magnetfluks kun inne i seg i z-retning (samme retning som solenoiden, høyrehånd strøm)
- En toroide har magnetfluks kun inne i seg i theta-retning (samme som geometrien til toroiden, og i retning høyrehåndsregel mtp strømmen)
### Eksempel 3.7 B-felt utenfor lang sylindrisk leder
Bruk Amperes lov, som kopler B (H)-felt langs en sløyfe (lukket) C og strømmen gjennom.

Via argumenter for retning får du da B-feltet.

Returstrømmen kan vurderes på samme måte, ved å la den gå i en stor halvsirkel tilbake. Ser da at størrelsen på feltet fra denne (i observasjonspunktet r) går mot null når radius på halvsirkelen b går mot uendelig.
### Eksempel 3.8 Magnetisk flukstetthet fra kabler
Her ser vi på èn kabel som går inn i planet og en som går ut. Observasjonspunktet er midt i mellom begge, flyttet ned et stykke.

Bare å adde vektorielt bidragene.
### Eksempel 3.9 Solenoide
N viklinger.

- Biot-Savart: Trenger ikke nødvendigvis være lang, tynn solenoide. Men vanskelig å finne utenom på z-aksen.
- Ampères lov: Må anta uendelig lang. Kan da finne også utenom z-aksen.

BS metoden er likevel vrien. Du bruker formelen for èn enkelt strømsløyfe (det fant vi tidligere, og du kan putte inn z og a for å endre høyde, radius).

Ved å benytte den kan du stå i z = 0, og se på -l/2 + delta, -l/2 + 2delta osv for å se på bidrag fra hver enkelt sløyfe.

Sum alle fra i=1 til N, eller ta heller et integral.

Bruk (z-z')^2, og integrer og dz'/delta, litt slik som gjorde for den myggspiralen.

Får eventuelt et uttrykk for B-feltet i midten av solenoiden.

Hva med Ampères lov metoden?

Først retninger. Kan B-feltet gå langs phi-retning? Nei. Fordi Amperes lov har ingen strøm gjennom seg i denne retningen.

Kan B-feltet gå langs r-retning? Nei. Fordi B-feltet divergerer ikke.

B(r) ikke avh av phi fordi symmetri, og ikke avh av z pga lang.

Vha Ampères lov over en sløyfe utenfor solenoiden er feltet konstant, samme med innsiden av solenoiden. Innsiden fant vi vha BS.

Integralet B prikk dl for en sløyfe C som går halvveis inni solenoiden blir B l', hvor l' er en gitt lengde av solenoiden.

Hvor mange viklinger går gjennom C? l' N/l = gitt lengde ganger viklinger per lengde.

Får da uttrykk for B inni solenoid og (0) utenfor!

### Eksempel 3.10 Toroide
Kan flukslinjene ha r- eller z-komponent? Nei. Hvis vi har èn av de måtte vi hatt begge, siden vi prøver å lage en "rockering" rundt et tverrsnitt av toroiden (siden vi må force flukslinja til å bite seg selv, ellers diverge). Hvis vi tillater denne flukslinja så omfavner løkka ingen strøm, derfor ingen B-felt.

Så vi jobber med phi-retning! Kan flukslinjene skaleres ved phi? Nei, symmetri. 

Deretter er alt rett frem, integrer vha Ampères lov.
## 3.5: Magnetiske felt i materialer
Husk at B-feltet over var gitt i omgivelsene (vakuum). Om vi skal ha det i et materiale bruker vi H-feltet.

Pga mikroskopiske strømsløyfer i materialer skapes det B-felt som kan endre thangs. Derfor bake inn disse i en magnetisering M. Nå er J og Js strømtetthet som er fri (målbar med Amperemeter).

Får nå H-feltet, som er B-feltet (delt på m0) og trukket fra påvirkningen fra M-feltet.

Altså er B-feltet: B = mu0 (H + M).

Husk at E og B er de fysiske feltene, når alt er tatt hensyn til. D og H oppstår pga vi deler i fri vs. bunden.
### Eksempel 3.11 B overalt koaks, mu i mellom
Må nå bruke symmetriargumenter, og Ampères lov for å finne H-feltet.
0 netto strøm inne i innerleder, eller utenfor hele oppsettet.

I mellom: H = I/2 pi r i theta-retn. som forventet!

Videre er B = mu H.
## 3.6: Grensebetingelser for B og H
- B1n = B2n
- H1t - H2t = Js kryss normalvektor
Så hvis ingen flatestrøm:
- H1t = H2t.
### Eksempel 3.12 Grenseflate mellom to lineære medier
- Medium 2: mu{r} >> 1
- Medium 1: Vakuum mu0

Bruker da |B1|^2 = B{1n}^2 + B{1t}^2, og setter in stuff like B{1n} = B{2n}, og at H{1t} = H{2t} når vi ikke har flatestrøm.

Bruk også at B1 = mu H1, og B2 = mu mr H2.

Sammenlign |B1| med |B2|. Ser at bortsett fra normalkomponenter så er |B2| skalert |B1| med mu{r}^2. Så dersom feltet B2 med høy mu{r} er nesten tangentielt, så slippes nesten ikke noe felt ut! Slik holdes fluks inne i jernkjerner osv.

mu{r} for rent jern: 200000. Jern + 4% silisium: 7000.
## 3.7: Magnetiske materialer
http://hyperphysics.phy-astr.gsu.edu/hbase/magnetic/magfield.html

Endelig forstå B vs. H!

B bruker vi i vanlige tilfeller. Men hva om et materiale blir magnetized og skaper eget felt som påvirker B-feltet vi skaper fra eksterne currents?

Da innfører vi H! H er det som oppstår KUN fra eksterne strømmer, og ikke fra det som blir skapt av materialet selv!

- Dimagnetiske materialer
- Paramagnetiske materialer
- Ferromagnetiske materialer

I ferromagnetiske materialer vil magnetiseringen M avhenge av historikk til påtrykt felt; hysteresekurve.

Vi øker H, da vil også magnetiseringen M øke. Får metning. Reduserer H, men M henger igjen.
### Eksempel 3.13 Kvalitativ oppførsel for H-felt og B-felt permamagnet i vakuum/luft
Her ser vi en vertikal sylinder. En magnetisering i z-retning befinner seg. Dermed er B-feltet veldig rett-opp inni sylinderen; de mikroskopiske strømsløyfene hjelper til.

Vi vet at H = B/mu0 - M inne i materialet. Utenfor er H = B/mu0.

La integrasjonskurven C gå langs en flukslinje i B-felt-figuren. Det er ingen frie strømmer, så lukket integral av H langs C skal bli 0 i følge Ampère.

Siden H = B/m0 utenfor er oppførselen der helt lik. Dette betyr at inni så må bidraget være negativt, ellers ville ikke integralet blitt null. Derfor er H rettet motsatt vei inni!
## 3.8: Magnetiske kretser
B-feltet er noen ganger mye større i et materiale enn utenfor, akkurat som at J er større der det er høy konduktivitet.

Sammenligning av elektrisk og magnetisk krets:
- J <--> B
- I = integral tverrsnitt J prikk dS <--> phi tverrsnitt = integral tverrsnitt B prikk dS
- Lukket integral over S av J prikk dS = 0 <--> Lukket integral over S av B prikk dS = 0
- J = sigma E <--> B = mu H
- sigma <--> mu
- Lukket linjeintegral av E prikk dl over C = emf <--> Lukket linjeintegral av H prikk dl = NI
- Resistans R <--> Reluktans Rm
### Eksempel 3.15 -- Fluksen gjennom tynn toroide (radius >> tykkelse)
Har et luftgap også. Har høy permeabilitet slik at fluksen følger toroiden.

Da er phi lik BS, S er tverrsnittareal.
Hvorfor gjelder dette? Fordi tynt tverrsnitt gir at fluksen ikke varierer særlig over tverrsnittet.

Vi har fluksen. Hva kan vi få ut av "spenningsligningen", som integrerer H og får ut NI? 
Får da ved å uttrykke H i luftgapet og i toroiden et uttrykk for B relatert til geometri og NI, og kan sette dette inn i fluksligningen.

Hva med reluktansene? Husk at de er gitt av Lengde/(Permeabilitet Tverrsnitt), som gir mening. Lang lengde gir mye reluktans. Høy permeabilitet gir lite reluktans. Stort tverrsnitt gir lite reluktans.

Typisk at man sier at mu{r} (permeabilitet-scalingen til materialet) går mot uendelig, slik at reluktansen ligger kun i luftgapet.
# 4: Elektrodynamikk
Nå også tidsavhengige felt.
## 4.1: Emf
Fra Walter Lewin vid: "Retningen" til emf er den samme som retningen til indusert strøm.
Du vet retningen til indusert strøm via Lenz' lov.

Husk at emf også er lik lukket integral av E-feltet. Det gir mening. E-feltet er jo V/m. Så dersom du integerer rundt en løkke så har du fulgt E-feltet en runde, og summen blir en emf.

Elektromotorisk spenning = emf.

Batterier gir emf.

Fra wikipedia:

"A source of emf can be thought of as a kind of charge pump that acts to move positive charge from a point of low potential through its interior to a point of high potential. … By chemical, mechanical or other means, the source of emf performs work dW on that charge to move it to the high potential terminal. The emf ℰ of the source is defined as the work dW done per charge dq: ℰ = dW/dq."

Så emfen arbeider for å flytte ladninger fra - til +.
Kan være at kilden til dette arbeidet er kjemisk (batterier) eller mekansik (dynamo) mm.

Emf er lik integralet av kraft per ladning rundt en krets, enhet volt.
Ekstern kraft må motvirke den elektriske motkraften, da er f = -E.

Siden i statikken så er et lukket linjeintegral av E dl lik null, skriver vi emf:

e = kretsintegral (f + E) prikk dl.
## 4.2: Faradays induksjonslov
Vi vet fra før at kraft på en ladning er Q v x B. Da er kraft per ladning f = v x B.

Vi ser på hastigheten til en sløyfe C i et time invariant B-felt. v = dr/dt.

Vi kan da se på indusert emf, uten E-felt (statikk).

Det vises da at i løpet av dt så lager dr x dl en flate, og vi prikker B-feltet gjennom flaten, som gir endring av fluks gjennom flaten.

Får da at:

e = -d/dt int{S} B prikk dS.

Dette er emf indusert via deformasjon. Faradays induksjonslov gir at denne formelen gjelder uansett om det va deformasjon eller tidsvariasjon av B, den gjelder uansett.

Gir mening: Du har en magnet og holder den i ro, og beveger en sløyfe med Q på i forhold. Da finner du den induserte emfen.
Du holder i stedet sløyfa fast, og beveger magneten i stedet, burde da få det samme!

Siden vi ser på (negativ) endring av fluks, kan man også skrive emfen som

e = -d/dt phi,  hvor phi er fluks som vanlig.

Hvis man har N viklinger og hver har samme fluks phi{1} så har man på samme måte

e = -d/dt N phi{1}.

Kan skrive om Faradays lov til differensialform, og får da en av Maxwells ligninger.

nabla x E = -partialderiv wrt time B.
## 4.3: Krets
Skal nå se på emf vs. krets. Total emf er dl integral av f{b}, f{m}, og E.
fb kommer er kraft per ladning i kildene, og integralet av det gir Vb.
I tilleg er f{m} magnetisk kraft pga deformasjon, eller bevegelse.

RI = sum emf. Så alle emfer driver strømmen gjennom resistans.

Da f{b} ga Vb, og fra forrige avsnitt gir f{m} og E -dPhi/dt, har vi at

RI = Vb - d/dt Phi.

Hvis vi ser på (f{b} + f{m} + E) for en typisk krets. Utenom i resistansen, er f{b} + f{m} = -E, da disse kreftene kun jobber mot E-feltet, og da blir (...) = 0.

I resistansen derimot er f{b} = 0 (ingen kilder i resistansen), og hvis ingen bevegelse er f{m} = 0. Da er RI = integral gj. res. av E prikk dl.

### Eksempel 4.1
Ser nå på en krets hvor en metallstang går konstant hastighet mot høyre, og et konstant magnetfelt B går inn i planet normal på kretsen.

Husk at fra 3.2 kan man finne magnetiske krefter ved

F = integral lukket linje av I dl x B. Da får man magnetisk kraft. Denne ville trekke stanga inn mot mindre areal. Hvis konstant hastighet må da en mekanisk kraft ha motsatt retning samme størrelse utover.

Arbeid er kraft ganger vei. Kraften er funnet, vei er v dt. Arbeid per tid er da kraft ganger v, som gir tilført effekt.
### Eksempel 4.2
Viser kommutator og spinnende sløyfe gjennom konstant B-felt.

Resultatet viser at indusert emf er lik (med kommutator):

e = BSw | sin wt |.

Interessant at konstant sløyfeareal og magnetfelt kan skaleres ved w til å gi større emf (men da også hurtigere variasjon).
### Eksempel 4.3 Lenz' lov
Hva skjer om man prøver å flytte en permanentmagnet gjennom en sløyfe?

RI = -dphi/dt. Man får altså en strøm lik negativ endring av fluks med hensyn på tid.

Man får altså en fluks som motsetter seg fluksen man innfører.
Hvor godt gjøres dette? Omvendt prop med R!
Så hvis R = 0 vil phi være konstant. Dvs at en superleder har konstant fluks gjennom seg.
### Eksempel 4.4 Emf i spiralspole
Du har en spiralspole i planet, og et harmonisk varierende B-felt som går inn i planet.

Phi = BS = sum(B pi r^2), hvor hver r varierer.

Eller hvis det er veldig tett, kan du integrere fra indre til ytre radius.

Det er fortsatt B S, men S er nå pi r^2 dr / d integrert fra indre til ytre radius.

Her er d lik avstanden mellom to naboviklinger, og skalererer dr på samme måte som man skalerer vektorer til enhetsvektorer maybe?

Uansett integreres dette, og så finner man emf ved å ta negativ tidsderivasjon.
### Eksempel 4.5 Frekvens vs. strømkonsentrasjon
Anta en jevn fordeling av strøm over tverrsnittet til en sylindrisk leder. Da har vi et magnetfelt som går via curl-right-hand regelen. Hvis man ser på en integrasjon av E prikk dl av en kurve som omfavner en del av fluksen som øker inn i planet (fordi strømmen øker), får man at denne er negativ.

Da blir emf = - -dPhi/dt > 0.

Hva betyr det? At man har E-felt-linjer i retningen til integrasjonsbanen. Dermed bremses strømmen inn mot midten, mens strømmen ytters får ekstra fart.

Skinneffekt!
## 4.4: Selvinduktans og gjensidig induktans
Selvinduktans: Du setter strøm i en spole. Du får et B-felt. Strømmen endrer seg, B-feltet endrer seg. Da endres fluksen gjennom spolen. Da får du en indusert emf, som kan endre strømmen. Dette er selvinduktans.

Selvinduktans: L = Phi / I. Her er det kun fluks som settes opp av I som skal tas med.

Gjensidig induktans: L{ij} = Phi{ij} / I{i}.
Phi{ij} betyr total fluks gjennom spolen j på grunn av strøm I{i} i spole i.

Vi kan nå sette opp emfen som spole j får av i (strømvariasjonen i den):

e{ij} = -dPhi{ij}/dt = -L{ij} dI{i}/dt.

Det er også slik at L{ij} = L{ji}.
### Eksempel 4.7 Selvinduktansen toroide
Du har N1 viklinger, og I1 strøm. Du lar en integrasjonskurve C gå langs toroiden i retningen som fluksen vil gå i pga I1.

Amperes lov lar deg finne H (og da B) relatert til viklinger og I1.

L = Phi / I1. Trenger Phi. Har at Phi = N1 Phi{tverrsnitt}, og Phi{tverrsnitt} finnes via integralet over tvsnt av B prikk dS.

Da er det bare å sette inn!
### Eksempel 4.8 Gjensidig induktans
Hva med L21, gjensidig induktans? 

L21 = Phi{21} / I2.

Dette er på grunn av 2 (strømmen i spole 2). Man ser på total fluks gjennom spole 1.

Vet at L21 = L12.

Så vi kan se på strømmen i spole 1, og total fluks gjennom spole 2.

L12 = Phi{12} / I1.

Så hva gjør du? Du antar en strøm gjennom spole 1 som i forrige eksempel. Du får da også fluksen som i forrige eksempel.
### Eksempel 4.9 Selvinduktans koakskabel
Recap av B-felt koakskabel:

mu I / 2 pi r , i theta-retning. Gjelder i mellom ytre og indre leder, null ellers.

Så akkurat det samme som B-feltet skapt av en current carrying wire!

Husk at vi ser på L = Phi / I. Hvordan finne vi Phi for en koakskabel?

Phi = integral B prikk dS. Vi har B-feltet. For å få en selvinduktans må vi ha et areal. Dermed ser man på en "krets" som går langs innerleder og ytterleder og skaper et areal S.

Dermed et raskt integral and we good! 
### Eksempel 4.10 Transformator
Du setter opp en spenning V1 som gir en strøm I1. Går gjennom viklinger N1. Du skaper fluks.

Du får en selvindusert emf, og du skaper en emf i motsatt spole.

Ved å se på forholdet kan du relatere de til gjensidig og selv-induktans. Da kan du også relatere spenningene til viklingene. Ved å bruke Amperes lov kan du finne strømforhold vs. viklinger.

Du kan også finne motstanden i krets 1 vs motstand krets 2, som er skalert med viklingsforholdet (kvadrert).
### Eksempel 4.11 Bonde stjeler elektrisitet
Du har B=mu0I/2 pi r som vanlig. I = I0 sin(2pi f t).

Du finner gjensidig induktans ved å finne fluksen som går gjennom bondens spole som resultat av magnetfeltet satt opp av linja.

Du kan da finne emf i spolen e{12} som er emf i spole (2) som resultat av linja (1).
Du setter da inn L{12}I i stedet for Phi{12} og deriverer.

Kan da sjekke at man må ha mange kilometer med spoletråd for å få noe særlig ut av det (gitt stor strøm, 50 Hz, stor spole ...).
## 4.5: Energi og krefter i magnetiske felt
NB! Wm under gjelder kun for lineære medier.

Du har joulsk tap, RI^2. Du har også et ledd som sier noe om endringen til systemets lagrede energi.

dAm = I dPhi.

Dette er arbeidet som trengs for å endre fluksen med dPhi.

Dersom intet mekanisk arbeid skjer kan man uttrykke fluksen i èn spole som summen av flukser fra selvinduktans og gjensidig induktans.

Setter du inn alt får du følgende uttrykk:

Wm = 1/2 sum{i=1 -> n} sum{j=1 -> n} L{ij}I{i}I{j}.
### Eksempel 4.12 Energi toroide
Via Amperes lov får du sammenheng mellom viklinger, strøm, H, og geometri. Får da også magnetisk flukstetthet B.

Kan da finne selvinduktans.

Energien finnes da via uttrykket Wm over.

Man ser da at Wm = wm * volum toroide.

Det betyr at wm = energi per volum; energitetthet.
### Eksempel 4.13 Toroide ikke-lineær
Fortsatt Amperes lov for H, N, I, og geometri.

Må nå bruke dAm = I dPhi (generell tilført magnetisk energi ved endring dPhi).

Phi er N B S.
Ampere kan uttrykkes ved I = ...

Så da blir dAm = I{innsatt ampere} N S dB.

Og som før kan man skrive wm = energi per volum, som her blir vist til å bli H dB.
Dersom man integrerer dette over en peroide (H dB), får man tilført energi i løpet av en periode (per volum).

I løpet av en periode har jo feltene gått tilbake til det de var.
Derfor må all energi ha gått til varme.
Derfor er effekttap/volum = areal i hysteresekurve ganger frekvens.
### Eksempel 4.14 Energi i transformator
Her er det bare å bruke formelen for lagret energi.

Wm = ...

Får bidrag fra selvinduktanser og gjensidige induktanser.
### Eksempel 4.15 Koaksialkabel
Nå en ny måte å finne energien på.

Du tar energitettheten wm = 1/2 mu H^2, og integrerer den mellom lederene (dv).
Siden H = I / 2 pi r i theta-retning er H konstant for en gitt dr i integralet.

Husk dv = 2 pi r l dr (tynn sylinder).

En annen fun fact er at du da kan finne Wm' som er energi per lengdeenhet.
Da finner du også lett selvinduktans per lengdeenhet, siden selvinduktansen er gitt av Wm'  = 1/2 L' I^2, når det kun er èn spole.
### Eksempel 4.16 Komfyr med induksjonstopp
Du har et magnetfelt som varierer med tid.
Det gir muligheten gjennom Faradays lov til et sirkulerende E-felt.

Strømtettheten er gitt av J = sigma E (ohm's lov). J prikk E er effekttap per volumenhet.
Integrer det over kasserollebunnen og du får et uttrykk for effekttap.

Uttrykket inneholdre bl.a sin^2(2 pi f t). Tidsmiddelverdi kan man finne ved å fjerne denne og dele på 2.

Man får også hysterese-bidrag ved volum * frekvens * areal i hysteresekurve.

### Magnetiske krefter via uttrykk for total magnetisk energi
Dersom man bruker energi på å flytte noe i x-retning, kan det skrives

F prikk (dx x{enhetsvektor}) = -dWm.

Så da er Fx = -partiell diff mhp x Wm.

Så: F = -nabla Wm,      gitt isolert og tapsfritt system.

Dersom strømmene holdes konstant (via en kilde) kan det vises at arbeidet som utføres av kilden utligner tapet på å flytte i en gitt retning, og man får

F = nabla Wm.
### Eksempel 4.17 Elektromagnet løfter jernstang
Flere ting å ta tak i her.
- Permeabil i kjerne uendelig som vanlig. Siden 1/2 mu H^2 er energi per vol, må da H være null i kjernen. 
- Siden wm = 1/2 mu H^2 = 1/2 1/mu B^2, er energitettheten inni null. Magnetisk energi til systemet er derfor gitt av energi  i luftgap.

Kan da integrere Wm = integral{luftgap} wm dv.

F = - partiell wrt x Wm = - ...

Dette forteller at vi har en tiltrekkende kraft (kraften prøver å minske x).
### Eksempel 4.18 Elektromagnet løfter jernstang, nå med strømkilde
Nå finner du H ved ampères lov (strømmen holdes konstant). Setter inn, og bruker

F = nabla wm

i stedet for -nabla wm, siden konstant strøm.
Det vises at vi fortsatt har en tiltrekkende kraft!
## 4.6: Forskyvingsstrøm
Her modifiseres Amperes lov til å fungere i dynamiske situasjoner (endring i D-felt over tid).

Nabla x H = J + partiell wrt t D.

Så et varierende elektrisk felt gir nå opphav til et magnetfelt!
### Eksempel 4.19 Forskyvningsstrøm vs. parallellplatekondensator
Dersom man ikke tar med forskyvningsstrøm kan man i visse tilfeller få at man ikke har H-felt når man har det.

Altså i Ampere-Maxwell-ligninga (over).
## 4.7: Maxwells ligninger
- D = eps0 E + P
- B = mu0 (H + M)
Når er disse forenklet? I lineære medier!
- D = eps E
- B = mu H
## 4.8: Lorenzpotensialene
Ting er "automatisk oppfylt" når man har et felt på høyre side og en curl på venstre side.
Fordi? Divergens av begge sider gir null.

Vi får i dynamikken ikke E = -nabla V, men

E = -nabla V - partiell wrt t A.

Er potensialene entydig? Nei. Dersom man lar A' = A + nabla f, så kan man se at 
nabla x A = nabla x A' - nabla x (nabla f) = nabla x A'. Det samme!

Vi kan utlede de retarderte potensialene.
Uttrykkene beskriver den elektromagnetiske bølgen som sendes ut fra en gitt strøm- og ladningsvariasjon.
## 4.11: Veien videre

# Khan Academy
## Triboelectric effect and charge
The effect of attraction like balloon vs. hair.

Opposite charges attract, like charges repel.
So + and - attract, - and - repel, + and + repel.

- e is the charge of a proton
- -e charge of electron
- A coulomb 1C is about 6.24 x 10^{18} e
## Coulomb's law
Forces between charges?

Fe (electrostatic) = K | q1 q2 | / r^2. Looks like gravity.

Since Fe is N, then K needs to be N m^2 / C^2.
## Conductors and insulators
Common: A solid has a fixed positively charged nucleus.

Insulators: The electrons can not move (ish).

Conductors: The electrons can move very freely.

What if you add charges? A conductor has charges move as far away from each other as possible. Conductors will then put all of them on the outside edge.

Insulators would have them stuck where you put them.

What if you had a net negative conducting rod and put it against a net zero charge rod? They want to get away from each other so "half" would jump ship to make a situation where the distance between each is maxed.

What if you move net neg rod towards net zero rod, net zero rod connected to ground? Well the electrons would want to get away so they go into ground.

Then you can disconnect from ground before moving net neg away, then move it away, and you have a net positive rod now!
## Conservation of charge
Particles decaying into more particles conserve charge. Everything conserves charge.
## Electric field definition
Positive charge makes an electric field around it. The field is not force.

A force appears if another particle moves into the field.

It is nice to know the field, then don't care about the charges.

E = F/q, force per charge (vectors!).
## Electric field direction
- Positive charges make a field radially outward
- Neg make radially inward

Just put a test charge at a spot and look at exerted force, mapping the field point by point.
## Magnitude of electric field created by a charge
|E1| = k |Q1|/r^2, where Coulombs law was placed into E = F/q.
## Net electric field from multiple charges in 1D
Of course a + charge and a - charge can add up vectorally to make more powerful E-field.
Superposition.
## Net electric field from multiple charges in 2D
Angles and vectors.
## Electric field
Just more of the same.
## Field from infinite plate
Positive test charge, positively (sigma / area) charged infinitely sized plate.

The x-components will all cancel out. So the direciton of the field will point in the z-direction.

Find the contribution of a ring with width dr on the test charge, and then integrate over the whole plate.

Cool result! The field is |E| = 2 k pi sigma. No h involved! So the distance is irrelevant for the electric field.
## Electric potential energy
Electric field is force per charge, E = F/q. 

So how much work to move a positive charge towards a positive inf plate?

W = F times distance. F given by Eq. 

So now you have a potential energy given some reference, just like gravitational potential energy.

What's the work needed to move a positive charge towards a positive charge q1 making an electric field pointing outwards?

dW = -k q1 q2 / r^2 dr.

Why negative? Because each work times distance is the exact opposite of the force exerted on the test charge at that point.

So just integrate from the longer distance to the shorter distance.
This leads us to setting the longer distance (the reference) to infinity.

The result is the potential at the point you ended up, relative to where you started.
## Voltage
- Electric potential energy: Associated with charge. In joules. Found by integrating work.
- Elecric potential: Associated with a position. General, it's per unit charge. So tells you how much work needed for any charge. So in J/C. This is voltage. Volts are given in Joules per Coulomb.
## Electric potential energy of charges
If a + and a + charge is close to each other and you let go of them, they will repel. How did they get this energy? Potential energy.

How much potential energy is contained within this system?

Ue = k Q1Q2/r.

Interesting: Even if the charges initially have different charges, they will move at the same speed (if the masses are the same).
## Electric potential at a point in space
An empty universe has no potential. Put a charge +Q somewhere. Close to this, the potential will be high. An example would be 20 J/C. If you then put a charge of 3C there, the potential energy of it would be 60 J.

So Ue = qV.

A voltage is the difference between two electric potentials in space.
## Electric potential charge configuration
Just add up electric potentials from different sources to find total potential at some point.
## Introduction to magnetism
You can have a single +e or a -e. In magnetism though we always have a dipole, not a pole by itself.

The magnetics are generated by electron spins and their motions around the protons.
## Magnetic force on a charge
Have a magnetic field B.

Then the force vector is:

F = Q(v x B).
## Magnetic force on a proton example
A charge can go in a circle if moving perpendicular to a constant magnetic field.
Given by F = Q(v x B).
## Magnetic force on a current carrying wire
We have still F = Q(v x B).

Now v can be written as v{vec} = l{vec}/t. (or t could be dt).

So F{vec} = Q/t (l{vec} x B{vec}).

Then F{vec} = I (l{vec} x B{vec}).

A wire carrying current creates cocenric magnetic field.
|B{vec}| = mu I / 2 pi r.
## What is a magnetic field?
Remember you can not put a "test charge" into a magnetic field like you can with electric fields- a magnetic monopole does not exist (as far as we know).

Stuff to try:
- Show magnetic field as vectors
- As field lines

"Flow" from North to South. Earth's magnetic south is close (10 degress off) to the geographic north.

Magnetometer: Used to find magnitude of magnetic field. Most use the fact that an electron feels a force as it moves through a m field.

Earth: About 5e-5 T.

Magnitude of magnetic field given by B.
Magnetic field strength given by H, takes into account the effect of magnetic fields being concentrated by magnetic materials.

Permeability = the ability to concentrate magnetic fields.

So what is a magnet? A material needs to satisfy some things:
- Electron spins mostly pair up in opposites, cancelling out magnetic effects.
- Iron has four unpaired electrons and makes a good candidate for magnet
- If we can establish an orientation permanently we have a perm magnet = ferromagnet.
- Some materials can become ordered only in presence of external field, like a kitchen magnet to a refridgerator door. This is a paramagnet.
## Magnetic force between two currents going in the same direction
F{12} is the force from wire 1 acting on wire 2 = I2 L2{vec} x B1{vec}.
So the second wire gets a force on it which scales with the length you look at and the current through it, cross producted by the field generated by wire 1.

But the field around wire 1 circulates, so which direction of B should you use? If using curl-hand-rule then the curl of you fingers will hit the other wire in a direction, use that.
## Magnetic force between two currents going in opposite directions
Now they repel.
## Induced current in a wire
F = Q (v x B), so if you move a wire through a magnetic field, you'll get a force. Charge is then moved, current!

Now work is force times distance.
Work per charge is then

w/Q = L (v x B). This is joules per coulomb. Voltage! Or we are making emf.
## Electric motors
You have a field B going leftward. You have a loop that can rotate, with a current.

You then get a torque on the loop since the farther part of the loop has a force inward and the closer part has a force outward.

The torque changes after 90 degrees, and it will oscillate. So use a commutator.
## Flux and magnetic flux
Flux: How much of something is flowing through a surface per time.

So large flux means more things moving through the surface per time. Can happen because of velocity or density.

Magnetic flux: Phi. The normal components through the given surface. Scales with density.

The unit is Wb, Weber. Tesla times area. The magnetic flux density is then Wb/m^2 = T.
## Faraday's law introduction
Change in magnetic flux through a loop (the surface enclosed by it) will give you an induced current.
This is Faraday's Law. Direction? The direction which opposes the changing flux. Remember curl-right-hand along current gives a magnetic field direction. This should point opposite to the change in flux (or else it would cause an amplification which would never end BLACK HOLE!).
## Lenz's Law
This is the "Direction?" question from above. The induced current sets up a magnetic field which opposes the increased flux.

Induced voltage is: -N times change in flux over change in time. So Wb / s = T m^2 / s = V.
## What is Faraday's law?
### Electromagnetic induction
Process by which a current can be induced to flow due to a changing magnetic field.

We have:
- emf = dPhi/dt, Faraday's law. Tells magnitude of induced emf
- emf = -N dPhi/dt, Faraday-Lenz law. Tells direction of induced current as well.

Drop a magnet through tube with many turns coils connected. Some energy burnt through resistance in wires. What happens?

The magnet falls slower, as the energy must have been taken from the potential energy of the magnet!

Two parallell wires. If you suddenly start a current in the left wire, an opposing current will be set up in the neighbouring wire, trying to oppose the magnetic field set up by the left wire.

Transformer: We apply voltage to a coil of wires around an iron core. The wires will now carry current. There will be flux. 

We know that Phi = BS (constant surface blabla), then if you have a changing magnetic field, you get dPhi/dt = SdB/dt = -emf.
## Emf induced in rod traveling through magnetic field
This is the classic "increasing area current loop with constant perpendicular magnetic field" example.

So dPhi/dt = dBS/dt = BLv dt/dt = BLv.
So then we have an emf = BLv and the current will oppose the increasing flux through the increasing area.
## Faraday's law for generating electricity
