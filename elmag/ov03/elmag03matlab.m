clear all;
close all; 
clc;

eta_0 = 8.85 * 10^(-12); % F/m
V_0 = 10; % V
d = 0.01; % m
rho = -10*10^(-5); % C/m^3

% x_min = 4*eta_0/rho*(rho*d/(eta_0*2) - V_0/d); % 
x_min = 0.015;
V_x_min = rho*x_min*(d - x_min)/(2*eta_0) * (d-x_min) + V_0*(1-x_min/d);