% |B| = mu I / 2 pi r

% The animation shows how the strenght of 
% the magnetic field around the wire increases
% as the current grows.

clc
clear all
close all

mu = 4*pi*10^(-7); % permeability of free space, in N/A^2
I = 0:0.1:10; % A


stepsize = 0.1;
min = -2;
max = 2;
[x, y] = meshgrid(min:stepsize:max, min:stepsize:max);

hsurf = surf(x, y, 0*x);
r = 2;

for i = 1:length(I)
    B = mu*I(i) ./ (2*pi*sqrt(x.^2 + y.^2)*r);
    set(hsurf, 'ZData', B);
    zlim(10^(-6)*[0 3])
    drawnow
    pause(.02)
end