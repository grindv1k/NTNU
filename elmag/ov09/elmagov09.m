close all
clear all
clc

mu_0 = 4*pi*10^(-7); % Ns^2/C^2
mu_r = 5000; % Ns^2/C^2

N = 100; % antall
I = 1*10^(-3); % A

S_LR = 0.02*0.01; % tverrsnitt venstre og høyre
S_TP = 0.01*0.01; % tverrsnitt oppe og nede

% R_i = l_i / (mu_i S_i )

R_L = 0.05 / (mu_r*mu_0 * S_LR );
R_R = R_L - 0.001 / (mu_r*mu_0 * S_LR ); 
R_T = 0.06 / (mu_r*mu_0 * S_TP); 
R_B = R_T;
R_gap = 0.001 / (mu_0 * S_LR);

fluks = N*I / (R_L + R_R + R_T + R_B + R_gap ) % Wb
fluks_infty_muR = N*I / R_gap % 

fluks_infty_muR / fluks