clear all 
close all
clc

syms z;
syms a;
syms mu0;
syms I;

Bz = mu0*I*a^2 / ( 2*(a^2 + z^2)^(3/2) );

Bz_d1 = diff(Bz, z);
pretty(Bz_d1) 

Bz_d2 = diff(Bz_d1, z)