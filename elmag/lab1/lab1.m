% LAB 1 Elmag
clear all
clc
close all

eps_0 = 8.85*10^(-12); % F/m
eps_r = 1; % F/m
a = 0.08; % m
R_osc = 10^6; % ohm
C_osc = 13*10^(-12); % F/m 
d = 0.005; % m
C = eps_0 * eps_r * a^2 * pi / d; % F/m
f_c = 5000; % Hz
f = 10*f_c; % Hz
w = 2*pi*f; % Hz

R_1 = ( ( R_osc / (w^2 * C^2 * R_osc^2 + 1) ) ...
    * ( sqrt( 1 + 3*(w^2 * C^2 * R_osc^2 +1) ) - 1) )

