clear all
close all
clc

syms t
k1 = heaviside(t-1);
k2 = 1-4*exp(-2*t)*sin(2*t);

hold on
plot1 = ezplot(k1, [0 5 -.5 1.5])
plot2 = ezplot(k2, [0 5 -.5 1.5])

set(plot1, 'Color', 'Black', 'LineWidth', 2)
set(plot2, 'LineWidth', 2)

legend('Eksakt  -- h1(t)', 'Approx -- h2(t)')

xlabel('tid')
ylabel('respons')