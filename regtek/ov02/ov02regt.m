M = [1, 10/9; -0.2, 1]; % by eigenvalz
M_inv = inv(M);

A = [-0.5, 0.5; 0.09, -0.15]; 
E = eig(A); % riktig!

[VB,DB] = eig(A); % har nå e^'A't type

ME = M*DB;

M_inv_factored = 11/9*M_inv;

%%
%clear all;
dt = 0.03;
A = [-0.5 0.5; 0.09, -0.15];
PHI = expm(A*dt);

%% 
%clear all;
dx = 0.05;
simtime = 100;
t = 0:dx:simtime;
for i = 1: (length(t))
    x1(i) = 327.27*exp(-0.6*t(i)) + 72.72*exp(-0.05*t(i));
    x2(i) = -65.45*exp(-0.6*t(i)) + 65.45*exp(-0.05*t(i));
end

plot(t,x1, '--', t, x2, 'LineWidth', 2)
legend('x1(t)', 'x2(t)');
xlabel('tid');
ylabel('grader Celsius');