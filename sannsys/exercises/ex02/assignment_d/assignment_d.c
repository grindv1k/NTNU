/**************/
/* Now: Mutex */
/**************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>

void* thread1_func(void* ptr);
void* thread2_func(void* ptr);

pthread_mutex_t mutex;

bool running = true;
int var1 = 0;
int var2 = 0;

int main()
{
    pthread_mutex_init(&mutex, NULL);
    pthread_t t1, t2;

    int t1_s = pthread_create(&t1, NULL, thread1_func, NULL);
    if(t1_s)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", t1_s);
        exit(EXIT_FAILURE);
    }
    int t2_s = pthread_create(&t2, NULL, thread2_func, NULL);
    if(t2_s)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", t2_s);
        exit(EXIT_FAILURE);
    }
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    exit(EXIT_SUCCESS);
}

void* thread1_func(void* ptr)
{
    int myid = (int) pthread_self();
    printf("I am thread %i, and I am running.\n", myid);
    while(running)
    {
        // No mutex: Current value of var1 is read, then might context switch, change var1, then return, then increment the PREVIOUS value
        pthread_mutex_lock(&mutex);
        var1++;
        var2 = var1;
        pthread_mutex_unlock(&mutex);
    }
    printf("I am thread %i, and I am now done.\n", myid);
    return 0;
}

void* thread2_func(void* ptr)
{
    int myid = (int) pthread_self();
    printf("I am thread %i, and I am running.\n", myid);
    for(int i = 0; i < 10; i++)
    {
        pthread_mutex_lock(&mutex);
        printf("Number 1: %d,\tNumber 2: %d\n", var1, var2);
        pthread_mutex_unlock(&mutex);
        usleep(100000);
    }
    printf("I am thread %i, and I am now done.\n", myid);
    running = false;
    return 0;
}
