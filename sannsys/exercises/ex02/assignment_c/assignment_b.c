/*******************/
/* Now: Semaphores */
/*******************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

void* thread_func(void* ptr);

sem_t resources;

int main()
{
  sem_init(&resources, 0, 3);

  pthread_t t1, t2, t3, t4, t5;

  int t1_s = pthread_create(&t1, NULL, thread_func, NULL);
  if(t1_s)
    {
      fprintf(stderr,"Error - pthread_create() return code: %d\n", t1_s);
      exit(EXIT_FAILURE);
    }
  int t2_s = pthread_create(&t2, NULL, thread_func, NULL);
  if(t2_s)
    {
      fprintf(stderr,"Error - pthread_create() return code: %d\n", t2_s);
      exit(EXIT_FAILURE);
    }
  int t3_s = pthread_create(&t3, NULL, thread_func, NULL);
  if(t3_s)
    {
      fprintf(stderr,"Error - pthread_create() return code: %d\n", t3_s);
      exit(EXIT_FAILURE);
    }
  int t4_s = pthread_create(&t4, NULL, thread_func, NULL);
  if(t4_s)
    {
      fprintf(stderr,"Error - pthread_create() return code: %d\n", t4_s);
      exit(EXIT_FAILURE);
    }
  int t5_s = pthread_create(&t5, NULL, thread_func, NULL);
  if(t5_s)
    {
      fprintf(stderr,"Error - pthread_create() return code: %d\n", t5_s);
      exit(EXIT_FAILURE);
    }

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_join(t3, NULL);
  pthread_join(t4, NULL);
  pthread_join(t5, NULL);

  exit(EXIT_SUCCESS);
}

void* thread_func(void* ptr)
{
  sem_wait(&resources);
  int myid = (int) pthread_self();
  printf("I am thread %i, and I am using a resource.\n", myid);
  usleep(1000000);
  printf("I am thread %i, and I am now done.\n", myid);
  sem_post(&resources);
  return 0;
}
