/****************************/
/* Now: Dining philosophers */
/****************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>

void* thread1_func(void* ptr);
void* thread2_func(void* ptr);
void* thread3_func(void* ptr);
void* thread4_func(void* ptr);
void* thread5_func(void* ptr);

pthread_mutex_t f1;
pthread_mutex_t f2;
pthread_mutex_t f3;
pthread_mutex_t f4;
pthread_mutex_t f5;

int main()
{
    pthread_mutex_init(&f1, NULL);
    pthread_mutex_init(&f2, NULL);
    pthread_mutex_init(&f3, NULL);
    pthread_mutex_init(&f4, NULL);
    pthread_mutex_init(&f5, NULL);

    pthread_t t1, t2, t3, t4, t5;

    pthread_create(&t1, NULL, thread1_func, NULL);
    pthread_create(&t2, NULL, thread2_func, NULL);
    pthread_create(&t3, NULL, thread3_func, NULL);
    pthread_create(&t4, NULL, thread4_func, NULL);
    pthread_create(&t5, NULL, thread5_func, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_join(t4, NULL);
    pthread_join(t5, NULL);

    exit(EXIT_SUCCESS);
}

void* thread1_func(void* ptr)
{
    for(int i = 0; i < 1000; i++)
    {
        pthread_mutex_lock(&f1);
        pthread_mutex_lock(&f2);
        printf("Philosopher 1 eating, for time number %d ...\n", i);
        pthread_mutex_unlock(&f1);
        pthread_mutex_unlock(&f2);
    }
    return 0;
}

void* thread2_func(void* ptr)
{
    for(int i = 0; i < 1000; i++)
    {
        pthread_mutex_lock(&f2);
        pthread_mutex_lock(&f3);
        printf("Philosopher 2 eating, for time number %d ...\n", i);
        pthread_mutex_unlock(&f2);
        pthread_mutex_unlock(&f3);
    }
    return 0;
}

void* thread3_func(void* ptr)
{
    for(int i = 0; i < 1000; i++)
    {
        pthread_mutex_lock(&f3);
        pthread_mutex_lock(&f4);
        printf("Philosopher 3 eating, for time number %d ...\n", i);
        pthread_mutex_unlock(&f3);
        pthread_mutex_unlock(&f4);
    }
    return 0;
}

void* thread4_func(void* ptr)
{
    for(int i = 0; i < 1000; i++)
    {
        pthread_mutex_lock(&f4);
        pthread_mutex_lock(&f5);
        printf("Philosopher 4 eating, for time number %d ...\n", i);
        pthread_mutex_unlock(&f4);
        pthread_mutex_unlock(&f5);
    }
    return 0;
}

void* thread5_func(void* ptr)
{
    for(int i = 0; i < 1000; i++)
    {
        pthread_mutex_lock(&f5);
        pthread_mutex_lock(&f1);
        printf("Philosopher 5 eating, for time number %d ...\n", i);
        pthread_mutex_unlock(&f5);
        pthread_mutex_unlock(&f1);
    }
    return 0;
}
