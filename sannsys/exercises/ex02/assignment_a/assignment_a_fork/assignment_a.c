/*************************************/
/* Using fork. Memory is not shared. */
/*************************************/

#include <stdio.h>
#include <string.h>
#include <unistd.h>

int global_var = 100;

int main()
{
    int local_var = 50;
    char buf[1024];

    pid_t pid1 = fork();
    if(pid1 < 0)
    {
        sprintf(buf, "Unsuccessful fork. Fork returned %d\n", pid1);
        write(1, buf, strlen(buf));
    }
    else if(pid1 == 0)
    {
        sprintf(buf, "Hi, I am child. Fork returned %d\n", pid1);
        write(1, buf, strlen(buf));

        sprintf(buf, "Initial values: Global: %d\tLocal: %d\n", global_var, local_var);
        write(1, buf, strlen(buf));

        sprintf(buf, "Incrementing global 3 times, local 10 times ...\n");
        write(1, buf, strlen(buf));
        global_var += 3;
        local_var += 10;

        sprintf(buf, "Values: Global: %d\tLocal: %d\n", global_var, local_var);
        write(1, buf, strlen(buf));
    }
    else
    {
        sprintf(buf, "Hi, I am parent. Fork returned %d\n", pid1);
        write(1, buf, strlen(buf));

        sprintf(buf, "Initial values: Global: %d\tLocal: %d\n", global_var, local_var);
        write(1, buf, strlen(buf));

        sprintf(buf, "Incrementing global 5 times, local 1 time ...\n");
        write(1, buf, strlen(buf));
        global_var += 5;
        local_var += 1;

        sprintf(buf, "Values: Global: %d\tLocal: %d\n", global_var, local_var);
        write(1, buf, strlen(buf));
    }

    return 0;
}
