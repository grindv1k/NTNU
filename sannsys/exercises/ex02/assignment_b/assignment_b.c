/**********************/
/* Now using threads. */
/**********************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

void* thread1_action(void* ptr);
void* thread2_action(void* ptr);

int global_var = 100;

int main()
{
    int local_var = 50;

    pthread_t thread1, thread2;

    int thread1_status = pthread_create(&thread1, NULL, thread1_action, &local_var);
    if(thread1_status)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1_status);
        exit(EXIT_FAILURE);
    }

    int thread2_status = pthread_create(&thread2, NULL, thread2_action, &local_var);
    if(thread2_status)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread2_status);
        exit(EXIT_FAILURE);
    }

    printf("pthread_create() for thread 1 returns: %d\n", thread1_status);
    printf("pthread_create() for thread 2 returns: %d\n", thread2_status);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    exit(EXIT_SUCCESS);
}

void* thread1_action(void* ptr)
{
    // Cast to int* then dereference
    printf("Thread1 local var is now: %d,\tincrementing by 5...\n", *((int*) ptr));
    *((int*) ptr) += 5;
    printf("Thread1 local var is now: %d\n", *((int*) ptr));

    printf("Thread1 global var is now: %d,\tincrementing by 3...\n", global_var);
    global_var += 3;
    printf("Thread1 global var is now: %d\n", global_var);

    return 0;
}

void* thread2_action(void* ptr)
{
    printf("Thread2 local var is now: %d,\tincrementing by 11...\n", *((int*) ptr));
    *((int*) ptr) += 11;
    printf("Thread2 local var is now: %d\n", *((int*) ptr));

    printf("Thread2 global var is now: %d,\tincrementing by 7...\n", global_var);
    global_var += 7;
    printf("Thread2 global var is now: %d\n", global_var);

    return 0;
}
