#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>
#include <unistd.h>
#include <pthread.h>

pthread_t tid[2];

void* sleepy_function() {
  pthread_t id = pthread_self();

  printf("Hi, I am thread %i and this is my first msg\n", (int)id);
  sleep(5);
  printf("Hi, I am thread %i and this is my second msg\n", (int)id);

  return NULL;
}

int main() {
  printf("Creating threads...\n");
  pthread_create(&(tid[0]), NULL, &sleepy_function, NULL);
  pthread_create(&(tid[1]), NULL, &sleepy_function, NULL);

  pthread_join(tid[0], NULL);
  pthread_join(tid[1], NULL);

  return 0;
}
