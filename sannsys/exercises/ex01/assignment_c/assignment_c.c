#include <stdio.h>
#include <stdlib.h>

int allocate(int value) {
  int* ptr = NULL;
  ptr = malloc(1024*1024*1024*sizeof(char));
  if(ptr == NULL){
    return -1;
  }
  *ptr = value;
  printf("Test of allocated memory: %i\n", *ptr);
  return 0;
}

int main() {
  int i = 0;
  while(1) {
    if(allocate(i) != 0) {
      perror("Error: ");
      exit(-1);
    };
  }

  return 0;
}
