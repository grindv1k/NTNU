#include <stdlib.h>
#include <stdio.h>
#include "linked_list.h"

list_t list_create()
{
  struct list* li = malloc(sizeof *li);
  li->head = NULL;
  li->tail = NULL;
  li->length = 0;

  return li;
}

void list_delete(list_t list)
{
  struct node* node = list->head;
  for(int i = 0; i < list->length; i++) {
    struct node* tmp = node->next;
    free(node);
    node = tmp;
  }
  // Only list remains
  free(list);
}

void list_insert(list_t list, int index, int data)
{
  struct node* insertion_point = list->head;

  // Move along until point found
  for(int i = 0; i < index; i++) {
    if(i >= list->length - 1)
      break;
    insertion_point = insertion_point->next;
  }

  struct node* new_node = malloc(sizeof *new_node);
  new_node->data = data;
  new_node->prev = insertion_point->prev;
  new_node->next = insertion_point;

  insertion_point->prev->next = new_node;
  insertion_point->prev = new_node;

  if(new_node->next == NULL)
    list->tail = new_node;
  list->length++;
}

void list_append(list_t list, int data)
{
  struct node* new_node = malloc(sizeof *new_node);
  new_node->data = data;

  if(list->length == 0) {
    // First node
    list->head = new_node;

    new_node->next = NULL;
    new_node->prev = NULL;
  } else {
    new_node->next = NULL;
    new_node->prev = list->tail;

    list->tail->next = new_node;
  }
  list->tail = new_node;

  list->length++;
}

void list_print(list_t list)
{
  struct node* node = list->head;
  for(int i = 0; i < list->length; i++) {
    printf("%i ", node->data);
    node = node->next;
  }
  printf("\n");
}

long list_sum(list_t list)
{
  int sum = 0;
  struct node* node = list->head;
  for(int i = 0; i < list->length; i++) {
    sum += node->data;
    node = node->next;
  }
  return sum;
}

int list_get(list_t list, int index)
{
  struct node* node = list->head;

  for(int i = 0; i < index; i++) {
    node = node->next;
  }

  if(node != NULL)
    return node->data;
  return -1;
}

int list_extract(list_t list, int index)
{
  struct node* extraction_node = list->head;

  // Move along until point found
  for(int i = 0; i < index; i++) {
    if(i >= list->length)
      return -1;
    extraction_node = extraction_node->next;
  }


  if(extraction_node == list->head) {
    list->head = list->head->next;
    if(list->head != NULL) {
      list->head->prev = NULL;
    }
  } else if (extraction_node == list->tail) {
    list->tail->next = NULL;
  } else {
    extraction_node->prev->next = extraction_node->next;
    extraction_node->next->prev = extraction_node->prev;
  }

  int data = extraction_node->data;
  free(extraction_node);
  list->length--;

  return data;
}
