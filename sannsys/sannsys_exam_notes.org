\clearpage

* What can we expect from the exam?
I scoured the /previous exam/ related material, and found the following:
** Longer answers
*** Case
- Need to be able to read a text case and then recommend HW, SW, connection,
  organization, drivers, OS. Additionally need to make actual top level designs.
- Need to be able to deal with planning around deadlines when making designs
- Response time analysis- is it schedulable?
*** Misc assorted
- Task vs threads vs process
- Critical section, unbound time
- Characteristics in  an OS for realtime systems, what's wanted?
- Linux "dual kernel"
- States of processes/threads in a system
- CPU time servers (deferred sporadic), what, how?
- Deadline monotonic vs rate monotonic
- Interrupt latency (electric to routine)
- Mixed criticality system
** Medium (a sentence to... five)
*** Misc assorted
- Reentrant procedure
- Micro- vs monolithic kernel
- Multitasking (context switching)- what happens
- The different timing mechanisms in the Linux kernel
- A three tier client server?
- The goal, means, and hows of priority inheritance and algorithms
- How would you share a buffer between real time processes, in a paging/swapping
  disk setting. Allocation, initialization, use ++
- When is rate monotonic scheduling valid
- Make pros+cons lists on e.g. Linux vs VxWorks
- Tasks: Periodic, sporadic, aperiodic
- Cross compilation
- Mutex vs binary semaphore
- QNX resource manager vs Linux device driver
- Root vs kernel space
- Round-robin scheduling
- Race condition
- Yocto, Buildroot
- Why use same core in a multiple CPU system (for a task?)
- Variations of threads (fibers...)
- Cache
- Circular wait
** Short answers
*** Priority related
- Inheritance vs. deadlocks
- Priority ceiling algorithm
- ICPP = Immediate ceiling prio
- Pros and cons of non-preemptive methods
*** Schedulability related
- Rate monotonic scheduling 
- Schedulability formulas
- Earliest deadline first
- DMPO = Deadline monotonic priority
- Soft/firm/hard real-time
- Understanding sufficient vs necessary
- Given a task set, answers questions about which task finishes first, blocking
  time. This is with and without priority inheritance, ICPP etc.
- Draw a task set graphically
- Utilization test, response time analysis
- Fixed priority scheduling
*** OS related
- Linux allows any user access to real-time
- POSIX
- VxWorks
- QNX
- Windows
- Virtual memory useful for swap/page?
- MMU is?
- FreeRTOS general knowledge
- Preempt-RT patch for Linux kernel
- Memory management fragmentation (external)
*** Real-time specific
- Requirements on deadlocks
- Important characteristics (for an RTOS)
*** Misc
- BusyBox
- Critical section
- Interrupt overhead
- Virtual memory
- Thrashing
- Translation lookaside buffer
* Lecture 01 - Introduction
** Definitions
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:30:50_640310041.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:31:06_624588399.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:31:18_651356323.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:31:33_304366232.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:31:44_012719245.png}
\end{center}
* Lecture 02a - Hardware and operating systems
** Instructions low level
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:33:18_828497994.png}
\end{center}
** Interrupts
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:33:38_366563834.png}
\end{center}

*** Example
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:34:11_848334997.png}
\end{center}

*** Processing of interrupts
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:35:31_599390610.png}
\end{center}

*** Multiple interrupts
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:35:45_857868305.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:36:00_879450762.png}
\end{center}

** Memory

*** Cache
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:36:18_989845055.png}
\end{center}

*** I/O
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:36:57_292880334.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:37:04_643217713.png}
\end{center}

** Operating systems
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:37:26_439988695.png}
\end{center}
*** Convenience
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:39:23_202582427.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:40:00_499686977.png}
\end{center}
*** Efficiency
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:40:17_702324688.png}
\end{center}
*** Multitasking
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:41:02_103305891.png}
\end{center}
*** Process
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:41:16_852423929.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:41:27_830681591.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:42:13_053235861.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:43:35_958479771.png}
\end{center}
* Lecture 02b - Multiprogramming, memory management
** Process model
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:47:22_801539578.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:48:10_887364931.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:48:30_926851636.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:48:38_339699013.png}
\end{center}

*** Modes of execution
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:48:48_695075414.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:50:28_957715057.png}
\end{center}

*** Process switch 
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:49:23_035254801.png}
\end{center}

*** Threads vs process
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:49:44_696012258.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:49:57_091099906.png}
\end{center}

** IPC
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:50:48_826301373.png}
\end{center}

*** Semaphores
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:51:01_653079277.png}
\end{center}

*** Race condition
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:51:13_999389668.png}
\end{center}

*** Mutex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:51:22_627081641.png}
\end{center}

*** Recursive mutex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:51:36_899692457.png}
\end{center}

*** The producer consumer problem
Semaphores:
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:52:35_282921428.png}
\end{center}
Then have sync but not protected access to buffer.
Mutex:
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:52:56_985534894.png}
\end{center}
Message passing:
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:53:08_196466965.png}
\end{center}

** Memory management
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:54:37_349130809.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:54:43_793197879.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:54:56_780256509.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:55:29_906212960.png}
\end{center}

*** Paging, segmentation
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:55:55_060004505.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:56:09_792838972.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:56:27_389494377.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:56:51_287135965.png}
\end{center}

*** Virtual memory
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:57:09_967455383.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_13:57:20_847170170.png}
\end{center}

* Lecture 03a - Scheduling

** Main
Dispatcher = short term scheduler. Other schedulers are medium term (which processes to swap in to main memory)
and long term = add processes to pool of executables.
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:31:34_143176991.png}
\end{center}

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:32:56_023239994.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:33:07_518583540.png}
\end{center}

*** First come first served
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:33:27_125251320.png}
\end{center}

*** Round robin
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:33:51_388627112.png}
\end{center}

*** Shortest next
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:34:03_591538120.png}
\end{center}

*** Shortest remaining time
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:34:20_033540148.png}
\end{center}

*** Highest response ratio
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:34:38_680261909.png}
\end{center}

*** Feedback
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:34:53_320286111.png}
\end{center}

** Simple task model
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:35:30_716938259.png}
\end{center}

*** Cyclic executive
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:35:48_685924882.png}
\end{center}

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:35:59_272407328.png}
\end{center}

*** Fixed priority scheduling
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:36:16_371561754.png}
\end{center}

** Scheduling tests
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:36:35_075867787.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:36:45_778494975.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:36:54_169174687.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:04_345619201.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:09_908300683.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:24_257034208.png}
\end{center}

** Response time analysis
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:35_490205804.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:43_122725428.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:49_900487242.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:37:57_579405867.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:38:04_631946098.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:38:16_095460049.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:38:25_685889992.png}
\end{center}

* Lecture 03b - Deadlocks, mixed criticality, FreeRTOS
** Deadlock
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:42:23_615175694.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:43:30_693706523.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:43:39_463379100.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:43:45_791862423.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:43:52_746137424.png}
\end{center}
** Worst case exec time
Should probably *Google mixed criticality and worst case execution time*
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:44:30_229899818.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:44:40_015156256.png}
\end{center}
** FreeRTOS
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:45:37_939741095.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:45:52_308443488.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:46:04_715196971.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:46:12_910612501.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:46:21_386560257.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:46:26_549203218.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:46:32_582396618.png}
\end{center}

* Lecture 04a - Priority inversion
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:22_925485544.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:28_845611541.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:35_605966437.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:40_404451995.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:45_471412695.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:48:53_000269096.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:02_088699203.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:07_456310348.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:14_222808810.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:28_739746623.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:37_663705599.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:43_717732365.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:50_231804708.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:49:56_138265887.png}
\end{center}

* Lecture 04b - Linux

** Kernel
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:12_332307060.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:19_561667114.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:25_911946719.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:32_508151920.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:39_687356381.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:52:55_128970321.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:04_378868714.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:10_847559825.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:15_639694717.png}
\end{center}

** Processes and threads
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:36_582400623.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:45_412845443.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:53:53_632521417.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:54:04_189211185.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:54:16_178723233.png}
\end{center}

** Libraries
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:54:41_617046772.png}
\end{center}

** Realtime
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:55:03_367466537.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:55:12_412032581.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:55:20_312879443.png}
\end{center}

** Xenomai
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:55:47_914849396.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:55:57_676003149.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:56:06_569027654.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:56:13_403954370.png}
\end{center}

** Embedded linux
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:57:27_986221011.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:57:53_400705603.png}
\end{center}
Consumer Grade ARM has the RPi as an example.
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:58:58_635940452.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_14:59:52_153398286.png}
\end{center}

*** uClibc
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:00:11_057085258.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:00:26_312784075.png}
\end{center}

* Lecture 05a - More Scheduling
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:02:07_881001822.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:02:28_138880670.png}
\end{center}
** Deadline monotonic
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:02:38_917931903.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:02:50_380084463.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:03:22_272040898.png}
\end{center}
** Aperiodic, periodic polling server, deferrable
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:04:03_255003526.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:04:08_472742381.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:04:12_843071388.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:04:19_932983463.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:04:31_108567006.png}
\end{center}
** Earliest deadline first
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:05:16_739967224.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:05:22_396738986.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:05:32_020066454.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:06:08_131731630.png}
\end{center}
** Multicore CPU
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:06:37_090356731.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:06:41_913267589.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:06:49_969983630.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:06:56_040296544.png}
\end{center}

* Lecture 06a - Windows

** Kernel
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:35:46_614911409.png}
\end{center}

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:35:56_784495116.png}
\end{center}

** Processes
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:38:42_149322571.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:38:52_876597913.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:39:02_539734637.png}
\end{center}

** Scheduling
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:39:15_422673513.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:39:23_824123605.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:39:38_763284061.png}
\end{center}

** Embedded compact edition real time
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:42:28_160241740.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:42:36_440881461.png}
\end{center}

* Lecture 06b - VxWorks and Android

** VxWorks
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:43:56_112609444.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:44:14_619807141.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:44:25_400594331.png}
\end{center}

*** BSP
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:46:13_929057777.png}
\end{center}

*** Memory Management
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:46:45_250242083.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:47:17_370410777.png}
\end{center}

*** Task model
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:46:55_942444789.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:47:52_473237758.png}
\end{center}

*** Kernel
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:47:36_390322664.png}
\end{center}

*** Scheduling
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:48:08_110524167.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:48:14_144789124.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:48:22_967438583.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:48:35_112595475.png}
\end{center}

*** IPC
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:48:47_922575952.png}
\end{center}

*** Misc
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:49:23_746520944.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:49:30_151476212.png}
\end{center}


** Android
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:17_579787844.png}
\end{center}

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:49:56_139328315.png}
\end{center}

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:25_149381388.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:30_770283933.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:41_516708713.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:48_599204890.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:50:53_393744062.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:51:00_336444900.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:51:07_505658696.png}
\end{center}

* Lecture 07 - Repetition

** OS overview

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_15:56:08_853892472.png}
\end{center}

*** When to use each type
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:03:42_386688315.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:03:49_612539752.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:04:11_022910385.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:04:18_929255963.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:04:27_739794219.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:04:39_113625509.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:04:51_630328406.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:05:02_297523369.png}
\end{center}

** Memory management
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:05:28_302883680.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:05:34_783040257.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:05:46_250533479.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:06:00_465397945.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:06:14_088103378.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:06:22_161261463.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_12_2017_16:06:35_146123702.png}
\end{center}

