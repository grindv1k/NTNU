#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage{hyperref} \hypersetup{colorlinks=true,} \usepackage{multicol}
#+BEGIN_EXPORT latex
\newcommand{\code}{TTK4147}
\newcommand{\exercise}{Lecture notes}
\newcommand{\semester}{Autumn 2017}
\newcommand{\unamea}{torstgri}
\newcommand{\stdnra}{750870}
\newcommand{\subject}{Sanntidssystemer}
\newcommand{\pagelabel}{Page}
\begin{titlepage}
\begin{center}
    \textsc{\LARGE $\big[$\code$\big]$ \\[0.2cm] \subject}\\[1.0cm]
    \noindent\rule{\textwidth}{0.4pt}
    {\\[1.0cm] \huge \bfseries \exercise \\[1.0cm]}
    \noindent\rule{\textwidth}{0.4pt}
    \vspace{0.5cm}
    \vspace{1cm}
        \texttt{\unamea @stud.ntnu.no}\\[.2cm]
    \vfill
    \includegraphics[width=0.5\textwidth]{/home/grindv1k/NTNU/logontnu_eng} \\
    \vspace{1.5cm}
    \textsc{\large \semester}
\end{center}
\end{titlepage}
\tableofcontents
\clearpage
#+END_EXPORT
* BW - C01 - lecture slides
** Terminology
- Hard real time means missing deadline is no-go.
- Soft real time means still important but not imperative.
- Firm real time means late delievery results are useless but still the system keeps trucking.
* BW - C01 - realtime systems
* BW - C11 - lecture slides
** Cyclic executive
When you have a set of "minor cycles" that together form a "major cycle", which then becomes a periodic thing.
Problems: Need predictability. Tasks with lots of computation must be split into smaller tasks.
** Task based scheduling
Tasks are running, or suspended (waiting for some event).
*** Fixed priority scheduling
Each task gets a priority at compile-time.
*** Earliest deadline first
Nearest deadline gets to go.
*** Value based scheduling
More adaptive.
*** Rate monotonic priority assignment
Unique priority based on period, shorter is better.
** Utilization test
N tasks, if the tasks when summing their utilization values are above the threshold, fail.
Sufficient, not necessary. This means that if it passes, we are good. If the test fails, we can't know for sure.
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_11:51:58_072531597.png}
\end{center}
** Response time analysis
Sufficient, and necessary!
A task has a period, and a computation time, and a priority.
Take the highest priority task, its computation time just needs to be lower than its period.
The next task must add its own computation time to the interference of the higher priority tasks,
which becomes the "new priority".
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_12:01:13_846131229.png}
\end{center}
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_12:01:24_520189417.png}
\end{center}
** Priority inversion
Low prio task steals time. Fix by priority inheritance.
** Ceiling priority protocol
A resource shared by a group of tasks brings with it the highest priority of those tasks, so any
of the tasks using it will be a top predator.
** Immediate ceiling pri protocol
You get the effects of the priority of the resource immediately.
Contrasting with the previous one, there the "priority gain" only happens when a higher prio task
actually tries to preempt you /on that resource/, you will still be preempted for other reasons.
* BW - C11 - scheduling realtime systems
* FreeRTOS - a brief overview

\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_12:23:44_835322240.png}
\end{center}

Blocked state: Queue, semaphore, delay..
** Idle task
Is a cleanup crew ya.
** Binary semaphores
A task blocks on a semaphore, and an ISR respons to an event and gives the sem.
** Scheduler
*** Context switch
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_18:38:01_617445322.png}
\end{center}
* High confidence computing
** Features
Symmetric multiprocessing = SMP.
Can have 3 gB physical RAM; can be disabled for backwards comp.
A local heap manager to deal with memory fragmentation.
Embedded internet explorer.
Antialiased font rendering.
TCP/IP stack.
Improved tools (debugging, silverlight UI thing..)
Remote tools.
Resource leak detector.
* VxWorks 6 
Lightweight RTOS.
Kernel mode = direct access to hardware. User mode is abstracted away, safer but perhaps less responsive.
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_19:22:09_025699115.png}
\end{center}
* VxWorks 7
FEATURES.
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_19:32:54_826312208.png}
\end{center}
* How fast is fast enough? Xenomai vs linux RT kernel
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_19:36:43_396384254.png}
\end{center}
Xenomai has great performance when it comes to response time; the median response time is close to all response times.
However, if only 95% hard real time is necessary, maybe a linux RT kernel is better.
* Xenomai overview
Either a patched version of Linux; a "co-kernel" extension, or as libraries for native linux.
* Lecture 01: Introduction
An overview: 
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/25_11_2017_19:53:34_563773825.png}
\end{center}
* QNX
** Microkernel
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_14:22:29_425614601.png}
\end{center}
#+END_EXPORT
Microkernel: Scheduling, memory management, IPC. Only what's needed.
** Why use for embedded
Scalable (small and huge), microkernels are suitable for real-time apps.
A process is in a non-preemptive kernel state for only a few microseconds (so little ovearhead).
Scheduling with priorities and inheritance.
IPC built in.
** Threads and processes
Thread = scheduling unit.
Process = thread container.
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_14:43:51_448972485.png}
\end{center}
#+END_EXPORT
*** Process memory
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_14:52:26_702596739.png}
\end{center}
#+END_EXPORT
** Scheduling
- 256 prios, 1-63 configurable, 64-255 is root-only.
- Running thread blocked --> unblocked means added to end of ready queue
- Running thread preempted due to high prio ready --> preempted put in front of queue. Running thread then yields, and scheduler can select next thread.
- Sporadic server: A task has a proirity budget which is exhausted after some time. This means the priority drops. It gets replenished after some time.
** Interrupts
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_15:17:18_939835931.png}
\end{center}
#+END_EXPORT
User-space programs with sufficient privileges can attach functions to interrupts.
There are also nested interrupts.
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[scale=0.2]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_15:19:50_583964429.png}
\end{center}
#+END_EXPORT
** Message passing
Process creates channel. Another process can attach to this channel.
Can also use \texttt{pthread} things. Message passing uses priority inheritance.
** Shared memory
When used: Direct read/write access, unsynced, unprotected, should use mutex.
** Device programming
Compare Linux in general vs QNX for communication with hardware:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[scale=0.2]{/home/grindv1k/NTNU/sannsys/scrots/16_10_2017_15:44:19_300599287.png}
\end{center}
#+END_EXPORT
*** Resource manager
Is a driver and interface to hardware. Puts files in \texttt{/dev}. Other programs R/W to this.
* Windows
** Kernel etc
Schedule threads, exception+interrupts, sync between multiprocessors.
*** Executive
Runs as kernel-level threads. Does memory management. Create, manage, delete processes+threads.
I/O manager.
*** HAL = Hardware abstraction layer
Between kernel and hardware. HAL translates hardware into a genereic interface for the kernel.
*** Device drivers
Extends functionality of the executive. Used for interacting with a specific I/O device.
*** Graphics
Provides GUI functions. We do this in kernel mode. This is for performance reasons (reduces context switches).
** Windows objects
Core windows writen in C. The design is object-based still.
There is an object manager.
*** Encapsulation
Data (attributes) and procedues (services). Attributes only accessible through services.
*** Misc
There is support for inheritance and polymorphism (limited)
** Threads
\begin{center}
\includegraphics[width=0.8\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_10:54:35_538434205.png}
\end{center}
** Scheduling (multiprocessor)
*Soft affinity*: Kernel tries assigning thread to previous processor (reuse of cache then possible)
*Hard affinity*:  Only one processor used.
** Dispatcher objects
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_11:19:25_019176075.png}
\end{center}
*** 
* VxWorks and Android
** VxWorks
Closed off documentation...
\begin{center}
\includegraphics[width=0.7\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_14:24:41_017253485.png}
\end{center}
VxWorks did not have memory management earlier. Does now. See below.
\begin{center}
\includegraphics[width=0.5\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_14:26:55_560004064.png}
\end{center}
*** Scheduling
On a single core, we have a preemptive scheduler that runs the highest prio task,
and other tasks are FCFS or round robin.
Can also do POSIX scheduler, partition scheduler (time slots) or a custom scheduler.
When on multi-core, a task can run on any core. A task can set CPU affinity. Can also have
a task reserve a core for exclusive usage (no preemption there!).

When doing RTP (real time processes) you can have them as foreground RTPs (assigned time slots),
but at the same time you can have background RTPs which only run when foreground RTPs are not. Cool!
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_14:48:25_434261593.png}
\end{center}
*** IPC
We have semaphores, mutexes, message queues.
Also have \texttt{taskLock()} and \texttt{taskUnlock()} to disable/enable preemption. Faster
than semaphores! This is because all other tasks are prevented from running (instead of them
trying for a resource and being blocked).
** Android
*** Linux kernel changes
\texttt{glibc} not used, rather an own standard lib \texttt{Bionic}.
Does not use X Window System.
Not the same basic utils.
So not directly compatible with linux. Mostly dev happens in Java (Kotlin tho).
Can support C/Cpp.
*** Android Applications
Activites: A screen visible in the UI
Services: Background task (can run even when other apps are used)
Content provider: Interface to app data. File, SQLite db, etc
Broadcast receiver: Responds to syste-wide announcements
**** Activity life cycle
\begin{center}
\includegraphics[width=0.8\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_15:29:46_769973149.png}
\end{center}
**** IPC
\begin{center}
\includegraphics[width=\textwidth]{/home/grindv1k/NTNU/sannsys/scrots/06_11_2017_15:30:48_686906694.png}
\end{center}
This is called Binder. Lightweight remote procedure calls.
**** Real time
Probably not.
