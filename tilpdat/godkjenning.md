# Godkjenning TilpDat

## Øving 10

 means confirmed godkjent.

| Navn!                             | Godkjent? |
| --------------------------------  | :-------: |
| Tom Daniel Grande                 |          |
| Vebjørn Steinsholt                |          |
| Jørn Bøni Hofstad                 |          |
| Atussa Koushan                    |          |
| Tonja Joseph                      |          |
| Raoul Pathak                      |          |
| Kristian Enoksen                  |          |
| Filip Gornitzka Abelson           |          |
| Andreas Haugland                  |          |
