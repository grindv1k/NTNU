#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
using namespace std;
using namespace cv;

int main(int argc, char** argv)
{


    VideoCapture cap;
    cap.open("rtp://192.168.123.201:5000/");
    if (!cap.isOpened())
        return -1;

    Mat edges;
    namedWindow("edges",1);
    for(;;){
        Mat frame;
        cap>>frame;
        imshow("edges",frame);
        if(waitKey(30)>=0)break;
    }

    return 0;
}
