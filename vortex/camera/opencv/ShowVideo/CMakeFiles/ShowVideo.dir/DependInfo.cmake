# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/grindv1k/NTNU/vortex/camera/opencv/ShowVideo/showvideo.cpp" "/home/grindv1k/NTNU/vortex/camera/opencv/ShowVideo/CMakeFiles/ShowVideo.dir/showvideo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/grindv1k/repos/opencv/build"
  "/home/grindv1k/repos/opencv/include"
  "/home/grindv1k/repos/opencv/include/opencv"
  "/home/grindv1k/repos/opencv/modules/core/include"
  "/home/grindv1k/repos/opencv/modules/flann/include"
  "/home/grindv1k/repos/opencv/modules/imgproc/include"
  "/home/grindv1k/repos/opencv/modules/ml/include"
  "/home/grindv1k/repos/opencv/modules/objdetect/include"
  "/home/grindv1k/repos/opencv/modules/photo/include"
  "/home/grindv1k/repos/opencv/modules/video/include"
  "/home/grindv1k/repos/opencv/modules/dnn/include"
  "/home/grindv1k/repos/opencv/modules/imgcodecs/include"
  "/home/grindv1k/repos/opencv/modules/shape/include"
  "/home/grindv1k/repos/opencv/modules/videoio/include"
  "/home/grindv1k/repos/opencv/modules/highgui/include"
  "/home/grindv1k/repos/opencv/modules/superres/include"
  "/home/grindv1k/repos/opencv/modules/ts/include"
  "/home/grindv1k/repos/opencv/modules/features2d/include"
  "/home/grindv1k/repos/opencv/modules/calib3d/include"
  "/home/grindv1k/repos/opencv/modules/java/include"
  "/home/grindv1k/repos/opencv/modules/stitching/include"
  "/home/grindv1k/repos/opencv/modules/videostab/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
