syms x
syms R 
assume(R > 0)
syms Pi

A = Pi*R^2/4;
expr = 1/A*x*sqrt(R^2-x^2);

ans = int(expr, x, 0, R);
pretty(ans)