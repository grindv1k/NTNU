% ov04
% V_vector = (u,v) = (a^2-(b-cx)^2)*i_vector + (-2cby+2c^2xy)*j_vector

% konstanter
a = 0.6;
b = 0.5; 
c = 0.2;

[x, y] = meshgrid(-2:0.5:2, -2:0.5:2);
u = a^2-(b-c*x).^2; % i-retning
v = -2*c*b*y+2*c^2*x.*y; % j-retning
quiver(x,y,u,v);
xlabel('x');
ylabel('y');

% Eneste stagnasjonspunkt i x=-.5, y=0