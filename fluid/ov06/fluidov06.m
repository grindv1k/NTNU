clear all
close all
clc

format short

R = 1; % m
dpDx = -50; % Pa/m
MU = [.5, 1, 2]; % kg/(m s)

dr = 0.05;
r = -R:dr:R;

u = 1./(4 * MU) * dpDx;
a = (r.^2 - R^2);
a = a';

u_dawg = a*u;

for i=1:length(MU)
    str = num2str(MU(i)) ;
    
    if length(str) == 1
        str = strcat(str, '.0');
    end
    
    D(i, :) = strcat('\mu = ', str);
end

u_dawg = u_dawg';

hold on
for i=1:length(MU)
    plot(u_dawg, r, 'LineWidth', 2)
end
legend(D);

xlabel('x')
ylabel('r')

% quiver(r*0, r, u_dawg(1,:), u_dawg(1,:)*0)
