y1 = 1; % placeholder, m
y2 = 3; % placeholder, m
P1 = rho*g*y1/2; % kg/(s^2 m)
P2 = rho*g*(y1-y2/2); % kg/(s^2 m)
B = 1; % m
A1 = y1*B; % m^2
A2 = (y1-y2)*B; % m^2
rho = 1000; % kg/m^3
g = 9.81; % m/s^2

V1 = y2*sqrt(2*g/(y1+y2));
V2 = y1*sqrt(2*g/(y1+y2));

%%
clc
clear all
close all
f = @(a) 1/2 - a^2 + 4*a^2/(1 + a) - 4*a/(1+a)
fzero(f,0.2)

%% Oppg tillegg
clc
clear all
close all

grid on
hold all
eps = [0 0.0001 0.001 0.01];
legends = cell(1, length(eps));
D = 1;
Re = logspace(3,8);
for j=1:length(eps)
for i=1:length(Re)
    colebrook = @(f) 1/sqrt(f) + 2*log10((eps(j)/D)/3.7 + 2.51/(Re(i)*(sqrt(f))));
    f(i) = fzero(colebrook, [0.00001 1]);
end
legends{1,j}=strcat('\epsilon/D = ', num2str(eps(j)/D));
plot(log10(Re), log10(f));
end
legend(legends)
