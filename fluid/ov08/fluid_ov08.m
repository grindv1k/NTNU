%% 5.78
clear all
close all
clc

g = 9.81;
h1 = 20;
h2 = 27;
rho_v = 1000; 

delta_P = g*rho_v*(h2-h1)

%% 5.87
clear all
close all
clc

D = 0.025; %m
P1 = 300*10^3; % Pa
rho_v = 1000; % kg/m^3
g = 9.81; % m/s^2
h1 = 8; % m
h_L = 2; % m

V = sqrt(2*P1/rho_v - 2*g*(h1+h_L) ); % m/s
A = pi*D^2/4; % m^2

volumstroem = V*A 

%% 5.93
clear all
close all
clc

g = 9.81; % m/s^2
rhoS = 1030; % kg/m^3
D1 = 0.1; % m
dV1dt = 0.04; % m^3/s
D2 = 0.05; % m
hL = 3; % m
h1 = 3; % m
eta = 0.7; % 
A1 = pi*D1^2/4; % m^2
A2 = pi*D2^2/4; % m^2

V2 = dV1dt / A2
V1 = dV1dt / A1

l1 = A1/A2
l2 = A2/A1

%% 5.93 -- Matlab
clear all
close all
clc

g = 9.81; % m/s^2
V = 20.4; % m/s
alpha_deg = [0:5:70];
alpha = alpha_deg*2*pi/360;
a_x = 0; % m/s^2
a_y = g;
dt = 0.1;
t = 0:dt:20;
x = V*cos(alpha)' * t;
y = V*sin(alpha)' * t;
for i=1:1:size(y)
y(i,:) = y(i,:) - g/2*t.^2;
end

hold on
for i=1:1:size(y)
plot(x(i,:),y(i,:), 'LineWidth', 1.5)
axis([0 50 -3 20])
end
hold off


xlabel('Distanse; meter')
ylabel('Høyde; meter')
title('Vannjet')
legend([kron(ones(size(alpha_deg')),'\alpha = '),num2str(alpha_deg')])

%% 
clear all
close all
clc

rho = 1.23; % kg/m^3
rhoS = 800; % kg/m^3
visc = 1.5 * 10^(-5); % m^2/s

g = 9.81; % m/s^2
dH = (40-7.5) * 10^(-3); % m
dP = rhoS*g*dH;

D1 = 0.04; % m
A1 = pi/4*D1^2; % m^2
D2 = 0.06; % m
A2 = pi/4*D2^2; % m^2
D3 = 0.03; % m
A3 = pi/4*D3^2; % m^2

V1 = sqrt(2*dP/rho * 1/(1 - A1^2/A2^2) ) 
V2 = V1 * A1/A2
Re2 = V2 * D2 / visc
dH2 = 20 * 10^(-3);
x = 20; % m
dP2 = rhoS * g * dH2;
dP2dx = dP2/x
V3 = V1*A1/A3
Re3 = V3*D3/visc
dH3 = -42.5 * 10^(-3);
dP3 = rhoS * g * dH3
dP3dx = dP3;
