close all
clear all
clc

x_min = 0;
x_max = 4;
y_min = 0;
y_max = 4;

dt = 0.4;

[x,y] = meshgrid(x_min:dt:x_max, y_min:dt:y_max);

u = 1 + 2.5*x + y;
v = -0.5 - 1.5*x - 2.5*y;
du_dx = 2.5;
du_dy = 1;
dv_dx = -1.5;
dv_dy = -2.5;
a_x = 0 + u * du_dx + v * du_dy + 0;
a_y = 0 + v * dv_dx + v * dv_dy + 0;

subplot(2, 1, 1)
quiver(x, y, u, v);
title('Velocity plot')
axis([x_min x_max y_min y_max])
xlabel('x')
ylabel('y')

subplot(2, 1, 2)
quiver(x, y, a_x, a_y);
title('Acceleration plot')
axis([x_min x_max y_min y_max])
xlabel('x')
ylabel('y')