close all
clear all
clc

rho = 894; % kg / m^3
mu = 2.33; % kg / m * s
Vavg = 0.5; % m / s
Dh = 40e-2; % m

Re = Vavg*Dh*rho/mu

% Laminar!
A = 1/4*Dh^2*pi;
VolFlow = Vavg*A;
L = 300; 
g = 1; % cancelled
h_L = 32/Re*L/Dh*Vavg^2/g

P = h_L*rho*g*VolFlow