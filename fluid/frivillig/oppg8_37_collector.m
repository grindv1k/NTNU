clear all
close all
clc

rho = 1.109; % kg / m^3
mu = 1.94e-5; % Pa / s;
nu = mu/rho; % Pa*m^3 / (s * kg)

L = 5; % m
B = 1; % m
h = 0.03; % m

A = B*h; % m^2

p = 2*B + 2*h; % m

Dh = 4*A/p; % m

VolFlow = 0.15; % m^3 / s

Vavg = VolFlow/A; % m / s

Re = Vavg*Dh/nu

dP1 = rho*32/Re*L/Dh*Vavg^2

epsilon = 0; % disregard roughness

colebrook = @(f) -sqrt(f)*2*log10(epsilon/Dh/3.7 + 2.51/(Re * sqrt(f))) - 1;
fr = fzero(colebrook, 0.1)


dP2 = rho*L/Dh*Vavg^2/2 * fr

% test haaland
haaland = 1/(1.8^2 * log10(6.9/Re)^2)