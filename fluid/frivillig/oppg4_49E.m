close all
clear all
clc

U0 = 5; % feet/s
b = 4.6; % s^(-1)
dx = 0.1;
dt = 0.001;

x_a = 0.25:0.1:0.75; % ft
% y_a = 0.75:0.1:1.25; % ft
y_a = [1 1 1 1 1 1] - 0.25;
y_a2 = y_a + 0.5;

x_a = x_a';
y_a = y_a';
y_a2 = y_a2';

% Dimensions of particle at t = 0 is a square with edge 0.5 ft
t = [0 0.2];
xPos = 1/b * ((U0 + b*x_a)*exp(b*t) - U0);
yPos = y_a * exp(-b*t);
yPos2 = y_a2 * exp(-b*t);


x = 0:dx:5;

hold on
for i=0:dx*7:15
u = i./(U0 + b * x);
plot(x, u)
u = -i./(U0 + b * x);
plot(x, u)
end

scatter(xPos(:,1), yPos(:,1), 'filled');
scatter(xPos(:,2), yPos(:,2), 'filled');
scatter(xPos(:,1), yPos2(:,1), 'filled');
scatter(xPos(:,2), yPos2(:,2), 'filled');

hold off