close all
clear all
clc

U0 = 0.5; % m/s
b = 1.2; % s^(-1)
dt = 0.1;

x = 0:dt:5;

hold on
for i=1:dt*8:15
u = i./(1.2*(U0 + b * x)) - 5/3;
plot(x, u)
end
axis([0, 5, 0, 6])
hold off