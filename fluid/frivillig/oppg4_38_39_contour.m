close all
clear all
clc

x_min = -3;
x_max = 3;
y_min = -3;
y_max = 3;

dt = 0.4;
omega = 1;

[x,y] = meshgrid(x_min:dt:x_max, y_min:dt:y_max);

u_r = 0;
K = 1; % m^2/s

r = sqrt(x.^2 + y.^2);

subplot(2,1,1)
u_theta = omega * r;
contour(x, y, u_theta,15)
title('4-38: solid-body rotation')

subplot(2,1,2)
u_theta = K * r.^(-1);
contour(x, y, u_theta,15)
title('4-39: line vortex')
