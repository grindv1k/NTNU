clear all;
close all;
clc;

a = 2;
b = 3;
c = 4;

dt = 0.5;

[x,y] = meshgrid(-10:dt:5, -10:dt:5);

u = a^2 - (b - c*x).^2;
v = -2*c*b*y + 2*c^2*x.*y;

quiver(x,y,u,v)