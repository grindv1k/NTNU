close all
clear all
clc

U0 = 5; % feet/s
b = 4.6; % s^(-1)
dt = 0.1;

x = 0:dt:5;

hold on
for i=0:dt*7:15
u = i./(U0 + b * x);
plot(x, u)
u = -i./(U0 + b * x);
plot(x, u)
end
hold off