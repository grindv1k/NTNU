close all
clear all
clc

dt = 0.7;

[x,y] = meshgrid(0:dt:5, 0:dt:6);

u = 0.5 + 1.2*x;
v = -2.0 - 1.2*y;

subplot(2, 1, 1)
quiver(x, y, u, v);
title('Velocity plot')

a_x = u * 1.2;
a_y = v * (-1.2);
subplot(2, 1, 2)
quiver(x, y, a_x, a_y);
title('Acceleration plot')

axis([0 5 0 6])
