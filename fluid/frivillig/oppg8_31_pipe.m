clear all
close all
clc

rho = 999.7; % kg / m^3
mu = 1.307e-3; % kg / (m * s)
D = 2e-3; % m
L = 15; % m
Vavg = 1.2; % m / s
g = 9.81; % m / s^2
A = 1/4 * D^2 * pi;
Volumetric_flow = Vavg * A;

P_change = 32 * mu * L / D^2 * Vavg
h_L = P_change / (rho * g)

Pow_needed = P_change * Volumetric_flow