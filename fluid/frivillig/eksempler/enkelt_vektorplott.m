% Enkelt vektorplott, eksempel 4-1
% u = 0.5+0.8*x, v = 1.5-0.8*y
clear all
close all
clc
[x,y]=meshgrid(-3:0.5:3,-1:0.5:5);
u=0.5+0.8*x;
v=1.5-0.8*y;
quiver(x,y,u,v)
