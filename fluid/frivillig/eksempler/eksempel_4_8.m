clear all
close all
clc
a=1;
[X,Y]=meshgrid(0:0.5:5,0:0.5:5);
u=X.^2;
v=-2*X.*Y-1;
quiver(X,Y,u,v)
hold on
grid on
x=[.8 1.2 1.2 .8 .8];
y=[3.8 3.8 4.2 4.2 3.8];
h=plot(x,y,'r','LineWidth',4);
ht=title(['Areal =',num2str(polyarea(x,y),2)]);
dt=0.0005;
pause
for t=0:dt:.5
    nx=x+dt*x.^2;
    y=y+dt*(-2*x.*y-1);
    x=nx;
    set(h,'XData',x,'YData',y);
    set(ht,'String',['Areal =',num2str(polyarea(x,y),2)]);
    drawnow;
end
hold off

