% Assignment 4 - matlab - TEP 4100

clear all
clc
close all

a       = 0.6;                                                              
b       = 0.5;
c       = 0.2;

%Create a grid:
[x,y]   = meshgrid(-2:0.5:7, -2:0.5:2);
               
%Create the velocities:
u       = a^2 - (b-c*x).^2;                                                 
v       = -2*c*b*y + 2*c^2*x.*y;
                                                              
%Plotting
figure(1) = figure('Name','Vector plot of a velocity field',...            
    'NumberTitle','off');                                                        

quiver(x,y,u,v)                                                     

ylabel('y position [m]');                                                  
xlabel('x position [m]');