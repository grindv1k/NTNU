% Vektorplott med strømlinjer
% u = a*x, v = -a*y, a=konstant
clear all
close all
clc
a=1;
[x,y]=meshgrid(-5:0.5:5,-5:0.5:5);
u=a*x;
v=-a*y;
C=x.*y;
contourf(x,y,C,20)
hold on
quiver(x,y,u,v)
hold off