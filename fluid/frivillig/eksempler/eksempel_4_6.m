clear all
close all
clc
a=1;
[X,Y]=meshgrid(-1:0.5:5,-1:0.5:5);
u=a*X;
v=-a*Y;
quiver(X,Y,u,v)
hold on

x=[1 2 2 1 1];
y=[4 4 5 5 4];
h=plot(x,y,'r','LineWidth',4);
ht=title(['Areal =',num2str(polyarea(x,y),2)]);
dt=0.001;
pause
for t=0:dt:1
    x=x+dt*a*x;
    y=y+dt*(-a*y);
    set(h,'XData',x,'YData',y);
    set(ht,'String',['Areal =',num2str(polyarea(x,y),2)]);
    drawnow;
end




hold off

