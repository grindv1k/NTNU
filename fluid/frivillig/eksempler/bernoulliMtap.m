% Eksempel: Et reservoir med dybde H har et utl�psr�r med lengde L og
% diameter D. Finn utl�pshastigheten v.
clear all
close all
clc
g=10;
H=1;
L=1;
D=0.01;
ny=1e-6;  % For vann

% Iterasjonsprosess:
Re=3e4    % Gjetter p� et Reynoldstall
for iter=1:5
    v=sqrt(2*g*H/(1+0.316*L/(Re^0.25*D)))
    Re=v*D/ny
end

% Finner l�sningen direkte:
null=@(v) g*H-v^2/2*(1+0.316*L/((v*D/ny)^0.25*D));
v=fzero(null,[0.1 10])

% Sjekker innl�pslengden Lh:
Re=v*D/ny;
Lh=D*1.36*Re^0.25

% Bruker Coolebrook's formel for � finne v
Re=3e4
epD=0;  % Pr�v med forskjellige verdier av esilon/D
for iter=1:5
    colebrook=@(f) 1/sqrt(f)+2*log10(epD/3.7+2.51/(Re*sqrt(f)));
    f=fzero(colebrook,[0.0000001 1])
    v=sqrt(2*g*H/(1+f*L/D))
    Re=v*D/ny
end
