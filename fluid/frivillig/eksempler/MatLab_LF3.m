clear all
clc
close all
g=9.81;
rho_block=2700;
rho_mud=1400;
height_block=1.2;
fric_coeff=[0.4, 0.6]';
width=[0.05:0.05:1];
weight_block=rho_block*g*height_block*width;
Force_friction=fric_coeff*weight_block;
height_mud_slide=sqrt(Force_friction/(rho_mud*g/2));
height_mud_tip = (3*weight_block.*width/(rho_mud*g)).^(1/3);

figure(1)
hold on
plot(width, height_mud_slide, width, height_mud_tip);
ylabel('Mud height [m]');
xlabel('Block width [m]');

legend('height of mud when blocks slide, f=0,4',...
       'height of mud when blocks slide, f=0,6',...
       'height of mud when blocks tips','Location','Best')
grid on