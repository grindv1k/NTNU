% Vektorplott av hastighetsfeltet 
% fra oppgave 1, Auditorie�ving 1
clear all
close all
clc
U=1;
L=1;
[x,y]=meshgrid(-L:0.1:L,-L:0.1:L);

% Anonyme funksjoner for u og v:
u=@(x,y,t)-U*x.^2.*y.*sin(U*t/L)/L^3;
v=@(x,y,t)U*x.*y.^2.*sin(U*t/L)/L^3;

for t=0:0.01:2*pi
    U=u(x,y,t);
    V=v(x,y,t);
    quiver(x,y,U,V,0)
    axis([-L L -L L])
    title(['Time = ',num2str(t,2)])
    drawnow;
end

