clear all;

% Trykk:
startTrykk = 310*10^3 ; % kPa
temperaturer = [25, 43, 57, 21, 32, 43, 50] + 273.15 ; % K
alleTrykk = temperaturer/temperaturer(1) * startTrykk;

% Masser:
startVolum = 0.025  ; % m^3
R_luft     = 287    ; % Pa m³ / kg K
masser = startTrykk ./ temperaturer / R_luft * startVolum; 
masseDifferanser = masser(1) - masser ; 

bar(0:6, masseDifferanser);