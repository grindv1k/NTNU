\subsection{Little Book of Semaphores Chapter 4 -- Classical synchronization patterns} 
\subsubsection{Chapter 4.1 -- Producer-consumer problem}
Producer: Mouse movement. If a movement happens, place it in an event buffer of sorts. When available, a consumer will handle the contents of the buffer.

Nuances:
\begin{itemize}
    \item Items being added/removed from buffer makes the buffer inconsistent; we need exclusive access to buffer
    \item Consumer arrives when empty buffer? Block until producer adds item
\end{itemize}
The first puzzle involves fixing basic producer- and consumer code. Let's try:

Producer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    event = waitForEvent()\;
    buffer.add(event)\;
\end{algorithm}
Consumer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    event = buffer.get()\;
    event.process()\;
\end{algorithm}
This was the supplied version, let's apply the constraints:

Producer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    event = waitForEvent()\;
    bufferAccess.wait()\;
    buffer.add(event)\;
    bufferAccess.signal()\;
    isNotEmpty.signal()\;
\end{algorithm}
Consumer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    isNotEmpty.wait()\;
    bufferAccess.wait()\;
    event = buffer.get()\;
    bufferAccess.signal()\;
    event.process()\;
\end{algorithm}

Where isNotEmpty is initialized to zero, and bufferAccess is initialized to one.

Yay, this solution was marked as the improved version. A broken solution is found if you place "isNotEmpty.wait()" inside the consumer "bufferAccess" mutex. Why?

I think if the consumer then takes both semaphores, we get a deadlock.

This was correct. We now see a pattern, if you need a semaphore while holding a mutex, you might be in trouble!

Next comes the finite buffer size, so we must block if full. Let's try:

Producer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    event = waitForEvent()\;
    spaceAvailable.wait()\;
    bufferAccess.wait()\;
    buffer.add(event)\;
    if(++count != bufferSize)\;
    spaceAvailable.signal()\;
    bufferAccess.signal()\;
    isNotEmpty.signal()\;
\end{algorithm}
Consumer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    isNotEmpty.wait()\;
    bufferAccess.wait()\;
    event = buffer.get()\;
    if(count-- == bufferSize)\;
    spaceAvailable.signal()\;
    bufferAccess.signal()\;
    event.process()\;
\end{algorithm}

The hint says to use a mutex, a semaphore called items init to zero, and a semaphore called spaces init to the buffer size.

Producer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    event = waitForEvent()\;
    spaces.wait()\;
    mutex.wait()\;
    buffer.add(event)\;
    mutex.signal()\;
    items.signal()\;
\end{algorithm}
Consumer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    items.wait()\;
    mutex.wait()\;
    event = buffer.get()\;
    mutex.signal()\;
    spaces.signal()\;
    event.process()\;
\end{algorithm}
This is correct. So over my first try we need no if-statements or variables.
\subsubsection{Chapter 4.2 -- Readers-writers problem}
\begin{itemize}
    \item Any num readers can be in crit sec at same time
    \item Writes must have exclusive access to crit sec
\end{itemize}
Let's try! So we probably need two layers. Say many readers are reading. Then we need a semaphore for the writers to indicate they want to write, and a second semaphore to wait for the readers to finish up. So maybe have the readers rendezvous and then let a write through or something?

Needed hints. The hints were to use "int readers = 0", "mutex = semaphore(1)", and "roomEmpty = semaphore(1)".

Reader:
\begin{algorithm}[H]
    \DontPrintSemicolon
    mutex.wait()\;
    readers++\;
    if(readers == 1) \{\;
        roomEmpty.wait()\;
    \}\;
    mutex.signal()\;
    critical section\;
    mutex.wait()\;
    readers--\;
    if(readers == 0) \{\;
        roomEmpty.signal()\; 
    \}\;
    mutex.signal()\;
\end{algorithm}
Writer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    roomEmpty.wait()\;
    mutex.wait()\;
    critical section\;
    mutex.signal()\;
    roomEmpty.signal()\;
\end{algorithm}
This was almost correct. The writer does actually not need to use the mutex, as it is enough to check if the room is empty. This is because if a writer is allowed in, readers must be zero. If a reader then attempts to start reading, it is blocked in the 4th line.

This pattern of first in locks and last out unlocks is common, it is called a \textbf{Lightswitch}.

This solution can make writers starve. How do we avoid this?

Idea: An int tracks num writers waiting. If non-negative, we stop readers from starting by a loop?

The hint introduces a turnstile. It should work as a mutex for the writers, and a turnstile for readers. So:

Reader:
\begin{algorithm}[H]
    \DontPrintSemicolon
    turnstile.wait()\;
    turnstile.signal()\;
    mutex.wait()\;
    readers++\;
    if(readers == 1) \{\;
        roomEmpty.wait()\;
    \}\;
    mutex.signal()\;
    critical section\;
    mutex.wait()\;
    readers--\;
    if(readers == 0) \{\;
        roomEmpty.signal()\; 
    \}\;
    mutex.signal()\;
\end{algorithm}
Writer:
\begin{algorithm}[H]
    \DontPrintSemicolon
    turnstile.wait()\;
    roomEmpty.wait()\;
    critical section\;
    turnstile.signal()\;
    roomEmpty.signal()\;
\end{algorithm}
This was correct. There is still another problem to solve. If we want to prioritize writers, how can we do this?

Idea: We introduce a semaphore that is kinda like the lightswitch, but it locks when a writer is queued, and unlocks when there are no writers waiting.

So after a writer gains room access, if it is the first one, it locks the sem. If it is the last one out, it unlocks it. The readers wait on it after they pass the turnstile.
\subsubsection{Chapter 4.3 -- No-starve mutex}
We need \textbf{bounded waiting}, which means a thread waiting on a semaphore must do so finitely, and this must be provable. Starvation is of course not acceptable.

So we need to cooperate with the scheduler. What properties do we need from it?
\begin{itemize}
    \item If only one thread ready to run, the scheduler has to let it run
    \item If thread ready to run, the time it must wait is bounded (NOT guaranteed by many existing schedulers!)
    \item If threads waiting on a semaphore when thread executes \texttt{signal} then one waiting thread must be woken
\end{itemize}
Why the last property? Would be silly if a thread could signal and then accept its own semaphore after looping!

Think on this: You have three threads, infinite loop, a mutex and a critical section each. If T1 runs crit sec, and leaves, T2 gets to run, and T2 finishes, which lets T1 in and not T3. This could go on. Starvation. Need another property:
\begin{itemize}
    \item If thread waiting at semaphore, the number of threads woken before it is finite/bounded
\end{itemize}
This could be enforce by e.g. FIFO. A semaphore that has this property is called \textbf{strong}. 
If only the last property is OK (one woken), then it is \textbf{weak}.

Dijkstra said the mutex problem probably not solvable with only weak semaphores. It is possible. What do? Assume finite threads.

Idea: We program FIFO. The first thread to arrive will be the one to next enter.

Mutex fix:
\begin{algorithm}[H]
    \DontPrintSemicolon
    isNext.wait()\;
    isNext.signal()\;

    mutex.wait()\;
    critical section\;
    mutex.signal()\;
\end{algorithm}
Isn't it this easy? Both semaphores initialized to one.

UPDATE: Why is the above dumb? The turnstile does nothing. I think.

The hint says to use two counters for num threads in each "room", a mutex, and two turnstiles (one initially open, one initially locked).

The solution was a bit intricate. A mutex is used to increases num threads arrived. Then in turnstile1, decrement this. If then zero, open second turnstile. If not, then more threads have accumulated at start than has reached the decrementation line in the code. Then keep going.

If the second turnstile has opened, the first is closed. In the second, we decrement a second counter (which has been equal to num threads incr in first part, but is only decr in second turnstile). If then this is zero, we close t2 and open t1.

Cool! This allows for 1 thread to pass through, or 1000 threads to pass through. My initial attempt (on paper) hardcoded some number of threads to accumulate before opening second turnstile. I think that could work too.
\subsubsection{Chapter 4.4 -- Dining philosophers}
(How about we actually try indenting the code this time)

Basic philosopher loop:

\begin{algorithm}[H]
    \DontPrintSemicolon
    \While{True:}{
        think()\;
        get\_forks()\;
        eat()\;
        put\_forks()\;
    }
\end{algorithm}

A philosopher has a value \textit{i} from 0 to 4. The philosopher can attempt to grab fork \textit{i} or $i+1$.

We need to satisfy:
\begin{itemize}
    \item One philosopher per fork at a time
    \item No deadlock
    \item No starvation
    \item Must let more than one philosopher eat at the time
\end{itemize}

First question: If we simply let each philosopher wait on a fork on either side, and after eating put a fork down on each side in the same order. What's wrong?

The problem is of course if everyone picks up their right fork. Deadlock.

One way to solve that is of course to make one philosopher left-handed. This is equal to saying only four are allowed at the table at the same time.

Or maybe we mutex a counter, and we disallow "get\_forks()" if it's 4?

Easier way to do that! Just init a semaphore to four.
\subsubsection{Chapter 4.5 -- Cigarette smokers problem}
Four threads. One agent, makes two ingredients available (at random). Three smokers. Each have an infinite supply of their own ingredient- either paper, tobacco, or matches.

If both complementary ingredients are made available to a smoker, the smoker should consume them.

Let's look at the agent as a black box that does two of the following:

"tobacco.signal()", "match.signal()", "paper.signal()".

The first thought is maybe to have each smoker thread just wait on their two ingredients, and lastly signal the agent to go again. This obviously leads to a deadlock if a thread is interleaved after consuming only one ingredient, allowing another thread to consume the other. Both are stuck, and the agent will not wake.

The hint:

\begin{algorithm}[H]
    \DontPrintSemicolon
    \emph{Shitty naming as we now know incoming}\;
    isTobbaco = isPaper = isMatch = False\;
    tobaccoSem = Semaphore(0);
    paperSem = Semahpore(0)\;
    matchSem = Semaphore(0)\;
\end{algorithm}

Now how to use this?

We have a "tobaccoPusher". It listens to the paper and the matches semaphore. If it gets signalled on the paper, it sets "isPaper" to true. Then it waits on matches. If it gets signalled, it checks if "isPaper" is still true. If it is, then signal "tobacco", and set "isPaper" to false. All changes done under mutex.

Equal solutions for the other threads.

This allows for interleaving, not getting deadlocked.

This was basically correct, but the solution showed that the proper way to structure this is to have a pusher wait on a ingredient, check conditionally the two others (under mutex) and signal the corresponding smoker (and setting the appropriate boolean to false). If two ingredients were not found, only set the one signalled to true.

It might be more difficult if the agent is crazy and puts out many ingredients without waiting. Shouldn't the pushers simply now increment and decrement and check on zero etc.?

That was correct!
