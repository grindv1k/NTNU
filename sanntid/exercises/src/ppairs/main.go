package main

import (
    "time"
    "runtime"
    "ppairs/log"
    "ppairs/networking"
)

func main() {
    runtime.GOMAXPROCS(10)

    log.Initialize()
    networking.Initialize()

    t := time.NewTicker(time.Second * 1)
    for {
        select {
        case <- t.C:
        }
    }
}
