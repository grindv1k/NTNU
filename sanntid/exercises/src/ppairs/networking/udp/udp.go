package udp

import (
    "net"
    "encoding/json"
    "time"
    "strconv"
    "ppairs/log"
)

const PORT = "30001"
const HOST = "127.0.0.1"

func Initialize() {
    log.Info("Initializing UDP ...")

    go backupListener()
}

func backupListener() {
    log.Info("Initializing backup ...")
    lastSeenInt := 0
    // Returns a *UDPAddr on the form ":PORT_LISTEN_SERVER" since no IP is specified.
    // This means we will accept packets from any address on this port.
    log.Info("Resolving ...")
    backupAddr, err := net.ResolveUDPAddr("udp", ":" + PORT)
    if err != nil {
        log.Error(true, "Resolve error (backup)!", err)
    }

    // Listens to addr provided; namely everything on :PORT_LISTEN_SERVER
    log.Info("Listening ...")
    backupConnection, err := net.ListenUDP("udp", backupAddr)
    if err != nil {
        log.Error(true, "Listener error (backup)!", err)
    }

    buf := make([]byte, 1024)
    var i int

    for {
        backupConnection.SetDeadline(time.Now().Add(time.Second * 3))
        n, addr, err := backupConnection.ReadFromUDP(buf)
        if err != nil {
            log.Error(false, "Reader error (backup)!", err)
            break
        }


        err = json.Unmarshal(buf[0:n], &i)
        if err != nil {
            log.Error(true, "Unmarshal error (backup)!", err)
        }

        log.Info("Got: \"" + strconv.Itoa(i) + "\" from: " + addr.String())
        if i != lastSeenInt {
            lastSeenInt = i
        }
    }

    backupConnection.Close()

    masterSender(lastSeenInt)
}

func masterSender(i int) {
    log.Info("Initializing master starting from " + strconv.Itoa(i) + " ...")
    backupAddr, err := net.ResolveUDPAddr("udp", ":" + PORT)
    if err != nil {
        log.Error(true, "Resolve error, backupAddr (master)!", err)
    }

    masterAddr, err := net.ResolveUDPAddr("udp", ":0")
    if err != nil {
        log.Error(true, "Resolve error, masterAddr (master)!", err)
    }

    conn, err := net.DialUDP("udp", masterAddr, backupAddr)
    if err != nil {
        log.Error(true, "Dial error (master)!", err)
    }

    defer conn.Close()

    for {
        buf, err := json.Marshal(i)
        if err != nil {
            log.Error(false, "Marshal error (master)!", err)
        }

        _, err = conn.Write(buf)
        log.Info("Writing i = " + strconv.Itoa(i) + " ...")
        if err != nil {
            log.Error(false, "Write error (master)!", err)
        } else {
            i++
        }
        time.Sleep(time.Second)
    }
}
