package tcp

import (
    "net"
    "bufio"
    "os"
    "strings"
    "ppairs/log"
)

const PORT = "30000"
const SERVER_IP = "localhost"

func Initialize(isClient bool) {
    log.Info("Initializing TCP ...")

    if isClient {
        go SendMsg()
    } else {
        go listenForConnections()
    }
}

func listenForConnections() {
    log.Info("Initializing server ...")

    // Wait for connections. If you arrives, get its IP, subnet, and port.
    listener, err := net.Listen("tcp", ":" + PORT)
    if err != nil {
        log.Error(true, "Listener error (server)!", err)
    }

    for {
        connection, err := listener.Accept()
        if err != nil {
            log.Error(true, "Connection error (server)!", err)
        }

        go handleConnection(connection)
    }
}

func handleConnection(connection net.Conn) {
    log.Info("Handling connection ...")

    // Will this ever happen?
    defer connection.Close()
    for {

        msg, err := bufio.NewReader(connection).ReadString('\n')
        if err != nil {
            log.Warning("Reader error (server)!", err)
            break
        }

        log.Info("Message received: " + strings.Split(msg, "\n")[0])
        msgBack := strings.ToUpper(msg)
        connection.Write([]byte(msgBack))
    }

    log.Info("Connection closed.")
}

func SendMsg() {
    connection, err := net.Dial("tcp", SERVER_IP + ":" + PORT)
    if err != nil {
        log.Error(true, "Connection error (client)!", err)
    }

    defer connection.Close()
    for {
        reader := bufio.NewReader(os.Stdin)

        text, err := reader.ReadString('\n')
        if err != nil {
            log.Error(true, "Reader error (client)!", err)
        }

        log.Info("Sending: " + strings.Split(text, "\n")[0])
        _, err = connection.Write([]byte(text))
        if err != nil {
            log.Error(true, "Write error (client)!", err)
        }

        msg, err := bufio.NewReader(connection).ReadString('\n')
        if err != nil {
            log.Error(true, "Reader error (client)!", err)
        }
        log.Info("Received: " + strings.Split(msg, "\n")[0])
    }

    log.Info("Connection closed.")
}
