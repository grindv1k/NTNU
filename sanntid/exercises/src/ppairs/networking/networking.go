package networking

import (
    "ppairs/log"
    "ppairs/networking/udp"
    //"ppairs/networking/tcp"
)

func Initialize() {
    log.Info("Initializing networking ...")
    udp.Initialize()
}
