package main

import (
    "flag"
    //"time"
    "runtime"
    "elevators/log"
    "elevators/networking"
    "elevators/driver"
    "elevators/requests"
)

func main() {
    runtime.GOMAXPROCS(10)

    client := flag.Bool("c", true, "Run as client (true, default)")
    tcp := flag.Bool("p", false, "Run as tcp(true) or udp(false, default)")
    id := flag.Uint("i", 256, "Elevator id")
    flag.Parse()

    if *id > 255 || *id < 0 {
        log.Error(true, "ID must be in the range 0 to 255!")
    }

    log.Initialize()
    networking.Initialize(*client, *tcp, uint8(*id))
    driver.Initialize(*client)
    requests.Initialize()

    // Test this by initing it and not using
    floorListener := driver.GetFloorListener()

    for {
        select {
        case <- floorListener:
        }
    }
}
