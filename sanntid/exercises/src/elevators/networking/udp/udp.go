package udp

import (
    "net"
    "bufio"
    "os"
    "strings"
    "elevators/log"
    "elevators/driver"
)

const PORT= "30001"

func Initialize(isClient bool, id uint8) {
    log.Info("Initializing UDP ...")

    if isClient {
        go SendMsg()
    } else {
        go listenForMsgs()
    }
}

func listenForMsgs() {
    log.Info("Initializing server ...")
    // Returns a *UDPAddr on the form ":PORT_LISTEN_SERVER" since no IP is specified.
    // This means we will accept packets from any address on this port.
    serverAddr, err := net.ResolveUDPAddr("udp", ":" + PORT)
    if err != nil {
        log.Error(true, "Resolve error (server)!", err)
    }

    // Listens to addr provided; namely everything on :PORT_LISTEN_SERVER
    serverConnection, err := net.ListenUDP("udp", serverAddr)
    if err != nil {
        log.Error(true, "Listener error (server)!", err)
    }

    defer serverConnection.Close()

    buf := make([]byte, 1024)

    // This should of course not be here. Testing
    motorDir := driver.GetMotorDirSetter()

    for {
        n, addr, err := serverConnection.ReadFromUDP(buf)
        if err != nil {
            log.Error(true, "Reader error (server)!", err)
        }

        msgSanitized := strings.Split(string(buf[0:n]), "\n")[0] 
        log.Info("Got: \"" + msgSanitized + "\" from: " + addr.String())

        switch msgSanitized {
        case "UP":
            motorDir <- driver.UP 
        case "DOWN":
            motorDir <- driver.DOWN
        case "STOP":
            motorDir <- driver.STOP
        }
    }
}

func SendMsg() {
    serverAddr, err := net.ResolveUDPAddr("udp", ":" + PORT)
    if err != nil {
        log.Error(true, "Resolve error, serverAddr (client)!", err)
    }

    // This will get us a random (or is it available?) port.
    localAddr, err := net.ResolveUDPAddr("udp", ":0")
    if err != nil {
        log.Error(true, "Resolve error, localAddr (client)!", err)
    }

    // Connects to serverAddr for sending
    connection, err := net.DialUDP("udp", localAddr, serverAddr)
    if err != nil {
        log.Error(true, "Listener error (client)!", err)
    }

    defer connection.Close()

    for {
        reader := bufio.NewReader(os.Stdin)

        text, err := reader.ReadString('\n')
        if err != nil {
            log.Error(true, "Reader error (client)!", err)
        }

        log.Info("Sending: " + strings.Split(text, "\n")[0])
        _, err = connection.Write([]byte(text))
        if err != nil {
            log.Error(true, "Write error (client)!", err)
        }
    }
}
