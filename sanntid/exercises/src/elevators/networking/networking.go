package networking

import (
    "elevators/log"
    "elevators/networking/udp"
    "elevators/networking/tcp"
)

func Initialize(isClient bool, isTcp bool, id uint8) {
    log.Info("Initializing networking ...")

    if isTcp {
        tcp.Initialize(isClient, id)
    } else {
        udp.Initialize(isClient, id)
    }
}
