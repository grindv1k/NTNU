package log

import (
    "os"
    "fmt"
    "strings"
    "runtime"
    "crypto/md5"
    "github.com/fatih/color"
)

func Initialize() {
    Info("Initializing log ...")
}

func Info(prefix string, arguments ...interface{}) {
    message := fmt.Sprintf(prefix, arguments...)
    module, _ := getCaller()
    printMessage("INFO", color.FgCyan, module, message)
}

func Warning(prefix string, arguments ...interface{}) {
    message := fmt.Sprintf(prefix, arguments...)
    module, _ := getCaller()
    printMessage("WARNING", color.FgYellow, module, message)
}

func Error(exit bool, prefix string, arguments ...interface{}) {
    message := fmt.Sprintf(prefix, arguments...)
    module, fn := getCaller()
    printMessage("ERROR", color.FgRed, module, message + " {fn:" + fn + "}")
    if exit {
        os.Exit(1)
    }
}

func CheckError(err error) {
    if err != nil {
        Error(true, err.Error())
    }
}

func printMessage(level string, levelColor color.Attribute, unit string, msg string) {
    paddedLevel := fmt.Sprintf("%7v", level)
    paddedUnit := fmt.Sprintf("%14v", unit)
    coloredLevel := color.New(levelColor).SprintFunc()(paddedLevel)
    coloredUnit := color.New(getUnitColor(unit)).SprintFunc()(paddedUnit)
    fmt.Printf("[ %s ] %s: %s\n", coloredLevel, coloredUnit, msg)
}

func getUnitColor(unit string) color.Attribute {
    selector := md5.Sum([]byte(unit + "entropyentropyentropyentropyentropyentropyent"))[10] % 12
    switch selector {
    case 0:
        return color.FgGreen
    case 1:
        return color.FgYellow
    case 2:
        return color.FgBlue
    case 3:
        return color.FgMagenta
    case 4:
        return color.FgCyan
    case 5:
        return color.FgRed
    case 6:
        return color.FgHiGreen
    case 7:
        return color.FgHiYellow
    case 8:
        return color.FgHiBlue
    case 9:
        return color.FgHiMagenta
    case 10:
        return color.FgHiCyan
    case 11:
        return color.FgHiRed
    }

    return color.FgWhite
}

func getCaller() (string, string) {
    ip, _, _, _ := runtime.Caller(2)
    stack := strings.Split(runtime.FuncForPC(ip).Name(), "/")
    module := strings.Split(stack[len(stack) - 1], ".")[0]
    function := strings.Split(stack[len(stack) - 1], ".")[1]
    return module, function
}
