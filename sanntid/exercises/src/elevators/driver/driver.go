package driver

/*
#cgo CFLAGS: -std=gnu11
#cgo LDFLAGS: -lcomedi -lm
#include "io.h"
#include "elev.h"
*/
import "C"

import (
    "elevators/log"
    "time"
    "strconv"
)

const NUM_FLOORS = 4
const POLL_INTERVAL = 10

type Button uint8
const (
    CALL_UP Button = iota
    CALL_DOWN
    INSIDE
)

type Lamp struct {
    button Button
    on bool
    floor int
}

type Dir uint8
const (
    STOP Dir = iota
    UP
    DOWN
)

func Initialize(isClient bool) {
    if !isClient {
        log.Info("Initializing driver (simulation) ...")
        C.elev_init(C.ET_Simulation);
    }
}

func GetMotorDirSetter() chan<- Dir {
    motorDir := make(chan Dir)

    go func() {
        for {
            select {
            case d := <-motorDir:
                switch d {
                case STOP:
                    C.elev_set_motor_direction(C.DIRN_STOP)
                    log.Info("Setting motor direction to STOP ...")
                case UP:
                    C.elev_set_motor_direction(C.DIRN_UP)
                    log.Info("Setting motor direction to UP ...")
                case DOWN:
                    C.elev_set_motor_direction(C.DIRN_DOWN)
                    log.Info("Setting motor direction to DOWN ...")
                }
            }
        }
    }()

    // Initialize
    motorDir <- STOP

    return motorDir
}

func GetButtonLampSetter() chan<- Lamp {
    lampSetter := make(chan Lamp)

    go func() {
        for {
            select {
            case b := <-lampSetter:
                switch b.button {
                case CALL_UP:
                    if b.floor != NUM_FLOORS - 1 {
                        C.elev_set_button_lamp(C.BUTTON_CALL_UP , C.uint8_t(b.floor), C.bool(b.on))
                    }
                case CALL_DOWN:
                    if b.floor != 0 {
                        C.elev_set_button_lamp(C.BUTTON_CALL_DOWN , C.uint8_t(b.floor), C.bool(b.on))
                    }
                case INSIDE:
                    C.elev_set_button_lamp(C.BUTTON_COMMAND , C.uint8_t(b.floor), C.bool(b.on))
                }
            }
        }
    }()

    // Initialize
    for i := 0; i < NUM_FLOORS; i++ {
        lampSetter <- Lamp{button: CALL_UP, on: false, floor: i}
        lampSetter <- Lamp{button: CALL_DOWN, on: false, floor: i}
        lampSetter <- Lamp{button: INSIDE, on: false, floor: i}
    }

    return lampSetter
}

func GetFloorListener() <-chan uint8 {
    floorListener := make(chan uint8)
    poller := time.NewTicker(POLL_INTERVAL * time.Millisecond)
    prevFloor := -1

    go func() {
        for {
            select {
            case <- poller.C:
                currFloor := int(C.elev_get_floor_sensor_signal())
                if currFloor != -1 && currFloor != prevFloor {
                    prevFloor = currFloor
                    floorListener <- uint8(prevFloor)
                    log.Info("Arrived at floor " + strconv.Itoa(prevFloor) + " ...")
                }
            }
        }
    }()

    return floorListener
}
